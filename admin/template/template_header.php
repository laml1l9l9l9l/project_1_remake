<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">

	<style type="text/css">
		body
		{
			margin: 0;
			padding: 0;
		}
		#all{
			width:100%;
			height: 1000px;
			background: white;
		}
		#left{
			width: 20%;
			background: #0070a3;
			height: 100%;
			float: left;
		}
		#title{
			width: 100%;
			height: 5%;
			text-align: center;
			font-size: 30px;
			font-weight: bold;
			color: white;
			border-bottom: 1px solid white;
			padding-top: 10px;
		}
		#menu{
			width: 100%;
			height: 94%;
			border-top: 1px solid white;
		}
		#right{
			float: left;
			width: 80%;
			height: 100%;
		}
		#header{
			background: #0070a3;
			width: 100%;
			height: 6%;
			border-bottom: 1px solid white;
			border-left: 1px solid white;
			position: relative;
			
		}
		#search_bar{
			width:30%;
			height: 30%;
			position: absolute;
			top: 17px;
			left: 270px;
			float: left;
		}
		#search_button{
			width:10%;
			height: 40%;
			position: absolute;
			top: 17px;
			left: 620px;
			float: left;
		}
		#logout{
			color: white;
			text-decoration: none;
			width: 10%;
			height: 100%;
			float: right;
			background: none;
			border-left: 1px solid #f7f7fa;
			
			text-align: center;
			position: absolute;
			right: 0;
			top:0;
			padding-top: 2%; 
		}
		#admin{
			color: white;
			text-decoration: none;
			width: 10%;
			height: 100%;
			float: right;
			background: none;
			border-left: 1px solid #f7f7fa;
			text-align: center;
			position: absolute;
			right: 108px;
			top:0;
			padding-top: 1%;
		}
		#website{
			color: white;
			text-decoration: none;
			width: 10%;
			height: 100%;
			border-right: 1px solid #f7f7fa;
			text-align: center;
			position: absolute;
			padding-top: 2%;
		}
		#logout:hover{
			background: none;
		}
		#content{
			background: #f7f7fa;
			width: 90%;
			height: 94%;
			border-top: 1px solid white;
			border-left: 1px solid white;
			padding-left: 5%;
			padding-right: 5%;
			position: relative;
		}
		ul{
			list-style-type: none;
			display: block;
			margin:0;
			padding-top:0;
			padding-left:0;
			
		}
		li{
			float: left;
			background: #00477a;
			width: 100%;
			border: 1px solid #000000;
		}
		.menu_bar{
			color: white;
			text-decoration: none;
			font-size: 20px;
			font-family:"Times New Roman", Times, serif;
			font-weight: bold;
			text-align: center;
			display:block;
			padding-top:15px;
			height:35px;	
		}
		.menu_bar_2{
			color: white;
			text-decoration: none;
			font-size:18px;
			font-family:"Times New Roman", Times, serif;
			font-weight: lighter;
			text-align: center;
			display:block;
			padding-top:15px;
			height:35px;
			
		}
		ul li ul{
			display: none;
		}
		li:hover ul{
			display:block;
			
		}
		.menu_bar:hover,.menu_bar_2:hover{
			text-decoration: underline;
			background: darkblue;
		}
		#logout:hover, #admin:hover, #website:hover{
			text-decoration: underline;
			color: red;
		}

	</style>

</head>
<body>
	<div id="all">
		<div id="left">
			<div id="title">Trang quản lý</div>
			<div id="menu">
		<ul>
			<li><a href="../quan_ly_admin/danh_sach_admin.php" class="menu_bar">Quản lý Admin</a></li>
			<li><a href="../quan_ly_nha_san_xuat/danh_sach_nha_san_xuat.php" class="menu_bar">Quản lý NSX</a></li>
			<li><a href="../quan_ly_danh_muc/danh_sach_danh_muc.php" class="menu_bar">Quản lý các danh mục</a></li>
			<li><a href="../quan_ly_san_pham/danh_sach_san_pham.php" class="menu_bar">Quản lý sản phẩm</a>
				<ul>
					<li><a href="../quan_ly_san_pham/quan_ly_ao.php" class="menu_bar_2">Áo</a></li>
					<li><a href="../quan_ly_san_pham/quan_ly_quan.php" class="menu_bar_2">Quần</a></li>
				</ul>	
			</li>
			<li><a href="../quan_ly_khach_hang/danh_sach_khach_hang.php" class="menu_bar">Quản lý khách hàng</a></li>
			<li><a href="../quan_ly_hoa_don/danh_sach_hoa_don.php" class="menu_bar">Quản lý hóa đơn</a>
				<ul>
					<li><a href="../quan_ly_hoa_don/thong_ke_hoa_don.php" class="menu_bar_2">Thống kê hóa đơn</a></li>
				</ul>
			</li>
		</ul>	
			</div>	
		</div>
		<div id="right">
			<div id="header">
				<?php
					$ten = "";
						if(isset($_GET["tim_kiem_ten"]))
						{
							$ten = $_GET["tim_kiem_ten"];
						}
					include("../../connecting/open.php");
					$sql = "select * from admin";
					$result = mysqli_query($ket_noi, $sql);	
					$admin = mysqli_fetch_array($result);
				?>
				<form>
					<input type="text" id="search_bar" placeholder="Nhập vào tìm kiếm" name="tim_kiem_ten" value=<?php echo($ten); ?>>
					<button id="search_button">Tìm kiếm</button>
				</form>
				<a href="../login_admin/logout_admin.php" id="logout">Đăng xuất</a>
				<a href="../quan_ly_admin/thong_tin_ca_nhan.php" id="admin" style="position: relative;"><small style="position: absolute; top:25px; left: -10px; right: 0; margin-left: auto; margin-right: auto;">Thông tin cá nhân</small></a>
				<a href="../../trang_chu/trang_chu.php" id="website">Website</a>
				<?php include("../../connecting/close.php"); ?>		
			</div>
			<div id="content" style="">
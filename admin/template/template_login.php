<style type="text/css">
		body{
			margin: 0;
			padding: 0;
			font-family: sans-serif;
			background: #00477a;
		}
		.form{
			background: #191919;
			width: 300px;
			padding: 40px;
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%, -50%);
			text-align: center;
		}
		.form h1{
			color: white;
			font-weight: 700;
			text-transform: uppercase;
		}
		.form input[type="text"], .form input[type="password"]{
			border: 0;
			background: none;
			display: block;
			margin: 20px auto;
			text-align: center;
			border: 2px solid #3498db;
			padding: 14px 10px;
			width: 200px;
			outline: none;
			color: white;
			border-radius: 24px;
			transition: 0.25s;
		}
		.form input[type="text"]:focus, .form input[type="password"]:focus{
			width: 280px;
			border-color: #2cee71;
		}
		.form input[type="submit"]{
			border: none;
			background: none;
			display: block;
			margin: 20px auto;
			text-align: center;
			border: 2px solid #2cee71;
			padding: 14px 40px;
			color: white;
			outline: none;
			border-radius: 24px;
			transition: 0.25s;
			cursor: pointer;
		}
		.form input[type="submit"]:hover{
			background: #2cee71;
		}	
		#error{
			color: red;
		}
</style>
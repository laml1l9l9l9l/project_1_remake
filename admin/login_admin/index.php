<?php
	session_start();
	if(isset($_SESSION["taiKhoan"]))
	{
		header("location:../quan_ly_admin/danh_sach_admin.php");
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Trang đăng nhập</title>
	<?php
		include("../template/template_login.php");
	?>
</head>
<body>
	<form class="form" action="xu_ly_login_admin.php" method="post">
		<h1>Đăng nhập</h1>
		<span id="error"><?php if(isset($_GET["error"])){echo "*Tên đăng nhập hoặc mật khẩu không đúng!";} ?><span>
		<input type="text" name="user" placeholder="Tên đăng nhập" required="required"/>
		<input type="password" name="pass" placeholder="Mật khẩu" required="required"/>
		<input type="submit" value="Đăng nhập">	
	</form>	
</body>
</html>
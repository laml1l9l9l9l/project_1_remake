<?php
	session_start();
	if(!isset($_SESSION["taiKhoan"]))
	{
		header("location:../login_admin/index.php");
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Sửa sản phẩm</title>
</head>
<body>
	<?php
		if(isset($_GET["maSanPham"]))
		{
			$maSp = $_GET["maSanPham"];
			include("../../connecting/open.php");
			$sql = "select * from san_pham where ma_san_pham = $maSp";
			$result=mysqli_query($ket_noi, $sql);
			$sp=mysqli_fetch_array($result);
		}	
	?>	
	<form action="xu_ly_sua_san_pham.php" method="post">
		<table>
			<tr>
				<td style="display: none">Mã sản phẩm: </td>
				<td><input type="hidden" name="ma" readonly="readonly" value="<?php echo($sp["ma_san_pham"]); ?>"></td>
			</tr>
			<tr>
				<td>Tên sản phẩm: </td>
				<td><input type="text" name="ten_san_pham" value="<?php echo($sp["ten_san_pham"]); ?>"></td>
			</tr>
			<tr>
				<td>Ảnh: </td>
				<td></td>
			</tr>	
			<tr>
				<td>Giá: </td>
				<td><input type="text" name="gia" value="<?php echo($sp["gia_san_pham"]); ?>"</td>
			</tr>
			<tr>
				<td>Mô tả: </td>
				<td><input type="text" name="mo_ta" value="<?php echo($sp["mo_ta"]); ?>"</td>
			</tr>
			<tr>
				<td>Tình trạng: </td>
				<td>
					<input type="radio" name="tinh_trang" value="1" <?php if($sp["tinh_trang"]==1){?> checked="checked" <?php } ?> >Còn hàng
					<input type="radio" name="tinh_trang" value="0" <?php if($sp["tinh_trang"]==0){?> checked="checked" <?php } ?> >Hết hàng
				</td>
			</tr>
			<tr>
				<td>Tên NSX: </td>
				<td>
					<select name="ten_NSX">
						<?php
							$sql_NSX = "select * from nha_san_xuat order by ma_nha_san_xuat asc";
							$result_NSX = mysqli_query($ket_noi, $sql_NSX);
							while($nsx = mysqli_fetch_array($result_NSX))
							{	
								?>
									<option value="<?php echo($nsx["ma_nha_san_xuat"]); ?>" <?php if($sp["ma_nha_san_xuat"]==$nsx["ma_nha_san_xuat"]){?> selected="selected" <?php } ?> >
										<?php echo($nsx["ten_nha_san_xuat"]); ?>
									</option>
								<?php
							}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Tên danh mục: </td>
				<td>
					<select name="ten_danh_muc">
						<?php
							$sql_danh_muc = "select * from danh_muc order by ten_danh_muc asc";
							$result_danh_muc = mysqli_query($ket_noi, $sql_danh_muc);
							while($danh_muc = mysqli_fetch_array($result_danh_muc))
							{
								?> 
									<option value="<?php echo($danh_muc["ma_danh_muc"]) ?>" <?php if($sp["ma_danh_muc"]==$danh_muc["ma_danh_muc"]){?> selected="selected" <?php } ?> >
										<?php echo($danh_muc["ten_danh_muc"]); ?>		
									</option>
								<?php
							}	
							include("../../connecting/close.php");
						?>	
					</select>
				</td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="Cập nhật">
					<a href="danh_sach_sua_san_pham.php">
						<button>Quay lại</button>
					</a>
				</td>
			</tr>
		</table>
	</form>
</body>
</html>
<?php 
	session_start();
	if(!isset($_SESSION["taiKhoan"]))
	{
		header("location:../login_admin/index.php");
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Quản lí sản phẩm</title>
	<style type="text/css">
		#tac_vu{
			position: absolute;
			right: 20px;
		}
	</style>
</head>
<?php	
	include("../template/template_header.php");
	$ten_san_pham = "";
		if(isset($_GET["tim_kiem_ten"]))
		{
			$ten_san_pham = $_GET["tim_kiem_ten"];
		} 
	$gia_san_pham = "";
	if(isset($_GET["tim_kiem_gia"]))
		{
			$gia_san_pham = $_GET["tim_kiem_gia"];
		}
	$ma_NSX = 0;
	if(isset($_GET["tim_kiem_NSX"]))
		{
			$ma_NSX = $_GET["tim_kiem_NSX"];
		}				
?>
	<h1>Danh sách sản phẩm</h1>
	<div id="tac_vu">
		<a href="them_san_pham.php"><button>Thêm sản phẩm</button></a><br><br>
		<form id="tim_kiem_gia">
		Tìm kiếm giá:<br />
	        <input type="radio" name="tim_kiem_gia" value="1" <?php if($gia_san_pham==1){?> checked="checked"<?php }?> onchange="document.getElementById('tim_kiem_gia').submit()"> Nhỏ hơn 500000.<br />
	        <input type="radio" name="tim_kiem_gia" value="2" <?php if($gia_san_pham==2){?> checked="checked"<?php }?> onchange="document.getElementById('tim_kiem_gia').submit()"> 500000 đến 1 triệu.<br />
	        <input type="radio" name="tim_kiem_gia" value="3" <?php if($gia_san_pham==3){?> checked="checked"<?php }?> onchange="document.getElementById('tim_kiem_gia').submit()"> Lớn hơn 1 triệu.<br />
	    </form><br>    
	    <form id="tim_kiem_nsx">
	    Tìm kiếm NSX:<br>
			<select name="tim_kiem_NSX" onchange="document.getElementById('tim_kiem_nsx').submit()">
				<?php 
					include("../../connecting/open.php");
					$sql_NSX = "select * from nha_san_xuat order by ma_nha_san_xuat asc";
					$result_NSX = mysqli_query($ket_noi, $sql_NSX);
					while($NSX = mysqli_fetch_array($result_NSX))
					{	
						?>
							<option value="<?php echo($NSX["ma_nha_san_xuat"]); ?>" <?php if($ma_NSX == $NSX["ma_nha_san_xuat"]){ ?> selected="selected" <?php } ?> ><?php echo($NSX["ten_nha_san_xuat"]); ?></option>
						<?php
					}
				?>		
			</select>
		</form>
	</div>     
<?php 
	$so_san_pham_1_trang = 7;
	$result_so_trang = mysqli_query($ket_noi,"select count(*) as tong_san_pham from san_pham where ten_san_pham like '%$ten_san_pham%'");
		if(isset($_GET["tim_kiem_gia"]))
		{
			$gia_san_pham = $_GET["tim_kiem_gia"];
			if($gia_san_pham == 1)
			{
				$result_so_trang = mysqli_query($ket_noi,"select count(*) as tong_san_pham from san_pham where ten_san_pham like '%$ten_san_pham%' and gia_san_pham <=500000");
			}else if($gia_san_pham == 2)
			{
				$result_so_trang = mysqli_query($ket_noi,"select count(*) as tong_san_pham from san_pham where ten_san_pham like '%$ten_san_pham%' and gia_san_pham <=1000000 and gia_san_pham >=500000");
			}else
			{
				$result_so_trang = mysqli_query($ket_noi,"select count(*) as tong_san_pham from san_pham where ten_san_pham like '%$ten_san_pham%' and gia_san_pham >=1000000");	
			}
		}
		else if(isset($_GET["tim_kiem_NSX"]))
		{
			$ma_NSX = $_GET["tim_kiem_NSX"];
			if($ma_NSX != 0)
			{
				$sql = "select count(*) as tong_san_pham from san_pham where ten_san_pham like '%$ten_san_pham%' and ma_nha_san_xuat = '$ma_NSX'";
				$result_so_trang = mysqli_query($ket_noi, $sql);	
			}
		}

		$dem_tong_san_pham = mysqli_fetch_array($result_so_trang);
		$tong_so_san_pham = $dem_tong_san_pham["tong_san_pham"];
		$so_trang = ceil($tong_so_san_pham/$so_san_pham_1_trang);
		//lay ra page
		$page = 0;
		$start = 0;
		if(isset($_GET["page"]))
		{
			$page = $_GET["page"];
			$start = $page*$so_san_pham_1_trang;	
		}
		$sql="select san_pham.ma_san_pham, san_pham.ten_san_pham, san_pham.anh_san_pham, san_pham.gia_san_pham, san_pham.mo_ta, san_pham.tinh_trang, nha_san_xuat.ten_nha_san_xuat, danh_muc.ten_danh_muc 
		from san_pham 
		join nha_san_xuat on san_pham.ma_nha_san_xuat = nha_san_xuat.ma_nha_san_xuat
		join danh_muc on san_pham.ma_danh_muc = danh_muc.ma_danh_muc 
		where ten_san_pham like '%$ten_san_pham%' 
		limit $start, $so_san_pham_1_trang";
		if(isset($_GET["tim_kiem_gia"]))
		{
			//tim kiem gia, lay tat ca san pham
			$gia_san_pham = $_GET["tim_kiem_gia"];
			if($gia_san_pham==1)
			{
				$sql="select san_pham.ma_san_pham, san_pham.ten_san_pham, san_pham.anh_san_pham, san_pham.gia_san_pham, san_pham.mo_ta, san_pham.tinh_trang, nha_san_xuat.ten_nha_san_xuat, danh_muc.ten_danh_muc
				from san_pham 
				join nha_san_xuat on san_pham.ma_nha_san_xuat = nha_san_xuat.ma_nha_san_xuat 
				join danh_muc on san_pham.ma_danh_muc = danh_muc.ma_danh_muc 
				where ten_san_pham like '%$ten_san_pham%' 
				and gia_san_pham <=500000 
				limit $start, $so_san_pham_1_trang";
				
			}else if($gia_san_pham==2)
			{
				$sql="select san_pham.ma_san_pham, san_pham.ten_san_pham, san_pham.anh_san_pham, san_pham.gia_san_pham, san_pham.mo_ta, san_pham.tinh_trang, nha_san_xuat.ten_nha_san_xuat, danh_muc.ten_danh_muc 
				from san_pham 
				join nha_san_xuat on san_pham.ma_nha_san_xuat = nha_san_xuat.ma_nha_san_xuat
				join danh_muc on san_pham.ma_danh_muc = danh_muc.ma_danh_muc 
				where ten_san_pham like '%$ten_san_pham%' 
				and gia_san_pham <=1000000 and gia_san_pham >=500000 
				limit $start,$so_san_pham_1_trang";
			}else
			{
				$sql="select san_pham.ma_san_pham, san_pham.ten_san_pham, san_pham.anh_san_pham, san_pham.gia_san_pham, san_pham.mo_ta, san_pham.tinh_trang, nha_san_xuat.ten_nha_san_xuat, danh_muc.ten_danh_muc 
				from san_pham 
				join nha_san_xuat on san_pham.ma_nha_san_xuat = nha_san_xuat.ma_nha_san_xuat 
				join danh_muc on san_pham.ma_danh_muc = danh_muc.ma_danh_muc
				where ten_san_pham like '%$ten_san_pham%' 
				and gia_san_pham >=1000000 
				limit $start,$so_san_pham_1_trang";
			}
		}else if(isset($_GET["tim_kiem_NSX"]))
		{
			//tim kiem NSX, lay tat ca san pham
			$ma_NSX = $_GET["tim_kiem_NSX"];
			if($ma_NSX != 0)
			{
				$sql="select san_pham.ma_san_pham, san_pham.ten_san_pham, san_pham.anh_san_pham, san_pham.gia_san_pham, san_pham.mo_ta, san_pham.tinh_trang, nha_san_xuat.ten_nha_san_xuat, danh_muc.ten_danh_muc
				from san_pham 
				join nha_san_xuat on san_pham.ma_nha_san_xuat = nha_san_xuat.ma_nha_san_xuat 
				join danh_muc on san_pham.ma_danh_muc = danh_muc.ma_danh_muc 
				where ten_san_pham like '%$ten_san_pham%' 
				and san_pham.ma_nha_san_xuat like '$ma_NSX' 
				limit $start, $so_san_pham_1_trang";
			}
		}
	$result = mysqli_query($ket_noi, $sql);
	if(mysqli_num_rows($result))
	{
?>	
	<table border="1" cellpadding="0" cellspacing="0" width="85%">
		<tr>
			<th width="2%">Mã sản phẩm</th>
			<th>Tên sản phẩm</th>
			<th>Ảnh</th>
			<th>Giá</th>
			<th width="30%">Mô tả</th>
			<th>Tình trạng</th>
			<th width="15%">Tên NSX</th>
			<th>Tên danh mục</th>
			<th colspan="2">Tác vụ</th>
		</tr>
		<?php
			while($sp = mysqli_fetch_array($result))
			{
				?>
					<tr>
						<td><?php echo($sp["ma_san_pham"]); ?></td>
						<td><?php echo($sp["ten_san_pham"]); ?></td>
						<td><img src="../../img/<?php echo($sp["anh_san_pham"]); ?>" width="100px" height="100px"></td>
						<td><?php echo($sp["gia_san_pham"]); ?></td>
						<td align="center"><?php echo($sp["mo_ta"]); ?></td>
						<td align="center"><?php if($sp["tinh_trang"]==0){?><font color="red"><?php echo("Hết hàng"); ?></font><?php }else { ?><font color="green"><?php echo("Còn hàng"); ?></font><?php } ?></td>
						<td><?php echo($sp["ten_nha_san_xuat"]); ?></td>
						<td align="center"><?php echo($sp["ten_danh_muc"]); ?></td>
						<td><a href="sua_san_pham.php?maSanPham=<?php echo($sp["ma_san_pham"]); ?>">Sửa</a></td>
						<td><a href="xoa_san_pham.php?maSanPham=<?php echo($sp["ma_san_pham"]); ?>" onclick="return confirm('Có muốn xóa!?')">Xóa</a></td>
					</tr>
				<?php
			}
			include("../../connecting/close.php");

		?>
	</table>
	<?php
		for($i = 0; $i<$so_trang; $i++)
		{
			if(isset($_GET["ten_san_pham"])&&isset($_GET["gia_san_pham"]))
			{
				//khi co tim kiem
				$ten = $_GET["ten_san_pham"];
				$gia = $_GET["gia_san_pham"];
				?>
                <a href="?ten=<?php echo($ten);?>&&gia=<?php echo($gia);?>&&page=<?php echo($i);?>"><?php echo($i+1);?></a>
                <?php	
			}else
			{
				?>
				<a href="?page=<?php echo($i);?>"><?php echo($i+1);?></a>
				<?php
			}
		}
		}else
		{
			echo("Không tìm thấy kết quả");
		}
	?>	
<?php include("../template/template_footer.php"); ?>
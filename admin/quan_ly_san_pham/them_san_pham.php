<?php
	session_start();
	if(!isset($_SESSION["taiKhoan"]))
	{
		header("location:../login_admin/index.php");
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Thêm sản phẩm</title>
</head>
<body>
	<form action="xu_ly_them_san_pham.php" name="them_san_pham">
		<table>
			<tr>
				<td>Tên sản phẩm: </td>
				<td><input type="text" name="ten_san_pham"></td>
			</tr>
			<tr>
				<td>Ảnh </td>
				<td><input type="text" name="anh" id="upload_anh"></td>
				<td><input type="button" name="Button" value="Chọn ảnh" onClick="window.open('Upload.php','','width=500,height=100, status=false')"></td>
			</tr>
			<tr>
				<td>Giá: </td>
				<td><input type="text" name="gia"></td>
			</tr>
			<tr>
				<td>Mô tả: </td>
				<td><input type="text" name="mo_ta"></td>
			</tr>
			<tr>
				<td>Tình trạng: </td>
				<td>
					<input type="radio" name="tinh_trang" value="1">Còn hàng
					<input type="radio" name="tinh_trang" value="0">Hết hàng
				</td>
			</tr>
			<tr>
				<td>Tên NSX: </td>
				<td>
					<select name="ten_NSX">
						<?php 
							include("../../connecting/open.php");
							$sqlNSX = "select * from nha_san_xuat order by ma_nha_san_xuat asc";
							$resultNSX = mysqli_query($ket_noi, $sqlNSX);
							while($nsx = mysqli_fetch_array($resultNSX))
							{	
								?>
									<option value="<?php echo($nsx["ma_nha_san_xuat"]); ?>">
										<?php echo($nsx["ten_nha_san_xuat"]); ?>
									</option>
								<?php
							}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Tên danh mục: </td>
				<td>
					<select name="ten_danh_muc">
						<?php
							$sqlDm = "select * from danh_muc order by ma_danh_muc asc";
							$resultDm = mysqli_query($ket_noi, $sqlDm);
							while($dm = mysqli_fetch_array($resultDm))
							{
								?> 
									<option value="<?php echo($dm["ma_danh_muc"]) ?>">
										<?php echo($dm["ten_danh_muc"]); ?>		
									</option>
								<?php
							}	
							include("../../connecting/close.php");
						?>	
					</select>
				</td>
			</tr>
			<tr>
				<td></td>
				<td><button>Thêm</button></td>
			</tr>	
		</table>
	</form>
</body>
</html>
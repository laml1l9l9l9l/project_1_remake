<?php 
	session_start();
	if(!isset($_SESSION["taiKhoan"]))
	{
		header("location:../login_admin/index.php");
	}
	include("../template/template_header.php"); 
	$ten_khach_hang = "";
		if(isset($_GET["tim_kiem_ten"]))
		{
			$ten_khach_hang = $_GET["tim_kiem_ten"];
		}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Quản lí khách hàng</title>
</head>
<h1>Danh sách thông tin khách hàng</h1>
<?php 
	include("../../connecting/open.php");
	$sql = "select * from khach_hang where ten_khach_hang like '%$ten_khach_hang%'";
	$result = mysqli_query($ket_noi, $sql);
	if(mysqli_num_rows($result))
	{	
?>	
<table border="1" cellspacing="0" cellpadding="0">
	<tr>
		<th>Mã khách hàng</th>
		<th>Tên tài khoản</th>
		<th>Tên</th>
		<th>Ngày sinh</th>
		<th>Giới tính</th>
		<th>Email</th>
		<th>Số điện thoại</th>
		<th>Địa chỉ</th>
		<th>Tác vụ</th>
	</tr>
	<?php
		
		while($kh = mysqli_fetch_array($result))
		{
			$ngay_sinh_khach_hang = date("d-m-Y", strtotime($kh["ngay_sinh"]));
	?>
				<tr>
					<td><?php echo($kh["ma_khach_hang"]); ?></td>
					<td><?php echo($kh["tai_khoan_khach_hang"]); ?></td>
					<td><?php echo($kh["ten_khach_hang"]); ?></td>
					<td><?php echo($ngay_sinh_khach_hang); ?></td>
					<td><?php if($kh["gioi_tinh"]==0){echo("Nam");}else{echo("Nữ");}; ?></td>
					<td><?php echo($kh["email_khach_hang"]); ?></td>
					<td><?php echo($kh["so_dien_thoai"]); ?></td>
					<td><?php echo($kh["dia_chi"]); ?></td>
					<td align="center"><a href="xoa_khach_hang.php?maKhachHang=<?php echo($kh["ma_khach_hang"]); ?>" onclick="return confirm('Có muốn xóa!?');">Xóa</a></td>
				</tr>
	<?php
	
		}	
		include("../../connecting/close.php");
	?>
</table>
	<?php
		}else
		{
			echo("Không tìm thấy kết quả");
		}
	?>
<?php include("../template/template_footer.php"); ?>
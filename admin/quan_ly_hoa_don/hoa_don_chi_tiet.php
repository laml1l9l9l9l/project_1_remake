<?php
	session_start();
	if(!isset($_SESSION["taiKhoan"]))
	{
		header("location:../login_admin/index.php");
	}
	include("../Template/template_header.php"); 
	$ten_san_pham = "";
		if(isset($_GET["tim_kiem_ten"]))
		{
			$ten_san_pham = $_GET["tim_kiem_ten"];
		} 
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Chi tiết hóa đơn</title>
</head>
<h1>Chi tiết hóa đơn</h1>
<?php
	if(isset($_GET["maHoaDon"]))
	{	
		$maHoaDon = $_GET["maHoaDon"];
		include("../../connecting/open.php");
		$sql = "select hoa_don_chi_tiet.ma_hoa_don, san_pham.ten_san_pham,san_pham.anh_san_pham, hoa_don_chi_tiet.so_luong from hoa_don_chi_tiet inner join san_pham on hoa_don_chi_tiet.ma_san_pham = san_pham.ma_san_pham where ma_hoa_don = $maHoaDon";
		$result = mysqli_query($ket_noi, $sql);
		if(mysqli_num_rows($result))
		{	
?>	
<table border="1" cellpadding="0" cellspacing="0">
	<tr>
		<th>Mã hóa đơn</th>
		<th>Tên sản phẩm</th>
		<th>Ảnh</th>
		<th>Số lượng</th>
	</tr>
	<?php
		
			while($hdct = mysqli_fetch_array($result))
			{
				?>
					<tr>
						<td><?php echo($hdct["ma_hoa_don"]); ?></td>
						<td><?php echo($hdct["ten_san_pham"]); ?></td>
						<td><img src="../../img/<?php echo($hdct["anh_san_pham"]); ?>" width="100px" height="100px"></td>
						<td><?php echo($hdct["so_luong"]); ?></td>
					</tr>
				<?php
			}
			include("../../connecting/close.php");
		}else
		{
			header("location:danh_sach_hoa_don.php");
		}	
	?>	
</table>
	<?php
		}else
		{
			echo("Không tìm thấy kết quả");
		}
	?>

<!-- Nút quay lại -->
<div style="width: 80%; margin-top: 10px;">
	<a href="danh_sach_hoa_don.php">
		<button>Quay lại</button>
	</a>
</div>

<?php include("../template/template_footer.php"); ?>
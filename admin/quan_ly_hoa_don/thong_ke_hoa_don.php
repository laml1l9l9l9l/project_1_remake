<?php
	session_start();

	if(!isset($_SESSION["taiKhoan"]))
	{
		header("location:../login_admin/index.php");
	}
	$ten_khach_hang = "";
		if(isset($_GET["tim_kiem_ten"]))
		{
			$ten_khach_hang = $_GET["tim_kiem_ten"];
		}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Thống kê hóa đơn</title>
	<?php
		include("../template/template_header.php"); 
	?>
</head>
<h1>Thống kê hóa đơn khách hàng</h1>

<a href="thong_ke_hoa_don.php#hoa_don_chua_giao" style="margin-left: 10px; text-decoration: none;">
	<button>Hóa đơn chưa giao</button>
</a>
<a href="thong_ke_hoa_don.php#hoa_don_dang_giao" style="margin-left: 10px; text-decoration: none;">
	<button>Hóa đơn đang giao</button>
</a>
<a href="thong_ke_hoa_don.php#hoa_don_da_giao" style="margin-left: 10px; text-decoration: none;">
	<button>Hóa đơn đã giao</button>
</a>
<a href="thong_ke_hoa_don.php#hoa_don_da_huy" style="margin-left: 10px; margin-right: 20px; text-decoration: none;">
	<button>Hóa đơn đã hủy</button>
</a>
<a href="danh_sach_hoa_don.php" title="Xem hóa đơn" >
	<img src="../../img/icon_come_back.jpg" width="35px" height="35px">
</a>

<!-- Hóa đơn đã hủy -->
<?php
	include("../../connecting/open.php");
	$sql = "select hoa_don.ma_hoa_don, hoa_don.ten_nguoi_nhan, hoa_don.so_dien_thoai_nguoi_nhan, hoa_don.dia_chi_nguoi_nhan, hoa_don.ngay_dat_hang, hoa_don.tinh_trang_giao_hang, hoa_don.thanh_tien, khach_hang.ten_khach_hang from hoa_don inner join khach_hang on hoa_don.ma_khach_hang = khach_hang.ma_khach_hang where ten_khach_hang like '%$ten_khach_hang%' order by ma_hoa_don desc";
	$result = mysqli_query($ket_noi, $sql);
	if(mysqli_num_rows($result))
	{
?>

<div id="hoa_don_chua_giao">
<table border="1" cellspacing="0" cellpadding="0" width="100%" style="margin-top: 10px;">
	
	<!-- Hóa đơn chưa giao -->

	<tr>
		<td colspan="9">
			<h3 style="text-align: center; color: purple;">Hóa Đơn Chưa Giao</h3>
		</td>
	</tr>
	<tr>
		<th>Mã hóa đơn</th>
		<th>Tên khách hàng</th>
		<th>Tên người nhận</th>
		<th>Số điện thoại người nhận</th>
		<th>Địa chỉ người nhận</th>
		<th>Ngày đặt hàng</th>
		<th>Tình trạng</th>
		<th>Tổng tiền</th>
		<th colspan="2">Tác vụ</th>
	</tr>
	<?php
		while($hd = mysqli_fetch_array($result))
		{
			if($hd["tinh_trang_giao_hang"]==0){
				$ngay_dat_hang = $hd["ngay_dat_hang"];
				$ngay_dat_hang_moi = date("d-m-Y", strtotime($ngay_dat_hang));
			?>
				<tr>
					<td width="2%"><?php echo($hd["ma_hoa_don"]); ?></td> 
					<td width="10%"><?php echo($hd["ten_khach_hang"]); ?></td>
					<td width="10%"><?php echo($hd["ten_nguoi_nhan"]); ?></td>
					<td><?php echo($hd["so_dien_thoai_nguoi_nhan"]); ?></td>
					<td width="28%"><?php echo($hd["dia_chi_nguoi_nhan"]); ?></td>
					<td align="center"><?php echo($ngay_dat_hang_moi); ?></td>
					<td>
						<?php
							if($hd["tinh_trang_giao_hang"]==0){
								echo "Đã hủy đơn hàng";
							}else{
								echo "Lỗi hiển thị :((";
							}
						?>
					</td>
					<td><?php echo($hd["thanh_tien"]); ?></td>
					<td align="center">
						<a href="xu_ly_huy_hoa_don.php?ma_hoa_don=<?php echo($hd["ma_hoa_don"]); ?>">
							Hủy hóa đơn
						</a>
					</td>
				</tr>	
			<?php

			}
		}	
	?>

</table>
</div>

<div id="hoa_don_dang_giao">
<table border="1" cellspacing="0" cellpadding="0" width="100%" style="margin-top: 10px;">

	<!-- Hóa đơn đang giao -->
	<tr>
		<td colspan="9">
			<h3 style="text-align: center; color: #d6d629;">Hóa Đơn Đang Giao</h3>
		</td>
	</tr>
	<tr>
		<th>Mã hóa đơn</th>
		<th>Tên khách hàng</th>
		<th>Tên người nhận</th>
		<th>Số điện thoại người nhận</th>
		<th>Địa chỉ người nhận</th>
		<th>Ngày đặt hàng</th>
		<th>Tình trạng</th>
		<th>Tổng tiền</th>
	</tr>
	<?php
		$sql = "select hoa_don.ma_hoa_don, hoa_don.ten_nguoi_nhan, hoa_don.so_dien_thoai_nguoi_nhan, hoa_don.dia_chi_nguoi_nhan, hoa_don.ngay_dat_hang, hoa_don.tinh_trang_giao_hang, hoa_don.thanh_tien, khach_hang.ten_khach_hang from hoa_don inner join khach_hang on hoa_don.ma_khach_hang = khach_hang.ma_khach_hang where ten_khach_hang like '%$ten_khach_hang%' order by ma_hoa_don desc";
		$result = mysqli_query($ket_noi, $sql);
		while($hd = mysqli_fetch_array($result))
		{
			if($hd["tinh_trang_giao_hang"]==1){
				$ngay_dat_hang = $hd["ngay_dat_hang"];
				$ngay_dat_hang_moi = date("d-m-Y", strtotime($ngay_dat_hang));
			?>
				<tr>
					<td width="2%"><?php echo($hd["ma_hoa_don"]); ?></td> 
					<td width="10%"><?php echo($hd["ten_khach_hang"]); ?></td>
					<td width="10%"><?php echo($hd["ten_nguoi_nhan"]); ?></td>
					<td><?php echo($hd["so_dien_thoai_nguoi_nhan"]); ?></td>
					<td width="28%"><?php echo($hd["dia_chi_nguoi_nhan"]); ?></td>
					<td align="center"><?php echo($ngay_dat_hang_moi); ?></td>
					<td>
						<?php
							if($hd["tinh_trang_giao_hang"]==1){
								echo "Đã hủy đơn hàng";
							}else{
								echo "Lỗi hiển thị :((";
							}
						?>
					</td>
					<td><?php echo($hd["thanh_tien"]); ?></td>
				</tr>	
			<?php

			}
		}	
	?>

</table>
</div>

<div id="hoa_don_da_giao">
<table border="1" cellspacing="0" cellpadding="0" width="100%" style="margin-top: 10px;">

	<!-- Hóa đơn đã giao -->
	<tr>
		<td colspan="9">
			<h3 style="text-align: center; color: green;">Hóa Đơn Đã Giao</h3>
		</td>
	</tr>
	<tr>
		<th>Mã hóa đơn</th>
		<th>Tên khách hàng</th>
		<th>Tên người nhận</th>
		<th>Số điện thoại người nhận</th>
		<th>Địa chỉ người nhận</th>
		<th>Ngày đặt hàng</th>
		<th>Tình trạng</th>
		<th>Tổng tiền</th>
	</tr>
	<?php
		$sql = "select hoa_don.ma_hoa_don, hoa_don.ten_nguoi_nhan, hoa_don.so_dien_thoai_nguoi_nhan, hoa_don.dia_chi_nguoi_nhan, hoa_don.ngay_dat_hang, hoa_don.tinh_trang_giao_hang, hoa_don.thanh_tien, khach_hang.ten_khach_hang from hoa_don inner join khach_hang on hoa_don.ma_khach_hang = khach_hang.ma_khach_hang where ten_khach_hang like '%$ten_khach_hang%' order by ma_hoa_don desc";
		$result = mysqli_query($ket_noi, $sql);
		while($hd = mysqli_fetch_array($result))
		{
			if($hd["tinh_trang_giao_hang"]==2){
				$ngay_dat_hang = $hd["ngay_dat_hang"];
				$ngay_dat_hang_moi = date("d-m-Y", strtotime($ngay_dat_hang));
			?>
				<tr>
					<td width="2%"><?php echo($hd["ma_hoa_don"]); ?></td> 
					<td width="10%"><?php echo($hd["ten_khach_hang"]); ?></td>
					<td width="10%"><?php echo($hd["ten_nguoi_nhan"]); ?></td>
					<td><?php echo($hd["so_dien_thoai_nguoi_nhan"]); ?></td>
					<td width="28%"><?php echo($hd["dia_chi_nguoi_nhan"]); ?></td>
					<td align="center"><?php echo($ngay_dat_hang_moi); ?></td>
					<td>
						<?php
							if($hd["tinh_trang_giao_hang"]==2){
								echo "Đã giao đơn hàng";
							}else{
								echo "Lỗi hiển thị :((";
							}
						?>
					</td>
					<td><?php echo($hd["thanh_tien"]); ?></td>
				</tr>	
			<?php

			}
		}	
	?>

</table>
</div>

<div id="hoa_don_da_huy">
<table border="1" cellspacing="0" cellpadding="0" width="100%" style="margin-top: 10px;">

	<!-- Hóa đơn đã hủy -->
	<tr>
		<td colspan="9">
			<h3 style="text-align: center; color: red;">Hóa Đơn Đã Hủy</h3>
		</td>
	</tr>
	<tr>
		<th>Mã hóa đơn</th>
		<th>Tên khách hàng</th>
		<th>Tên người nhận</th>
		<th>Số điện thoại người nhận</th>
		<th>Địa chỉ người nhận</th>
		<th>Ngày đặt hàng</th>
		<th>Tình trạng</th>
		<th>Tổng tiền</th>
		<th colspan="2">Tác vụ</th>
	</tr>
	<?php
		$sql = "select hoa_don.ma_hoa_don, hoa_don.ten_nguoi_nhan, hoa_don.so_dien_thoai_nguoi_nhan, hoa_don.dia_chi_nguoi_nhan, hoa_don.ngay_dat_hang, hoa_don.tinh_trang_giao_hang, hoa_don.thanh_tien, khach_hang.ten_khach_hang from hoa_don inner join khach_hang on hoa_don.ma_khach_hang = khach_hang.ma_khach_hang where ten_khach_hang like '%$ten_khach_hang%' order by ma_hoa_don desc";
		$result = mysqli_query($ket_noi, $sql);
		while($hd = mysqli_fetch_array($result))
		{
			if($hd["tinh_trang_giao_hang"]==3){
				$ngay_dat_hang = $hd["ngay_dat_hang"];
				$ngay_dat_hang_moi = date("d-m-Y", strtotime($ngay_dat_hang));
			?>
				<tr>
					<td width="2%"><?php echo($hd["ma_hoa_don"]); ?></td> 
					<td width="10%"><?php echo($hd["ten_khach_hang"]); ?></td>
					<td width="10%"><?php echo($hd["ten_nguoi_nhan"]); ?></td>
					<td><?php echo($hd["so_dien_thoai_nguoi_nhan"]); ?></td>
					<td width="28%"><?php echo($hd["dia_chi_nguoi_nhan"]); ?></td>
					<td align="center"><?php echo($ngay_dat_hang_moi); ?></td>
					<td>
						<?php
							if($hd["tinh_trang_giao_hang"]==3){
								echo "Đã hủy đơn hàng";
							}else{
								echo "Lỗi hiển thị :((";
							}
						?>
					</td>
					<td><?php echo($hd["thanh_tien"]); ?></td>
					<td align="center">
						<a href="xu_ly_xoa_hoa_don.php?ma_hoa_don=<?php echo($hd["ma_hoa_don"]); ?>">
							Xóa hóa đơn
						</a>
					</td>
				</tr>	
			<?php

			}
		}	
	?>
</table>
</div>

	<?php	
			include("../../connecting/close.php");	
		}else
		{
			echo("Không tìm thấy kết quả");
		}
	?>

<!-- Nút quay lại -->
<div style="width: 80%; margin-top: 10px;">
	<a href="danh_sach_hoa_don.php">
		<button>
			Xem hóa đơn
		</button>
	</a>
<?php
	if(isset($_GET["xoa_hoa_don"])){
		if($_GET["xoa_hoa_don"]==1){
?>
	<span>
		Xóa hóa đơn thành công
	</span>
<?php
		}else{
?>
	<span>
		Lỗi xóa hóa đơn
	</span>
<?php
		}
	}
?>
</div>

<?php
	include("../template/template_footer.php");
?>
<?php
	session_start();

	if(!isset($_SESSION["taiKhoan"]))
	{
		header("location:../login_admin/index.php");
	}
	$ten_khach_hang = "";
		if(isset($_GET["tim_kiem_ten"]))
		{
			$ten_khach_hang = $_GET["tim_kiem_ten"];
		}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Quản lí hóa đơn</title>
	<?php
		include("../template/template_header.php"); 
	?>
</head>
<h1>Danh sách hóa đơn khách hàng</h1>
<table border="1" cellspacing="0" cellpadding="0" width="100%">
<?php
	include("../../connecting/open.php");
	$sql = "select hoa_don.ma_hoa_don, hoa_don.ten_nguoi_nhan, hoa_don.so_dien_thoai_nguoi_nhan, hoa_don.dia_chi_nguoi_nhan, hoa_don.ngay_dat_hang, hoa_don.tinh_trang_giao_hang, hoa_don.thanh_tien, khach_hang.ten_khach_hang from hoa_don inner join khach_hang on hoa_don.ma_khach_hang = khach_hang.ma_khach_hang where ten_khach_hang like '%$ten_khach_hang%'";
	$result = mysqli_query($ket_noi, $sql);
	if(mysqli_num_rows($result))
	{
?>	
	<tr>
		<th>Mã hóa đơn</th>
		<th>Tên khách hàng</th>
		<th>Tên người nhận</th>
		<th>Số điện thoại người nhận</th>
		<th>Địa chỉ người nhận</th>
		<th>Ngày đặt hàng</th>
		<th>Tình trạng</th>
		<th>Tổng tiền</th>
		<th colspan="2">Tác vụ</th>
	</tr> 
	<?php
		while($hd = mysqli_fetch_array($result))
		{
			$ngay_dat_hang = $hd["ngay_dat_hang"];
			$ngay_dat_hang_moi = date("d-m-Y", strtotime($ngay_dat_hang));
			?>
				<tr>
					<td width="2%"><?php echo($hd["ma_hoa_don"]); ?></td> 
					<td width="10%"><?php echo($hd["ten_khach_hang"]); ?></td>
					<td width="10%"><?php echo($hd["ten_nguoi_nhan"]); ?></td>
					<td><?php echo($hd["so_dien_thoai_nguoi_nhan"]); ?></td>
					<td width="28%"><?php echo($hd["dia_chi_nguoi_nhan"]); ?></td>
					<td align="center"><?php echo($ngay_dat_hang_moi); ?></td>
					<td>
						<form action="tinh_trang_giao_hang.php" id="tinh_trang<?php echo($hd["ma_hoa_don"]); ?>">
							<input type="hidden" value="<?php echo($hd['ma_hoa_don']); ?>" name="ma_hoa_don" id="<?php echo($hd['ma_hoa_don']); ?>">
							<select name="tinh_trang" onchange="var tinh_trang=this.id;document.getElementById('tinh_trang'+tinh_trang).submit();" id="<?php echo($hd['ma_hoa_don']); ?>">
								<option value="0" <?php if($hd["tinh_trang_giao_hang"] == 0){?> selected="selected" <?php } ?> >Chưa giao hàng</option>
								<option value="1" <?php if($hd["tinh_trang_giao_hang"] == 1){?> selected="selected" <?php } ?> >Đang giao hàng</option>
								<option value="2" <?php if($hd["tinh_trang_giao_hang"] == 2){?> selected="selected" <?php } ?> >Đã giao hàng</option>
							</select>	
						</form>
					</td>
					<td><?php echo($hd["thanh_tien"]); ?></td>
					<td align="center"><a href="hoa_don_chi_tiet.php?maHoaDon=<?php echo($hd["ma_hoa_don"]); ?>">Chi tiết</a></td>
				</tr>	
			<?php
		}	
		include("../../connecting/close.php");		
	?>
</table>
	<?php 
		}else
		{
			echo("Không tìm thấy kết quả");
		}
	?>

<!-- Nút quay lại -->
<div style="width: 80%; margin-top: 10px;">
	<a href="huy_hoa_don.php" style="text-decoration: none;">
		<button>
			Hủy hóa đơn
		</button>
	</a>
	<?php
		if(isset($_GET["huy_hoa_don"])){
			if($_GET["huy_hoa_don"]==1){
	?>
		<span style="color: green;">
	<?php
				echo "Hủy Hóa Đơn Thành Công";
	?>
		</span>
	<?php
			}else{
	?>
		<span style="color: red;">
	<?php
				echo "Lỗi Hủy Hóa Đơn";
	?>
		</span>
	<?php
			}
		}
	?>
</div>

<?php include("../template/template_footer.php"); ?>
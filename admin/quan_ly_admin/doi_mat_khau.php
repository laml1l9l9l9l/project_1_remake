<?php
	session_start();
	if(!isset($_SESSION["taiKhoan"]))
	{
		header("location:../login_admin/index.php");
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Đổi mật khẩu</title>
	<style type="text/css">
		span{color:red}
	</style>
</head>
<body>
	<?php
		if(isset($_GET["ma_admin"]))
		{
			$maTk = $_GET["ma_admin"];
			include("../../connecting/open.php");
			$sql = "select * from admin where ma_admin = $maTk";
			$result=mysqli_query($ket_noi, $sql);
			$admin=mysqli_fetch_array($result);
			include("../../connecting/close.php");
		}	
	?>	
	<form action="xu_ly_doi_mat_khau.php" method="post" id="doi_mat_khau">
		<table>
			<tr>
				<td style="display: none">Mã: </td>
				<td><input type="hidden" name="ma" value="<?php echo($admin["ma_admin"]); ?>"></td>
			</tr>
			<tr>
				<td style="display: none">Mật khẩu:</td>
				<td><input type="hidden" name="mat_khau" value="<?php echo($admin["mat_khau"]); ?>"></td>
			</tr>
			<tr>
				<td>Mật khẩu cũ: </td>
				<td><input type="password" name="mat_khau_cu" id="mat_khau_cu"></td>
				<td><span id="loi_mat_khau_cu"></span></td>
			</tr>
			<tr>
				<td>Mật khẩu mới: </td>
				<td><input type="password" name="mat_khau_moi" id="mat_khau_moi"></td>
				<td><span id="loi_mat_khau_moi"></span></td>
			</tr>
			<tr>
				<td>Nhập lại mật khẩu mới: </td>
				<td><input type="password" name="nhap_lai_mat_khau_moi" id="nhap_lai_mat_khau_moi"></td>
				<td><span id="loi_nhap_lai_mat_khau_moi"></span></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><button type="button" onclick="doi_mat_khau()">Cập nhật</button></td>
			</tr>	
		</table>
	</form>	
	<script type="text/javascript">
		function doi_mat_khau()
		{
			var dem_loi = 0;

			var mat_khau_cu = document.getElementById('mat_khau_cu').value;
			var mat_khau_moi = document.getElementById('mat_khau_moi').value;
			var nhap_lai_mat_khau_moi = document.getElementById('nhap_lai_mat_khau_moi').value;

			var loi_mat_khau_cu = document.getElementById('loi_mat_khau_cu');
			var loi_mat_khau_moi = document.getElementById('loi_mat_khau_moi');
			var loi_nhap_lai_mat_khau_moi = document.getElementById('loi_nhap_lai_mat_khau_moi');

			regex_mat_khau_cu = /^[a-zA-Z0-9_~!@#$%^&*()=+-\/.]+$/;
			regex_mat_khau_moi = /^[a-zA-Z0-9_~!@#$%^&*()=+-\/.]+$/;
			regex_nhap_lai_mat_khau_moi = /^[a-zA-Z0-9_~!@#$%^&*()=+-\/.]+$/;

			if(mat_khau_cu.length == 0)
			{
				loi_mat_khau_cu.innerHTML = '*Không được để trống';
				dem_loi = 1;
			}else if(mat_khau_cu.length < 6)
			{
				loi_mat_khau_cu.innerHTML = '*Mật khẩu ít nhất 6 ký tự';
				dem_loi = 1; 
			}else
			{
				var result_mat_khau_cu = regex_mat_khau_cu.test(mat_khau_cu);
				if(result_mat_khau_cu == false)
				{
					loi_mat_khau_cu.innerHTML = '*Mật khẩu không đúng định dạng';
					dem_loi = 1;
				}else
				{
					loi_mat_khau_cu.innerHTML = '';
				}	
			}

			if(mat_khau_moi.length == 0)
			{
				loi_mat_khau_moi.innerHTML = '*Không được để trống';
				dem_loi = 1;
			}else if(mat_khau_moi.length < 6)
			{
				loi_mat_khau_moi.innerHTML = '*Mật khẩu ít nhất 6 ký tự';
				dem_loi = 1;
			}else
			{
				var result_mat_khau_moi = regex_mat_khau_moi.test(mat_khau_moi);
				if(result_mat_khau_moi == false)
				{
					loi_mat_khau_moi.innerHTML = '*Mật khẩu không đúng định dạng';
					dem_loi = 1;
				}else
				{
					loi_mat_khau_moi.innerHTML = '';
				}	
			}

			if(nhap_lai_mat_khau_moi.length == 0)
			{
				loi_nhap_lai_mat_khau_moi.innerHTML = '*Không được để trống';
				dem_loi = 1;
			}else if(nhap_lai_mat_khau_moi.length < 6)
			{
				loi_nhap_lai_mat_khau_moi.innerHTML = '*Mật khẩu ít nhất 6 ký tự';
				dem_loi = 1;
			}else if(nhap_lai_mat_khau_moi != mat_khau_moi)
			{
				loi_nhap_lai_mat_khau_moi.innerHTML = '*Mật khẩu không đúng';
				dem_loi = 1;
			}else
			{
				var result_nhap_lai_mat_khau_moi = regex_nhap_lai_mat_khau_moi.test(nhap_lai_mat_khau_moi);
				if(result_nhap_lai_mat_khau_moi == false)
				{
					loi_nhap_lai_mat_khau_moi.innerHTML = '*Mật khẩu không đúng định dạng';
					dem_loi = 1;
				}else
				{
					loi_nhap_lai_mat_khau_moi.innerHTML = '';
				}	
			}

			if(dem_loi == 1)
			{
				return false;
			}else
			{
				document.getElementById('doi_mat_khau').submit();
			}	
		}
	</script>
</body>
</html>
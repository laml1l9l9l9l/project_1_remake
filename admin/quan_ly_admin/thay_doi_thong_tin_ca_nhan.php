<?php
	session_start();
	if(!isset($_SESSION["taiKhoan"]))
	{
		header("location:../login_admin/index.php");
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Thay đổi thông tin cá nhân</title>
	<style type="text/css">
		span{color:red}
	</style>
</head>
<body>
	<?php
		if(isset($_GET["ma_admin"]))
		{
			$maTk = $_GET["ma_admin"];
			include("../../connecting/open.php");
			$sql = "select * from admin where ma_admin = $maTk";
			$result=mysqli_query($ket_noi, $sql);
			$admin=mysqli_fetch_array($result);
			include("../../connecting/close.php");
		}
	?>	
	<form action="xu_ly_thay_doi_thong_tin_ca_nhan.php" id="thay_doi_thong_tin_ca_nhan">
	<table>
		<tr>
			<td style="display: none">Mã:</td>
			<td><input type="hidden" name="ma" readonly="readonly" value="<?php echo($admin["ma_admin"]); ?>"></td>
		</tr>
		<tr>
			<td>Tên: </td>
			<td><input type="text" name="ten" id="ten" value="<?php echo($admin["ten_admin"]); ?>"></td>
			<td><span id="loi_ten"></span></td>
		</tr>
		<tr>
			<td>Email: </td>
			<td><input type="text" name="email" id="email" value="<?php echo($admin["email_admin"]); ?>"></td>
			<td><span id="loi_email"></span></td>
		</tr>
		<tr>
			<td>Số điện thoại: </td>
			<td><input type="text" name="sdt" id="sdt" value="<?php echo($admin["so_dien_thoai"]); ?>"></td>
			<td><span id="loi_sdt"></span></td>
		</tr>
		<tr>
			<td></td>
			<td><button type="button" onclick="thay_doi_thong_tin_ca_nhan()">Cập nhật</button></td>
		</tr>
	</table>
	</form>
	<script type="text/javascript">
		function thay_doi_thong_tin_ca_nhan()
		{
			var ten = document.getElementById('ten').value;
			var email = document.getElementById('email').value;
			var sdt = document.getElementById('sdt').value;

			var loi_ten = document.getElementById('loi_ten');
			var loi_email = document.getElementById('loi_email');
			var loi_sdt = document.getElementById('loi_sdt');

			var regex_ten = /^[a-zA-Z0-9\ ]+$/;
			var regex_email = /^[a-zA-Z0-9_.-]+@[a-zA-Z0-9]+\.?[A-Za-z]+\.[a-zA-Z]{2,3}$/;
			var regex_sdt = /^[0-9]{9,10}$/;

			var dem_loi = 0;

			if(ten.length == 0)
			{
				loi_ten.innerHTML = '*Không được để trống';
				dem_loi = 1;
			}else
			{
				var result_ten = regex_ten.test(ten);
				if(result_ten == false)
				{
					loi_ten.innerHTML = '*Tên không đúng định dạng';
					dem_loi = 1;
				}else
				{
					loi_ten.innerHTML = '';
				}	
			}

			if(email.length == 0)
			{
				loi_email.innerHTML = '*Không được để trống';
				dem_loi = 1;
			}else
			{
				var result_email = regex_email.test(email);
				if(result_email == false)
				{
					loi_email.innerHTML = '*Email không đúng định dạng';
					dem_loi = 1;
				}else
				{
					loi_email.innerHTML = '';
				}	
			}

			if(sdt.length == 0)
			{
				loi_sdt.innerHTML = '*Không được để trống';
				dem_loi = 1;
			}else
			{
				var result_sdt = regex_sdt.test(sdt);
				if(result_sdt == false)
				{
					loi_sdt.innerHTML = '*Số điện thoại không đúng định dạng';
					dem_loi = 1;
				}else
				{
					loi_sdt.innerHTML = '';
				}	
			}

			if(dem_loi == 1){
				return false;
			}else{
				document.getElementById('thay_doi_thong_tin_ca_nhan').submit();
			}
		}	
	</script>
</body>
</html>
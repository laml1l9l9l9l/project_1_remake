<?php
	session_start();
	if(!isset($_SESSION["taiKhoan"]))
	{
		header("location:../login_admin/index.php");
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Thêm admin</title>
	<style type="text/css">
		span{color: red}
	</style>
</head>
<body>
	<form action="Xu_ly_them_Admin.php" method="post" id="them_admin">
		<table>
			<tr>
				<td>Tên tài khoản: </td>
				<td><input type="text" name="ten_tai_khoan" id="ten_tai_khoan"></td>
				<td><span id="loi_ten_tai_khoan"></span></td>
			</tr>
			<tr>
				<td>Mật khẩu: </td>
				<td><input type="password" name="mat_khau" id="mat_khau"></td>
				<td><span id="loi_mat_khau"></span></td>
			</tr>
			<tr>
				<td>Tên: </td>
				<td><input type="text" name="ten" id="ten"></td>
				<td><span id="loi_ten"></span></td>
			</tr>
			<tr>
				<td>Email: </td>
				<td><input type="text" name="email" id="email"></td>
				<td><span id="loi_email"></span></td>
			</tr>
			<tr>
				<td>Số điện thoại: </td>
				<td><input type="text" name="sdt" id="sdt"></td>
				<td><span id="loi_SDT"></span></td>
			</tr>
			<tr>
				<td>Phân quyền</td>
				<td>
					<input type="radio" name="phan_quyen" value="0">Super Admin
					<input type="radio" name="phan_quyen" value="1" checked="checked">Admin
				</td>
				<td><span id="loi_phan_quyen"></span></td>
			</tr>
			<tr>
				<td></td>
				<td><button type="button" onclick="them_admin()">Thêm</button></td>
			</tr>	
		</table>
	</form>
	<script type="text/javascript">
		function them_admin()
		{
			var dem_loi = 0;

			var ten_tai_khoan = document.getElementById('ten_tai_khoan').value;
			var mat_khau = document.getElementById('mat_khau').value;
			var ten = document.getElementById('ten').value;
			var email = document.getElementById('email').value;
			var sdt = document.getElementById('sdt').value;

			var loi_ten_tai_khoan = document.getElementById('loi_ten_tai_khoan');
			var loi_mat_khau = document.getElementById('loi_mat_khau');
			var loi_ten = document.getElementById('loi_ten');
			var loi_email = document.getElementById('loi_email');
			var loi_SDT = document.getElementById('loi_SDT');

			var regex_ten_tai_khoan = /^[a-zA-Z0-9_]+$/;
			var regex_mat_khau = /^[a-zA-Z0-9_~!@#$%^&*()=+-\/.]+$/;
			var regex_ten = /^[a-zA-Z0-9\ ]+$/;
			var regex_email = /^[a-zA-Z0-9_.-]+@[a-zA-Z0-9]+\.?[A-Za-z]+\.[a-zA-Z]{2,3}$/;
			var regex_SDT = /^[0-9]{9,10}$/;

			if(ten_tai_khoan.length == 0)
			{
				loi_ten_tai_khoan.innerHTML = '*Không được để trống';
				dem_loi = 1;
			}else
			{
				var result_ten_tai_khoan = regex_ten_tai_khoan.test(ten_tai_khoan);
				if(result_ten_tai_khoan == false)
				{
					loi_ten_tai_khoan.innerHTML = '*Tên tài khoản không đúng định dạng';
					dem_loi = 1;
				}else
				{
					loi_ten_tai_khoan.innerHTML = '';
				}	
			}

			if(mat_khau.length == 0)
			{
				loi_mat_khau.innerHTML = '*Không được để trống';
				dem_loi = 1;
			}else if(mat_khau.length<6)
			{
				loi_mat_khau.innerHTML = '*Mật khẩu ít nhất 6 ký tự';
				dem_loi = 1;
			}else
			{
				var result_mat_khau= regex_mat_khau.test(mat_khau);
				if(result_mat_khau == false)
				{
					loi_mat_khau.innerHTML = '*Mật khẩu không đúng định dạng';
					dem_loi = 1;
				}else
				{
					loi_mat_khau.innerHTML = '';
				}	
			}

			if(ten.length == 0)
			{
				loi_ten.innerHTML = '*Không được để trống';
				dem_loi = 1;
			}else
			{
				var result_ten = regex_ten.test(ten);
				if(result_ten == false)
				{
					loi_ten.innerHTML = '*Tên không đúng định dạng';
					dem_loi = 1;
				}else
				{
					loi_ten.innerHTML = '';
				}	
			}

			if(email.length == 0)
			{
				loi_email.innerHTML = '*Không được để trống';
				dem_loi = 1;
			}else
			{
				var result_email = regex_email.test(email);
				if(result_email == false)
				{
					loi_email.innerHTML = '*Email không đúng định dạng';
					dem_loi = 1;
				}else
				{
					loi_email.innerHTML = '';
				}	
			}

			if(sdt.length == 0)
			{
				loi_SDT.innerHTML = '*Không được để trống';
				dem_loi = 1;
			}else
			{
				var result_SDT = regex_SDT.test(sdt);
				if(result_SDT == false)
				{
					loi_SDT.innerHTML = '*Số điện thoại không đúng định dạng';
					dem_loi = 1;
				}else
				{
					loi_SDT.innerHTML = '';
				}	
			}

			if(dem_loi == 1){
				return false;
			}else{
				document.getElementById('them_admin').submit();
			}
		}		
	</script>
</body>
</html>

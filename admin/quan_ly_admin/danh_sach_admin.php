<?php
	session_start();
	if(!isset($_SESSION["taiKhoan"]))
	{
		header("location:../login_admin/index.php");
	}	
	include("../template/template_header.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Quản lý admin</title>
</head>
<h1>Danh sách Admin</h1>
<?php 
	if(isset($_SESSION["phanQuyen"]))
	{
		$phanQuyen = $_SESSION["phanQuyen"];
		if($phanQuyen == 0)
		{
			?>
				<a href="them_admin.php"><button>Thêm admin</button></a><br>
			<?php
		}
	}
	$ten_admin = "";
		if(isset($_GET["tim_kiem_ten"]))
		{
			$ten_admin = $_GET["tim_kiem_ten"];
		}
?>
<?php 
	include("../../connecting/open.php");
	$sql = "select * from admin where ten_admin like '%$ten_admin%'";
	$result = mysqli_query($ket_noi, $sql);
	if(mysqli_num_rows($result))
	{	
?>
<table border="1" cellpadding="0" cellspacing="0">
	<tr align="center">
		<th>Mã tài khoản</th>
		<th>Tên</th>
		<th>Email</th>
		<th>Số điện thoại</th>
		<th>Phân quyền</th>
		<?php
			if($phanQuyen == 0)
			{
				?>
					<th colspan="2">Tác vụ</th>
				<?php			
			}
		?>
	</tr>
	<?php
		while($admin = mysqli_fetch_array($result))
		{
			?>
				<tr>
					<td><?php echo($admin["ma_admin"]); ?></td>
					<td><?php echo($admin["ten_admin"]); ?></td>
					<td><?php echo($admin["email_admin"]); ?></td>
					<td><?php echo($admin["so_dien_thoai"]); ?></td>
					<td><?php if($admin["phan_quyen"]==0){echo("Super Admin");}else{echo("Admin");} ?></td>
					<?php
					if($phanQuyen == 0)
					{
						?>	
							<td style="text-align: center;">
								<?php
									if($admin["phan_quyen"] == 0)
									{
										echo("");
									}else
									{
										?>
											<a href="Sua_Admin.php?ma_admin=<?php echo($admin["ma_admin"]); ?>">Sửa</a>
										<?php	
									}	
								?>
							</td>
						<?php
					}
					?>
				</tr>	
			<?php
		}	
		include("../../connecting/close.php");	
	?>
</table>
<?php 
	}else
	{
		echo("Không tìm thấy kết quả");
	}
?>			
<?php include("../template/template_footer.php"); ?>
<?php
    session_start();
    if(isset($_GET["ma_san_pham"]) && isset($_GET["ma_danh_muc"])){
        $ma_san_pham = $_GET["ma_san_pham"];
        $ma_danh_muc = $_GET["ma_danh_muc"];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Chi Tiết Sản Phẩm</title>
    
    <?php
        include '../template/head_link.php';
    ?>

</head>

<body class="product-page">

    <!-- Menu -->
    <?php
        include '../template/menu.php';
        include '../connecting/open.php';
        $lenh_lay_san_pham = mysqli_query($ket_noi,"select san_pham.ma_san_pham, san_pham.ten_san_pham, san_pham.anh_san_pham, san_pham.gia_san_pham, san_pham.mo_ta, san_pham.tinh_trang, danh_muc.ten_danh_muc, nha_san_xuat.ten_nha_san_xuat from (san_pham inner join nha_san_xuat on san_pham.ma_nha_san_xuat = nha_san_xuat.ma_nha_san_xuat) inner join danh_muc on san_pham.ma_danh_muc = danh_muc.ma_danh_muc where ma_san_pham = $ma_san_pham");
        $san_pham = mysqli_fetch_array($lenh_lay_san_pham);
    ?>
    
    <div class="wrapper">
        <div class="page-header page-header-mini">
        <?php
            if($ma_danh_muc == 1){
        ?>
            <div class="page-header-image" data-parallax="true" style="background-image: url('../img/pp-cov.jpg') ;">
            </div>
        <?php
            }else{
        ?>
            <div class="page-header-image" data-parallax="true" style="background-image: url('../img/background_quan.jpg') ;">
            </div>
        <?php
            }
        ?>
        </div>
        <div class="main">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <div id="productCarousel" class="carousel slide" data-ride="carousel" data-interval="8000">
                            <!-- Slide show ảnh sản phẩm -->
                            <!-- <ol class="carousel-indicators">
                                <li data-target="#productCarousel" data-slide-to="0" class="active"></li>
                                <li data-target="#productCarousel" data-slide-to="1"></li>
                                <li data-target="#productCarousel" data-slide-to="2"></li>
                                <li data-target="#productCarousel" data-slide-to="3"></li>
                            </ol> -->

                            
                            <div class="carousel-inner" role="listbox">
                                <div class="carousel-item active">
                                    <img class="d-block img-raised" src="../img/<?php echo $san_pham["anh_san_pham"]; ?>" alt="Slide 1">
                                </div>
                                <!-- <div class="carousel-item">
                                    <img class="d-block img-raised" src="../img/pp-2.jpg" alt="Slide 2">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block img-raised" src="../img/pp-3.jpg" alt="Slide 3">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block img-raised" src="../img/pp-4.jpg" alt="Slide 4">
                                </div> -->
                            </div>
                            <!-- <a class="carousel-control-prev" href="#productCarousel" role="button" data-slide="prev">
                                <button type="button" class="btn btn-primary btn-icon btn-round btn-sm" name="button">
                                    <i class="now-ui-icons arrows-1_minimal-left"></i>
                                </button>
                            </a>
                            <a class="carousel-control-next" href="#productCarousel" role="button" data-slide="next">
                                <button type="button" class="btn btn-primary btn-icon btn-round btn-sm" name="button">
                                    <i class="now-ui-icons arrows-1_minimal-right"></i>
                                </button>
                            </a> -->

                            <!-- Hết slide show ảnh sản phẩm -->
                        </div>

                        <!-- Chú thích sản phẩm -->
                        <!-- <p class="blockquote blockquote-primary">
                            "And thank you for turning my personal jean jacket into a couture piece. Wear yours with mirrored sunglasses on vacation."
                            <br>
                            <br>
                            <small>Kanye West</small>
                        </p> -->
                    </div>
                    <div class="col-md-6 ml-auto mr-auto"  id="chi_tiet_san_pham">
                        <h2 class="title"> <?php echo $san_pham["ten_san_pham"]; ?> </h2>
                        <h5 class="category"><?php echo $san_pham["ten_danh_muc"]; ?></h5>
                        <h2 class="main-price"> <?php echo number_format($san_pham["gia_san_pham"]); ?> VNĐ </h2>
                        <div id="accordion" role="tablist" aria-multiselectable="true" class="card-collapse">
                            <div class="card card-plain">
                                <div class="card-header" role="tab" id="headingOne">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Mô Tả
                                        <i class="now-ui-icons arrows-1_minimal-down"></i>
                                    </a>
                                </div>
                                <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="card-body">
                                        <p> <?php echo $san_pham["mo_ta"]; ?>. </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-plain">
                                <div class="card-header" role="tab" id="headingTwo">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Nhà Sản Xuất
                                        <i class="now-ui-icons arrows-1_minimal-down"></i>
                                    </a>
                                </div>
                                <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="card-body">
                                        <p> <?php echo $san_pham["ten_nha_san_xuat"]; ?> </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-plain">
                                <div class="card-header" role="tab" id="headingThree">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Tình trạng
                                        <i class="now-ui-icons arrows-1_minimal-down"></i>
                                    </a>
                                </div>
                                <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="card-body">
                                    <?php
                                        if($san_pham["tinh_trang"] == 0){
                                    ?>
                                            <p><strong>Còn Hàng</strong></p>
                                    <?php
                                        }else{
                                    ?>
                                            <p><strong>Hết Hàng</strong></p>
                                    <?php
                                        }
                                    ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Chọn màu và cỡ -->
                        <!-- <div class="row pick-size">
                            <div class="col-lg-6 col-md-8 col-sm-6">
                                <label>Select color</label>
                                <select class="selectpicker" data-style="select-with-transition btn btn-block" data-size="7">
                                    <option value="1">Black</option>
                                    <option value="2">Gray</option>
                                    <option value="3">White</option>
                                </select>
                            </div>
                            <div class="col-lg-6 col-md-8 col-sm-6">
                                <label>Select size</label>
                                <select class="selectpicker" data-style="select-with-transition btn btn-block" data-size="7">
                                    <option value="1">Small </option>
                                    <option value="2">Medium</option>
                                    <option value="3">Large</option>
                                </select>
                            </div>
                        </div> -->
                        <div class="row justify-content-end">
                            <a href="san_pham.php#san_pham">
                                <button class="btn btn-primary mr-3">Quay lại &nbsp;<i class="now-ui-icons arrows-1_minimal-left"></i></button>
                            </a>
                        <?php
                            if($san_pham["tinh_trang"] == 0){
                        ?>
                            <a href="../gio_hang/them_san_pham_vao_gio_hang.php?ma_san_pham=<?php echo$san_pham["ma_san_pham"]; ?>">
                                <button class="btn btn-primary mr-3">Thêm vào giỏ hàng &nbsp;<i class="now-ui-icons shopping_cart-simple"></i></button>
                            </a>
                        <?php
                            }else{
                        ?>
                            <a href="" title="Sản phẩm hết hàng">
                                <button class="btn btn-primary mr-3" disabled="disabled">Thêm vào giỏ hàng &nbsp;<i class="now-ui-icons shopping_cart-simple"></i></button>
                            </a>
                        <?php
                            }
                        ?>
                        </div>
                    </div>
                </div>

            <!-- Thông báo -->
            <?php
                if(isset($_GET["them_san_pham"])){

                    if($_GET["them_san_pham"] == 1){
                        // Thêm sản phẩm thành công
            ?>
                <div class="row">
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>Đặt hàng thành công!</strong> Bạn hãy nhấn nút quay lại để tiếp tục mua sắm.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true" class="align-middle">&times;</span>
                        </button>
                    </div>
                </div>
            <?php

                    }else{
                        // Thêm sản phẩm lỗi
            ?>
                <div class="row">
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>Sản phẩm đạt số lượng giới hạn!</strong> Bạn nên đặt hàng thì mới có thể mua tiếp.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true" class="align-middle">&times;</span>
                        </button>
                    </div>
                </div>
            <?php

                    }

                }
            ?>
            </div>
        </div>
        <!-- Hết phần giữa -->
        
    <!-- Phần footer -->
    <?php
        include '../template/footer.php';
        include '../connecting/close.php';
    ?>
    </div>
</body>

<!--   Core JS Files   -->
<script src="../js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="../js/core/popper.min.js" type="text/javascript"></script>
<script src="../js/core/bootstrap.min.js" type="text/javascript"></script>
<script src="../js/plugins/moment.min.js"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="../js/plugins/bootstrap-switch.js"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="../js/plugins/bootstrap-tagsinput.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="../js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="../js/plugins/jasny-bootstrap.min.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../js/plugins/nouislider.min.js" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="../js/plugins/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="../js/now-ui-kit.js?v=1.1.0" type="text/javascript"></script>

</html>

<?php
    }else{
       header("location:san_pham.php#san_pham"); 
    }
?>
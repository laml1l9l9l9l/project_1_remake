<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title> Sản Phẩm </title>
    
    <?php
        include '../template/head_link.php';
    ?>

</head>

<body class="ecommerce-page">
    
<!-- Menu -->
<?php
    include '../template/menu.php';
?>

<!-- Slide Show -->
<div class="wrapper">
    <div id="carouselExampleIndicators" class="carousel slide">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class=""></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2" class=""></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="carousel-item">
                <div class="page-header header-filter">
                    <div class="page-header-image" style="background-image: url('../img/bg40.jpg');"></div>
                    <div class="content-center text-center">
                        <div class="row">
                            <div class="col-md-8 ml-auto mr-auto2">
                                <h1 class="title">Thời trang công sở</h1>
                                <h4 class="description text-white">Trang phục công sở theo phong cách Paris</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item active">
                <div class="page-header header-filter">
                    <div class="page-header-image" style="background-image: url('../img/bg41.jpg');"></div>
                    <div class="content-center">
                        <div class="row">
                            <div class="col-md-8 ml-auto mr-auto text-center">
                                <h1 class="title">Phong cách đường phố</h1>
                                <h4 class="description text-white">Những bộ trang phục dạo phố được thiết kế đơn giản mang lại nét khỏe khoắn, hấp dẫn</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="page-header header-filter">
                    <div class="page-header-image" style="background-image: url('../img/bg29.jpg');"></div>
                    <div class="content-center text-center">
                        <div class="row">
                            <div class="col-md-8 ml-auto mr-auto">
                                <h1 class="title">Thời trang cho nam</h1>
                                <h4 class="description text-white">Những bộ cánh thiết kế cho nam giới the nhiều phong cách khác nhau tạo ra nhiều sự lựa chọn cho khách hàng</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <i class="now-ui-icons arrows-1_minimal-left"></i>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <i class="now-ui-icons arrows-1_minimal-right"></i>
        </a>
    </div>

    <!-- Phần chính -->
    <div class="main" id="san_pham">
        <div class="section">
            <div class="container">

                <!-- Tìm kiếm -->
                <h2 class="section-title">Tìm kiếm sản phẩm</h2>
                <div class="row">
                    <div class="col-md-3">
                        <div class="collapse-panel">

                            <!-- Tìm kiếm theo tên -->
                            <?php
                                // Đoạn php hieent thị số sản phẩm 1 trang và tìm kiếm
                                include '../connecting/open.php';

                                //phan trang va tim kiem san pham
                                $tim_kiem_theo_ten = "";
                                $dem_danh_muc = 0;
                                $dem_nha_san_xuat = 0;

                                if(isset($_GET["tim_kiem_theo_ten"])){
                                    $tim_kiem_theo_ten = $_GET["tim_kiem_theo_ten"];
                                }
                                $page = 1;
                                if(isset($_GET['page'])){
                                    $page = $_GET['page'];
                                }
                                // so san pham 1 trang
                                $limit = 6;

                                // Bo qua bao nhieu san pham
                                $offset = ($page-1)*$limit;

                                $lenh_phan_trang = mysqli_query($ket_noi,"SELECT * from san_pham where ten_san_pham like '%$tim_kiem_theo_ten%' limit $limit offset $offset");

                            if(isset($_GET["ma_danh_muc"])&&isset($_GET["ma_nha_san_xuat"])){
                                $ma_danh_muc = $_GET["ma_danh_muc"];
                                $ma_nha_san_xuat = $_GET["ma_nha_san_xuat"];
                            
                                if($ma_danh_muc == 0 && $ma_nha_san_xuat == 0){
                                    $lenh_phan_trang = mysqli_query($ket_noi,"select * from san_pham where ten_san_pham like '%$tim_kiem_theo_ten%' limit $limit offset $offset");
                                }else if($ma_danh_muc != 0 && $ma_nha_san_xuat == 0){
                                    $lenh_phan_trang = mysqli_query($ket_noi,"select * from san_pham where ten_san_pham like '%$tim_kiem_theo_ten%' and ma_danh_muc=$ma_danh_muc limit $limit offset $offset");
                                }else if($ma_danh_muc == 0 && $ma_nha_san_xuat != 0){
                                    $lenh_phan_trang = mysqli_query($ket_noi,"select * from san_pham where ten_san_pham like '%$tim_kiem_theo_ten%' and ma_nha_san_xuat=$ma_nha_san_xuat limit $limit offset $offset");
                                }else{
                                    $lenh_phan_trang = mysqli_query($ket_noi,"select * from san_pham where ten_san_pham like '%$tim_kiem_theo_ten%' and ma_danh_muc=$ma_danh_muc and ma_nha_san_xuat=$ma_nha_san_xuat limit $limit offset $offset");
                                }

                            }

                            ?>
                            <form action="?tim_kiem_theo_ten=<?php echo $tim_kiem_theo_ten; ?>#san_pham">
                            <div> 
                                <div class="card-header" role="tab" id="headingOne">
                                    <h6 class="mb-0">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#tim_kiem_theo_ten_san_pham" aria-expanded="true" aria-controls="collapseOne" style="text-decoration: none;">
                                            Tên sản phẩm
                                            <i class="now-ui-icons arrows-1_minimal-down"></i>
                                        </a>
                                    </h6>
                                </div>
                                <div id="tim_kiem_theo_ten_san_pham" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="card-body">
                                        <input type="text" name="tim_kiem_theo_ten" id="tim_kiem_theo_ten" value="<?php echo $tim_kiem_theo_ten ?>">
                                        <button type="submit">
                                            <i class="now-ui-icons ui-1_zoom-bold"></i>
                                        </button>
                                    </div>
                                </div>
                                <h5 class="card-title">
                                    Làm mới lựa chọn
                                    <button class="btn btn-default btn-icon btn-neutral pull-center" rel="tooltip" title="Làm mới" type="reset" value="Reset">
                                        <i class="arrows-1_refresh-69 now-ui-icons"></i>
                                    </button>
                                </h5>
                            </div>

                            <!-- Tìm kiếm theo lựa chọn -->
                            <?php

                            ?>
                            <div class="card-body">
                                <!-- <div class="card card-refine card-plain">
                                    <h4 class="card-title">
                                        Lọc sản phẩm
                                        <button class="btn btn-default btn-icon btn-neutral pull-right" rel="tooltip" title="Làm mới" type="reset">
                                            <i class="arrows-1_refresh-69 now-ui-icons"></i>
                                        </button>
                                    </h4>
                                    <div class="card-header" role="tab" id="headingOne">
                                        <h6 class="mb-0">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                Giới hạn giá sản phẩm
                                                <i class="now-ui-icons arrows-1_minimal-down"></i>
                                            </a>
                                        </h6>
                                    </div>
                                    <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="card-body">
                                            <span id="price-left" class="price-left pull-left" data-currency="&dollar;">100</span>
                                            <span id="price-right" class="price-right pull-right" data-currency="&dollar;">850</span>
                                            <div class="clearfix"></div>
                                            <div id="sliderRefine" class="slider slider-refine"></div>
                                        </div>
                                    </div>
                                </div> -->

                                <!-- Tìm kiếm theo loại sản phẩm -->
                                <div class="card card-refine card-plain">
                                    <div class="card-header" role="tab" id="headingTwo">
                                        <h6 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                Loại sản phẩm
                                                <i class="now-ui-icons arrows-1_minimal-down"></i>
                                            </a>
                                        </h6>
                                    </div>
                                    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="card-body">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                <input id="tat_ca_danh_muc" type="checkbox" checked="checked" value="0" name="ma_danh_muc" class="form-check-input">
                                                <span class="form-check-sign"></span>
                                                    Tất cả
                                                </label>
                                            </div>

                                    <?php
                                        $dem_danh_muc = 0;
                                        $lenh_lay_danh_muc = mysqli_query($ket_noi,"select * from danh_muc");
                                        while($danh_muc = mysqli_fetch_array($lenh_lay_danh_muc)){

                                            $dem_danh_muc++;
                                    ?>
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                <input id="<?php echo $danh_muc["ten_danh_muc"]; ?>" type="checkbox" value="<?php echo $dem_danh_muc; ?>" name="ma_danh_muc" class="form-check-input">
                                                <span class="form-check-sign"></span>
                                                    <?php echo $danh_muc["ten_danh_muc"]; ?>
                                                </label>
                                            </div>
                                    <?php
                                        }
                                    ?>
                                        </div>
                                    </div>
                                </div>

                                <!-- Tìm kiếm theo nhà sản xuất -->
                                <div class="card card-refine card-plain">
                                    <div class="card-header" role="tab" id="headingThree">
                                        <h6 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                Nhà sản xuất
                                                <i class="now-ui-icons arrows-1_minimal-down"></i>
                                            </a>
                                        </h6>
                                    </div>
                                    <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">

                                        <div class="card-body">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                <input id="tat_ca_danh_muc" type="checkbox" checked="checked" value="0" name="ma_nha_san_xuat" class="form-check-input">
                                                <span class="form-check-sign"></span>
                                                    Tất cả
                                                </label>
                                            </div>

                                    <?php
                                        $dem_nha_san_xuat = 0;
                                        $lenh_lay_nha_san_xuat = mysqli_query($ket_noi,"select * from nha_san_xuat");
                                        while($nha_san_xuat = mysqli_fetch_array($lenh_lay_nha_san_xuat)){

                                            $dem_nha_san_xuat++;
                                    ?>
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                <input id="<?php echo $danh_muc["ten_nha_san_xuat"]; ?>" type="checkbox" value="<?php echo $dem_nha_san_xuat; ?>" name="ma_nha_san_xuat" class="form-check-input">
                                                <span class="form-check-sign"></span>
                                                    <?php echo $nha_san_xuat["ten_nha_san_xuat"]; ?>
                                                </label>
                                            </div>
                                    <?php
                                        }
                                    ?>
                                        </div>
                                    </div>
                                </div>

                                <!-- Tìm kiếm theo màu -->
                                <!-- <div class="card card-refine card-plain">
                                    <div class="card-header" role="tab" id="headingfour">
                                        <h6 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                                                Colour
                                                <i class="now-ui-icons arrows-1_minimal-down"></i>
                                            </a>
                                        </h6>
                                    </div>
                                    <div id="collapsefour" class="collapse" role="tabpanel" aria-labelledby="headingfour">
                                        <div class="card-body">
                                            <div class="checkbox">
                                                <input id="checkbox27" type="checkbox">
                                                <label for="checkbox27">
                                                    All
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <input id="checkbox28" type="checkbox">
                                                <label for="checkbox28">
                                                    Black
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <input id="checkbox29" type="checkbox">
                                                <label for="checkbox29">
                                                    Blue
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <input id="checkbox30" type="checkbox">
                                                <label for="checkbox30">
                                                    Brown
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <input id="checkbox31" type="checkbox">
                                                <label for="checkbox31">
                                                    Gray
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <input id="checkbox32" type="checkbox">
                                                <label for="checkbox32">
                                                    Green
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <input id="checkbox33" type="checkbox">
                                                <label for="checkbox33">
                                                    Neutrals
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <input id="checkbox34" type="checkbox">
                                                <label for="checkbox34">
                                                    Purple
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                            </div>

                            </form>
                        </div>
                    </div>

                    <!-- Sản phẩm -->
                    <div class="col-md-9" id="san_pham">
                        <div class="row">

                        <!-- Lấy sản phẩm và phân trang -->
                        <?php

                            $lenh_dem_tim_kiem = mysqli_query($ket_noi,"select count(*) as dem_tim_kiem from san_pham where ten_san_pham like '%$tim_kiem_theo_ten%'");

                        if(isset($_GET["ma_danh_muc"])&&isset($_GET["ma_nha_san_xuat"])){
                            if($ma_danh_muc != 0 && $ma_nha_san_xuat != 0){
                                $lenh_dem_tim_kiem = mysqli_query($ket_noi,"select count(*) as dem_tim_kiem from san_pham where ten_san_pham like '%$tim_kiem_theo_ten%' and ma_danh_muc = $ma_danh_muc and ma_nha_san_xuat = $ma_nha_san_xuat");
                            }else if($ma_danh_muc != 0 && $ma_nha_san_xuat == 0){
                                $lenh_dem_tim_kiem = mysqli_query($ket_noi,"select count(*) as dem_tim_kiem from san_pham where ten_san_pham like '%$tim_kiem_theo_ten%' and ma_danh_muc = $ma_danh_muc");
                            }else if($ma_danh_muc == 0 && $ma_nha_san_xuat != 0){
                                $lenh_dem_tim_kiem = mysqli_query($ket_noi,"select count(*) as dem_tim_kiem from san_pham where ten_san_pham like '%$tim_kiem_theo_ten%' and ma_nha_san_xuat = $ma_nha_san_xuat");
                            }else{
                                $lenh_dem_tim_kiem = mysqli_query($ket_noi,"select count(*) as dem_tim_kiem from san_pham where ten_san_pham like '%$tim_kiem_theo_ten%'");
                            }
                        }
                                $lay_san_pham = mysqli_fetch_array($lenh_dem_tim_kiem);
                                $tong_san_pham = $lay_san_pham["dem_tim_kiem"];
                                if($tong_san_pham == 0){
                        ?>

                                    <div class="col-md-8 ml-auto mr-auto">
                                        <h4>Không tìm thấy sản phẩm nào</h4>
                                    </div>

                            <?php
                                }else{

                                while($san_pham = mysqli_fetch_array($lenh_phan_trang)){
                            ?>

                            <div class="col-lg-4 col-md-6">
                                <div class="card card-product card-plain">
                                    <div class="card-image">
                                        <a href="chi_tiet_san_pham.php?ma_san_pham=<?php echo $san_pham["ma_san_pham"] ?>&ma_danh_muc=<?php echo $san_pham["ma_danh_muc"] ?>">
                                            <img src="../img/<?php echo $san_pham["anh_san_pham"] ?>" alt="..." width="250px" height="250px"/>
                                        </a>
                                    </div>
                                    <div class="card-body">
                                        <a href="chi_tiet_san_pham.php?ma_san_pham=<?php echo $san_pham["ma_san_pham"] ?>&ma_danh_muc=<?php echo $san_pham["ma_danh_muc"] ?>">
                                            <h4 class="card-title"><?php echo $san_pham["ten_san_pham"] ?></h4>
                                        </a>
                                        <p class="card-description">
                                            <?php echo $san_pham["mo_ta"] ?>
                                        </p>
                                        <div class="card-footer">
                                            <div class="price-container">
                                                <span class="price"><?php echo number_format($san_pham["gia_san_pham"]); ?> VNĐ</span>
                                            </div>
                                            <a href="chi_tiet_san_pham.php?ma_san_pham=<?php echo $san_pham["ma_san_pham"] ?>&ma_danh_muc=<?php echo $san_pham["ma_danh_muc"] ?>">
                                                <button class="btn btn-danger btn-neutral btn-round pull-right" rel="tooltip" data-placement="left">
                                                    <i class="now-ui-icons design_bullet-list-67"></i> Xem chi tiết
                                                </button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- hết thẻ -->

                            <?php
                                }
                            // Đóng thẻ else
                                }
                            ?>

                        </div>

                        <!-- Nút phân trang -->
                        <div id="row">
                            <div class="col-md-3 ml-auto mr-auto">

                                <!-- Đoạn php phân trang -->
                                <?php
                                    $san_pham_1_trang = ceil($tong_san_pham/$limit);

                                    for ($i=1; $i<=$san_pham_1_trang ; $i++) {

                                        if(isset($_GET["ma_danh_muc"])&&isset($_GET["ma_nha_san_xuat"])){
                                ?>
                                <a href="?page=<?php echo $i; ?>&tim_kiem_theo_ten=<?php echo $tim_kiem_theo_ten; ?>&ma_danh_muc=<?php echo $ma_danh_muc; ?>&ma_nha_san_xuat=<?php echo $ma_nha_san_xuat; ?>#san_pham" style="text-decoration: none;">
                                    <button rel="tooltip" class="btn btn-primary btn-round">
                                        <?php echo $i; ?>
                                    </button>
                                </a>
                                <?php
                                        }else{
                                ?>
                                
                                <a href="?page=<?php echo $i; ?>&tim_kiem_theo_ten=<?php echo $tim_kiem_theo_ten; ?>#san_pham" style="text-decoration: none;">
                                    <button rel="tooltip" class="btn btn-primary btn-round">
                                        <?php echo $i; ?>
                                    </button>
                                </a>
                                
                                <?php
                                        }
                                    }
                                ?>

                            </div>
                        </div>
                    </div>
            <!-- đóng div container -->
            </div>
        <!-- đóng div section -->
        </div>

        <!-- Sản phẩm khuyến mãi -->
        <div class="section" id="san_pham_khuyen_mai">
            <div class="container">
                <h2 class="section-title">Sản Phẩm Khuyến Mãi</h2>
                <div class="row">
                    <div class="col-md-4">
                        <div class="card card-product card-plain">
                            <div class="card-image">
                                <img class="img rounded" src="../img/saint-laurent1.jpg" />
                            </div>
                            <div class="card-body">
                                <h4 class="card-title">
                                    <a href="#pablo">Áo Lông</a>
                                </h4>
                                <p class="card-description">Áo lông thời trang tạo nên thêm nét lãng tử cho người mặc.</p>
                                <div class="card-footer">
                                    <div class="price-container">
                                        <span class="price price-old">1 500 000 VNĐ</span>&nbsp
                                        <span class="price price-new">500 000 <small>VNĐ</small></span>
                                    </div>
                                    <div class="stats stats-right">
                                        <button type="button" rel="tooltip" title="" class="btn btn-icon btn-neutral" data-original-title="Thêm vào danh sách yêu thích">
                                            <i class="now-ui-icons ui-2_favourite-28"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-product card-plain">
                            <div class="card-image">
                                <img class="img rounded" src="../img/saint-laurent.jpg" />
                            </div>
                            <div class="card-body">
                                <h4 class="card-title">
                                    <h4 class="card-title">Áo Da</h4>
                                </h4>
                                <p class="card-description">Áo được làm từ chất liệu da bò với nhiều khóa kéo tạo thêm nét nam tính, khỏe khoắn cho người mặc.</p>
                                <div class="card-footer">
                                    <div class="price-container">
                                        <span class="price price-old">1 430 000 VNĐ</span>&nbsp
                                        <span class="price price-new">740 000 <small>VNĐ</small></span>
                                    </div>
                                    <div class="stats stats-right">
                                        <button type="button" rel="tooltip" title="" class="btn btn-icon btn-neutral" data-original-title="Thêm vào danh sách yêu thích">
                                            <i class="now-ui-icons ui-2_favourite-28"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-product card-plain">
                            <div class="card-image">
                                <img class="img rounded" src="../img/gucci.jpg" />
                            </div>
                            <div class="card-body">
                                <h4 class="card-title">
                                    <h4 class="card-title">Áo Gucci</h4>
                                </h4>
                                <p class="card-description">Sản phẩm được cung cấp bởi hãng thời trang Gucci và được làm từ chất liệu cao cấp.</p>
                                <div class="card-footer">
                                    <div class="price-container">
                                        <span class="price price-old">2 430 000 VNĐ</span>&nbsp
                                        <span class="price price-new">890 000 <small>VNĐ</small></span>
                                    </div>
                                    <div class="stats stats-right">
                                        <button type="button" rel="tooltip" title="" class="btn btn-icon btn-neutral btn-default" data-original-title="Thêm vào danh sách yêu thích">
                                            <i class="now-ui-icons ui-2_favourite-28"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Gửi mail -->
        <div class="subscribe-line subscribe-line-image" style="background-image: url('../img/bg43.jpg');" id="gui_mail">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 ml-auto mr-auto">
                        <div class="text-center">
                            <h4 class="title">Gửi phảm hồi cho shop</h4>
                            <p class="description">
                                Bạn hãy gửi những ý kiến của bản thân về sản phẩm cho shop tại đây
                            </p>
                        </div>
                        <div class="card card-raised card-form-horizontal">
                            <div class="card-body">
                                <form method="" action="">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="now-ui-icons ui-1_email-85"></i>
                                                </span>
                                                <input type="email" class="form-control" placeholder="Nhập phản hồi của bạn...">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <button type="button" class="btn btn-primary btn-block">Gửi</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end-main-raised -->

    <!-- Phần footer -->
    <?php
        include '../template/footer.php';
        include '../connecting/close.php';
    ?>

</div>
</body>

<!--   Core JS Files   -->
<script src="../js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="../js/core/popper.min.js" type="text/javascript"></script>
<script src="../js/core/bootstrap.min.js" type="text/javascript"></script>
<script src="../js/plugins/moment.min.js"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="../js/plugins/bootstrap-switch.js"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="../js/plugins/bootstrap-tagsinput.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="../js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="../js/plugins/jasny-bootstrap.min.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../js/plugins/nouislider.min.js" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="../js/plugins/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="../js/now-ui-kit.js?v=1.1.0" type="text/javascript"></script>
<!-- <script type="text/javascript">
    $(document).ready(function() {

        var slider2 = document.getElementById('sliderRefine');

        noUiSlider.create(slider2, {
            start: [42, 880],
            connect: true,
            range: {
                'min': [30],
                'max': [900]
            }
        });

        var limitFieldMin = document.getElementById('price-left');
        var limitFieldMax = document.getElementById('price-right');

        slider2.noUiSlider.on('update', function(values, handle) {
            if (handle) {
                limitFieldMax.innerHTML = $('#price-right').data('currency') + Math.round(values[handle]);
            } else {
                limitFieldMin.innerHTML = $('#price-left').data('currency') + Math.round(values[handle]);
            }
        });
    });
</script> -->

</html>
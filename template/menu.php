<!-- Nav menu -->
    <nav class="navbar navbar-expand-lg bg-white fixed-top navbar-transparent" color-on-scroll="500">
        <div class="container">

            <!-- Icon website -->
            <div class="navbar-translate">
                <a class="navbar-brand" href="../trang_chu/trang_chu.php" rel="tooltip" title="Designed by Invision. Coded by Creative Laam-firstking99" data-placement="bottom" target=”_blank”>
                    <img src="../img/icon_project1.jpg" width="80px" height="70px">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                </button>
            </div>

            <!-- Menu -->
            <div class="collapse navbar-collapse" data-nav-image="../img/blurred-image-1.jpg" data-color="orange">
                <ul class="navbar-nav ml-auto">

                    <!-- Trang chủ -->
                    <li class="nav-item">
                        <a class="nav-link" href="../trang_chu/trang_chu.php" target="_blank">
                            <i class="now-ui-icons shopping_shop"></i>
                            <p>Trang Chủ</p>
                        </a>
                    </li>

                    <!-- Sản phẩm -->
                    <li class="nav-item">
                        <a class="nav-link" href="../trang_chu/trang_chu.php#san_pham">
                            <i class="now-ui-icons shopping_box"></i>
                            <p>Sản Phẩm</p>
                        </a>
                    </li>
                        <!-- <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="../sections.html#headers">
                                <i class="now-ui-icons shopping_tag-content"></i> Áo
                            </a>
                            <a class="dropdown-item" href="../sections.html#features">
                                <i class="now-ui-icons shopping_tag-content"></i> Quần
                            </a>
                            <a class="dropdown-item" href="../sections.html#blogs">
                                <i class="now-ui-icons text_align-left"></i> Blogs
                            </a>
                            <a class="dropdown-item" href="../sections.html#teams">
                                <i class="now-ui-icons sport_user-run"></i> Teams
                            </a>
                            <a class="dropdown-item" href="../sections.html#projects">
                                <i class="now-ui-icons education_paper"></i> Projects
                            </a>
                            <a class="dropdown-item" href="../sections.html#pricing">
                                <i class="now-ui-icons business_money-coins"></i> Pricing
                            </a>
                            <a class="dropdown-item" href="../sections.html#testimonials">
                                <i class="now-ui-icons ui-2_chat-round"></i> Testimonials
                            </a>
                            <a class="dropdown-item" href="../sections.html#contactus">
                                <i class="now-ui-icons tech_mobile"></i> Contact Us
                            </a>
                        </div> -->
                    </li>

                    <!-- Giới thiệu -->
                    <li class="nav-item">
                        <a class="nav-link" href="../gioi_thieu/gioi_thieu.php" target=”_blank”>
                            <i class="now-ui-icons education_glasses"></i>
                            <p>Giới thiệu</p>
                        </a>
                    </li>
                    
                    <!-- Tin tức -->
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown">
                            <i class="now-ui-icons education_paper" aria-hidden="true"></i>
                            <p>Tin tức</p>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="../trang_chu/trang_chu.php#tin_thoi_trang">
                                <i class="now-ui-icons education_agenda-bookmark"></i> Tin thời trang
                            </a>
                            <a class="dropdown-item" href="../trang_chu/trang_chu.php#san_pham_khuyen_mai">
                                <i class="now-ui-icons shopping_bag-16"></i> Sản phẩm khuyến mãi
                            </a>
                        </div>
                    </li>

                    <!-- list -->
                   <!--  <li class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown">
                            <i class="now-ui-icons design_image" aria-hidden="true"></i>
                            <p>Gio hang</p>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="../examples/about-us.html">
                                <i class="now-ui-icons business_bulb-63"></i> About-us
                            </a>
                            <a class="dropdown-item" href="../examples/blog-post.html">
                                <i class="now-ui-icons text_align-left"></i> Blog Post
                            </a>
                            <a class="dropdown-item" href="../examples/blog-posts.html">
                                <i class="now-ui-icons design_bullet-list-67"></i> Blog Posts
                            </a>
                            <a class="dropdown-item" href="../examples/contact-us.html">
                                <i class="now-ui-icons location_pin"></i> Contact Us
                            </a>
                            <a class="dropdown-item" href="../examples/landing-page.html">
                                <i class="now-ui-icons education_paper"></i> Landing Page
                            </a>
                            <a class="dropdown-item" href="../examples/login-page.html">
                                <i class="now-ui-icons users_circle-08"></i> Login Page
                            </a>
                            <a class="dropdown-item" href="../examples/pricing.html">
                                <i class="now-ui-icons business_money-coins"></i> Pricing
                            </a>
                            <a class="dropdown-item" href="../examples/ecommerce.html">
                                <i class="now-ui-icons shopping_shop"></i> Ecommerce Page
                            </a>
                            <a class="dropdown-item" href="../examples/product-page.html">
                                <i class="now-ui-icons shopping_bag-16"></i> Product Page
                            </a>
                            <a class="dropdown-item" href="../examples/profile-page.html">
                                <i class="now-ui-icons users_single-02"></i> Profile Page
                            </a>
                            <a class="dropdown-item" href="../examples/signup-page.html">
                                <i class="now-ui-icons tech_mobile"></i> Signup Page
                            </a>
                        </div>
                    </li> -->

                    <!-- Liên hệ -->
                    <li class="nav-item">
                        <a class="nav-link" href="../trang_chu/trang_chu.php#gui_mail">
                            <i class="now-ui-icons tech_laptop"></i>
                            <p>Liên Hệ</p>
                        </a>
                    </li>

                    <!-- Giỏ hàng -->
                    <li class="nav-item">
                        <a class="nav-link btn btn-primary" href="../gio_hang/gio_hang.php" target=”_blank”>
                            <i class="now-ui-icons shopping_cart-simple"></i> Giỏ hàng
                        </a>
                    </li>

            <?php
                if(isset($_SESSION["tai_khoan_khach_hang"])){
            ?>

                    <!-- Quản lý tài khoản -->
                    <li class="nav-item">
                        <a class="nav-link" href="../tai_khoan_khach_hang/quan_ly_tai_khoan.php">
                            <i class="now-ui-icons users_single-02"></i>
                            <p>Quản lý tài khoản</p>
                        </a>
                    </li>

                    <!-- Đăng xuất -->
                    <li class="nav-item">
                        <a class="nav-link" href="../tai_khoan_khach_hang/dang_xuat.php">
                            <i class="now-ui-icons ui-1_simple-remove"></i>
                            <p>Đăng Xuất</p>
                        </a>
                    </li>

            <?php
                }else{
            ?>

                    <!-- Đăng nhập -->
                    <li class="nav-item">
                        <a class="nav-link" href="../tai_khoan_khach_hang/dang_nhap.php">
                            <i class="now-ui-icons users_circle-08"></i>
                            <p>Đăng Nhập</p>
                        </a>
                    </li>

                    <!-- Đăng ký -->
                    <li class="nav-item">
                        <a class="nav-link" href="../tai_khoan_khach_hang/dang_ky.php">
                            <i class="now-ui-icons files_paper"></i>
                            <p>Đăng Ký</p>
                        </a>
                    </li>

            <?php
                }
            ?>

                    <!-- <li class="nav-item">
					<a class="nav-link" rel="tooltip" title="Follow us on Twitter" data-placement="bottom" href="https://twitter.com/CreativeTim" target="_blank">
						<i class="fa fa-twitter"></i>
						<p class="hidden-lg-up">Twitter</p>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" rel="tooltip" title="Like us on Facebook" data-placement="bottom" href="https://www.facebook.com/CreativeTim" target="_blank">
						<i class="fa fa-facebook-square"></i>
						<p class="hidden-lg-up">Facebook</p>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" rel="tooltip" title="Follow us on Instagram" data-placement="bottom" href="https://www.instagram.com/CreativeTimOfficial" target="_blank">
						<i class="fa fa-instagram"></i>
						<p class="hidden-lg-up">Instagram</p>
					</a>
				</li> -->
            </ul>
        </div>
    </div>
</nav>
<!-- Hết thanh menu -->
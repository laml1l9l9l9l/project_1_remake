<footer class="footer ">
    <div class="container">
        <nav>
            <ul>
                <!-- Lien he -->
                <!-- <li>
                    <a href="https://www.creative-tim.com">
                        Creative Tim
                    </a>
                </li>
                <li>
                    <a href="http://presentation.creative-tim.com">
                        About Us
                    </a>
                </li>
                <li>
                    <a href="http://blog.creative-tim.com">
                        Blog
                    </a>
                </li>
                <li>
                    <a href="https://www.creative-tim.com/license">
                        License
                    </a>
                </li> -->

                <!-- Social media -->
                <li>
                    <a class="nav-link" rel="tooltip" title="Theo dõi Twitter của chúng tôi" data-placement="bottom" href="https://twitter.com/CreativeTim" target="_blank">
                        <i class="fa fa-twitter"></i>
                        <p class="hidden-lg-up">Twitter</p>
                    </a>
                </li>
                <li>
                    <a class="nav-link" rel="tooltip" title="Theo dõi Facebook của chúng tôi" data-placement="bottom" href="https://www.facebook.com/CreativeTim" target="_blank">
                        <i class="fa fa-facebook-square"></i>
                        <p class="hidden-lg-up">Facebook</p>
                    </a>
                </li>
                <li>
                    <a class="nav-link" rel="tooltip" title="Theo dõi Instagram của chúng tôi" data-placement="bottom" href="https://www.instagram.com/CreativeTimOfficial" target="_blank">
                        <i class="fa fa-instagram"></i>
                        <p class="hidden-lg-up">Instagram</p>
                    </a>
                </li>
            </ul>
        </nav>
        <div class="copyright">
            &copy;
            <script>
                document.write(new Date().getFullYear())
            </script>, Designed by
            <a href="http://www.invisionapp.com" target="_blank">Invision</a>. Coded by
            <a href="https://www.facebook.com/laamnguyentung" target="_blank">Laam-firstking99</a>. Version 0.0.1
        </div>
    </div>
 </footer>
function form_dang_ky(){
	var dem_loi = 0;

	// regex tài khoản
	var tai_khoan = document.getElementById('tai_khoan').value;
	var regex_tai_khoan = /^[a-z0-9\-\_]{3,25}$/;
	var kiem_tra_tai_khoan = regex_tai_khoan.test(tai_khoan);
	if(tai_khoan.length == 0){
		document.getElementById('loi_tai_khoan').innerHTML = "Bạn không được để trống";
		dem_loi = 1;
	}else{
		if(kiem_tra_tai_khoan){
			document.getElementById('loi_tai_khoan').innerHTML = "";
		}else{
			document.getElementById('loi_tai_khoan').innerHTML = "Bạn nhập sai định dạng";
			dem_loi = 1;
		}
	}

	// regex mật khẩu
	var mat_khau = document.getElementById('mat_khau').value;
	var regex_mat_khau = /^([A-Za-z1-9\!\@\#\$\%\^\&\*\~\`]){4,30}$/;
	var kiem_tra_mat_khau = regex_mat_khau.test(mat_khau);
	if(mat_khau.length == 0){
		document.getElementById('loi_mat_khau').innerHTML = "Bạn không được để trống";
		dem_loi = 1;
	}else{
		if(kiem_tra_mat_khau){
			document.getElementById('loi_mat_khau').innerHTML = "";
		}else{
			document.getElementById('loi_mat_khau').innerHTML = "Bạn nhập sai định dạng";
			dem_loi = 1;
		}
	}

	// regex họ tên
	var ho_ten = document.getElementById('ho_ten').value;
	var regex_ho_ten = /^([A-Z][\']?[a-z]{1,7}[\ ]){1,5}[A-Z][\']?[a-z]{1,7}$/;
	var kiem_tra_ho_ten = regex_ho_ten.test(ho_ten);
	if(ho_ten.length == 0){
		document.getElementById('loi_ho_ten').innerHTML = "Bạn không được để trống";
		dem_loi = 1;
	}else{
		if(kiem_tra_ho_ten){
			document.getElementById('loi_ho_ten').innerHTML = "";
		}else{
			document.getElementById('loi_ho_ten').innerHTML = "Bạn nhập sai định dạng";
			dem_loi = 1;
		}
	}

	// regex ngày - tháng - năm
	var nam_sinh = document.getElementById('nam_sinh').value;
	var thang_sinh = document.getElementById('thang_sinh').value;
	var ngay_sinh = document.getElementById('ngay_sinh').value;

	if((nam_sinh == 0) || (thang_sinh == 0) || (ngay_sinh == 0)){
		document.getElementById('loi_ngay_thang_nam').innerHTML = "Bạn không được để trống";
		dem_loi = 1;
	}else{
		document.getElementById('loi_ngay_thang_nam').innerHTML = "";
	}

	// regex số điện thoại
	var so_dien_thoai = document.getElementById('so_dien_thoai').value;
	var regex_so_dien_thoai = /^[\+]?[0-9]{10,11}$/;
	var kiem_tra_so_dien_thoai = regex_so_dien_thoai.test(so_dien_thoai);
	if(so_dien_thoai.length == 0){
		document.getElementById('loi_so_dien_thoai').innerHTML = "Bạn không được để trống";
		dem_loi = 1;
	}else{
		if(kiem_tra_so_dien_thoai){
			document.getElementById('loi_so_dien_thoai').innerHTML = "";
		}else{
			document.getElementById('loi_so_dien_thoai').innerHTML = "Bạn nhập sai định dạng";
			dem_loi = 1;
		}
	}

	// regex địa chỉ
	var dia_chi = document.getElementById('dia_chi').value;
	var regex_dia_chi = /^[a-z0-9]+[\,\. a-z0-9]*[a-z0-9]$/;
	var kiem_tra_dia_chi = regex_dia_chi.test(dia_chi);
	if(dia_chi.length == 0){
		document.getElementById('loi_dia_chi').innerHTML = "Bạn không được để trống";
		dem_loi = 1;
	}else{
		if(kiem_tra_dia_chi){
			document.getElementById('loi_dia_chi').innerHTML = "";
		}else{
			document.getElementById('loi_dia_chi').innerHTML = "Bạn nhập sai định dạng";
			dem_loi = 1;
		}
	}

	if(dem_loi == 1){
		return false;
	}else{
		return true;
	}
}
function form_cap_nhat_thong_tin(){
	var dem_loi = 0;

	// Số điện thoại
	var so_dien_thoai = document.getElementById('so_dien_thoai').value;
	var regex_so_dien_thoai = /^[\+]?[0-9]{10,11}$/;
	var kiem_tra_so_dien_thoai = regex_so_dien_thoai.test(so_dien_thoai);
	if(so_dien_thoai.length == 0){
		document.getElementById('loi_so_dien_thoai').innerHTML = "Bạn không được để trống";
		dem_loi = 1;
	}
	else{
		if(so_dien_thoai.length < 10){
			document.getElementById('loi_so_dien_thoai').innerHTML = "Số điện thoại của bạn phải nhiều hơn 10 ký tự";
			dem_loi = 1;
		}else if(so_dien_thoai.length > 12){
			document.getElementById('loi_so_dien_thoai').innerHTML = "Số điện thoại của bạn không được quá 12 ký tự";
			dem_loi = 1;
		}else{
			if(kiem_tra_tai_khoan){
				document.getElementById('loi_so_dien_thoai').innerHTML = "";
			}else{
				document.getElementById('loi_so_dien_thoai').innerHTML = "Bạn nhập sai định dạng - Không được sử dụng ký tự đặc biệt";
				dem_loi = 1;
			}
		}
	}

	// Email
	var email = document.getElementById('email').value;
	var regex_email = /^[a-zA-Z0-9\_\-]+\@[a-zA-Z0-9]+\.([a-zA-Z]{2,3}\.)?[a-zA-Z]{2,3}$/;
	var kiem_tra_email = regex_email.test(email);
	if(email.length == 0){
		document.getElementById('loi_email').innerHTML = "Bạn không được để trống";
		dem_loi = 1;
	}else{
		if(kiem_tra_email){
			document.getElementById('loi_email').innerHTML = "";
		}else{
			document.getElementById('loi_email').innerHTML = "Bạn nhập sai định dạng - abc@gmail.com";
			dem_loi = 1;
		}
	}

	// Địa chỉ
	var dia_chi = document.getElementById('dia_chi').value;
	var regex_dia_chi = /^[a-zA-Z0-9\_\-]+\@[a-zA-Z0-9]+\.([a-zA-Z]{2,3}\.)?[a-zA-Z]{2,3}$/;
	var kiem_tra_dia_chi = regex_dia_chi.test(dia_chi);
	if(dia_chi.length == 0){
		document.getElementById('loi_dia_chi').innerHTML = "Bạn không được để trống";
		dem_loi = 1;
	}else{
		if(kiem_tra_dia_chi){
			document.getElementById('loi_dia_chi').innerHTML = "";
		}else{
			document.getElementById('loi_dia_chi').innerHTML = "Bạn nhập sai định dạng - số 1, đường Giải Phóng, quận Hai Bà Trưng, Hà Nội";
			dem_loi = 1;
		}
	}

	if(dem_loi == 1){
		return false;
	}else{
		return true;
	}
}
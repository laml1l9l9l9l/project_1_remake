function form_dang_nhap(){
	var dem_loi = 0;

	var tai_khoan = document.getElementById('tai_khoan').value;
	var regex_tai_khoan = /^[a-z0-9\-\_]{3,25}$/;
	var kiem_tra_tai_khoan = regex_tai_khoan.test(tai_khoan);
	if(tai_khoan.length == 0){
		document.getElementById('loi_tai_khoan').innerHTML = "Bạn không được để trống";
		dem_loi = 1;
	}
	else{
		if(tai_khoan.length < 3){
			document.getElementById('loi_tai_khoan').innerHTML = "Tài khoản của bạn phải nhiều hơn 2 ký tự";
			dem_loi = 1;
		}else if(tai_khoan.length > 25){
			document.getElementById('loi_tai_khoan').innerHTML = "Tài khoản của bạn không được quá 25 ký tự";
			dem_loi = 1;
		}else{
			if(kiem_tra_tai_khoan){
				document.getElementById('loi_tai_khoan').innerHTML = "";
			}else{
				document.getElementById('loi_tai_khoan').innerHTML = "Bạn nhập sai định dạng - Không được sử dụng ký tự đặc biệt";
				dem_loi = 1;
			}
		}
	}

	var mat_khau = document.getElementById('mat_khau').value;
	var regex_mat_khau = /^([A-Za-z1-9\!\@\#\$\%\^\&\*\~\`\+\=\-\_]){4,50}$/;
	var kiem_tra_mat_khau = regex_mat_khau.test(mat_khau);
	if(mat_khau.length == 0){
		document.getElementById('loi_mat_khau').innerHTML = "Bạn không được để trống";
		dem_loi = 1;
	}else{
		if(mat_khau.length < 4){
			document.getElementById('loi_mat_khau').innerHTML = "Mật khẩu của bạn không được ngắn hơn 4 ký tự";
			dem_loi = 1;
		}else if(mat_khau.length > 50){
			document.getElementById('loi_mat_khau').innerHTML = "Mật khẩu của bạn không được dài quá 50 ký tự";
			dem_loi = 1;
		}else{
			if(kiem_tra_mat_khau){
				document.getElementById('loi_mat_khau').innerHTML = "";
			}else{
				document.getElementById('loi_mat_khau').innerHTML = "Bạn nhập sai định dạng - Không được sử dụng khoảng trống";
				dem_loi = 1;
			}
		}
	}

	if(dem_loi == 1){
		return false;
	}else{
		return true;
	}
}
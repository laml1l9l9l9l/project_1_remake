<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title> Trang Chủ </title>
    
    <?php
        include '../template/head_link.php';
    ?>

</head>

<body class="ecommerce-page">
    
<!-- Menu -->
<?php
    include '../template/menu.php';
?>

<!-- Slide Show -->
<div class="wrapper">
    <div id="carouselExampleIndicators" class="carousel slide">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class=""></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2" class=""></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="carousel-item">
                <div class="page-header header-filter">
                    <div class="page-header-image" style="background-image: url('../img/bg40.jpg');"></div>
                    <div class="content-center text-center">
                        <div class="row">
                            <div class="col-md-8 ml-auto mr-auto2">
                                <h1 class="title">Thời trang công sở</h1>
                                <h4 class="description text-white">Trang phục công sở theo phong cách Paris</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item active">
                <div class="page-header header-filter">
                    <div class="page-header-image" style="background-image: url('../img/bg41.jpg');"></div>
                    <div class="content-center">
                        <div class="row">
                            <div class="col-md-8 ml-auto mr-auto text-center">
                                <h1 class="title">Phong cách đường phố</h1>
                                <h4 class="description text-white">Những bộ trang phục dạo phố được thiết kế đơn giản mang lại nét khỏe khoắn, hấp dẫn</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="page-header header-filter">
                    <div class="page-header-image" style="background-image: url('../img/bg29.jpg');"></div>
                    <div class="content-center text-center">
                        <div class="row">
                            <div class="col-md-8 ml-auto mr-auto">
                                <h1 class="title">Thời trang cho nam</h1>
                                <h4 class="description text-white">Những bộ cánh thiết kế cho nam giới the nhiều phong cách khác nhau tạo ra nhiều sự lựa chọn cho khách hàng</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <i class="now-ui-icons arrows-1_minimal-left"></i>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <i class="now-ui-icons arrows-1_minimal-right"></i>
        </a>
    </div>

    <!-- Phần chính -->
    <div class="main">
        <div class="section" id="san_pham">
            <div class="container">

                <!-- Sản phẩm -->
                <h2 class="section-title">Sản phẩm mới nhất</h2>

                    <div class="col-md-12">
                        <div class="row">

                            <!-- Lấy sản phẩm -->
                            <?php
                                include '../connecting/open.php';
                                $lenh = mysqli_query($ket_noi,"select * from san_pham order by ma_san_pham desc limit 6 ");
                                while($san_pham = mysqli_fetch_array($lenh)){
                            ?>

                            <div class="col-lg-4 col-md-6">
                                <div class="card card-product card-plain">
                                    <div class="card-image">
                                        <a href="../san_pham/chi_tiet_san_pham.php?ma_san_pham=<?php echo $san_pham["ma_san_pham"] ?>&ma_danh_muc=<?php echo $san_pham["ma_danh_muc"] ?>">
                                            <img src="../img/<?php echo $san_pham["anh_san_pham"] ?>" alt="..." width="250px" height="250px"/>
                                        </a>
                                    </div>
                                    <div class="card-body">
                                        <a href="../san_pham/chi_tiet_san_pham.php?ma_san_pham=<?php echo $san_pham["ma_san_pham"] ?>&ma_danh_muc=<?php echo $san_pham["ma_danh_muc"] ?>">
                                            <h4 class="card-title"><?php echo $san_pham["ten_san_pham"] ?></h4>
                                        </a>
                                        <p class="card-description">
                                            <?php echo $san_pham["mo_ta"] ?>
                                        </p>
                                        <div class="card-footer">
                                            <div class="price-container">
                                                <span class="price"><?php echo number_format($san_pham["gia_san_pham"]); ?> VNĐ</span>
                                            </div>
                                            <a href="../san_pham/chi_tiet_san_pham.php?ma_san_pham=<?php echo $san_pham["ma_san_pham"] ?>&ma_danh_muc=<?php echo $san_pham["ma_danh_muc"] ?>">
                                                <button class="btn btn-danger btn-neutral btn-round pull-right" rel="tooltip" data-placement="left">
                                                    <i class="now-ui-icons design_bullet-list-67"></i> Xem chi tiết
                                                </button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- hết thẻ -->

                            <?php
                                }
                            ?>

                        </div>

                        <!-- Nut xem tiếp -->
                        <div id="row">
                            <div class="col-md-3 ml-auto mr-auto">
                                <a href="../san_pham/san_pham.php#san_pham">
                                    <button rel="tooltip" class="btn btn-primary btn-round">
                                        Xem tiếp...
                                        <i class="now-ui-icons emoticons_satisfied"></i>
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
            <!-- đóng div container -->
            </div>
        <!-- đóng div section -->
        </div>

        <!-- Tin tức -->
        <div class="container">
            <h2 class="section-title" id="tin_thoi_trang">Tin thời trang mới</h2>
        </div>
        <div class="projects-4">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 px-0">
                        <div class="card card-fashion card-background" style="background-image: url('../img/bg35.jpg')">
                            <div class="card-body">
                                <div class="card-title text-left">
                                    <h2>
                                        <a href="#link">
                                            Chọn Vest dự tiệc phong cách thời trang Đông Xuân - Tạp chí thời trang của tuần
                                        </a>
                                    </h2>
                                </div>
                                <div class="card-footer text-left">
                                    <div class="stats">
                                        <span>
                                            <i class="now-ui-icons users_circle-08"></i>Emy Grace
                                        </span>
                                        <span>
                                            <i class="now-ui-icons tech_watch-time"></i> January 20, 2019
                                        </span>
                                    </div>
                                    <div class="stats-link pull-right">
                                        <a href="#link" class="footer-link">Thời trang của tháng</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 px-0">
                        <div class="card-container">
                            <div class="card card-fashion">
                                <div class="card-title">
                                    <a href="#link">
                                    </a>
                                    <h4>
                                        <a href="#link">
                                        </a>
                                        <a href="#link">
                                            Xu hướng thời trang trong tết Kỷ Hợi 2019
                                        </a>
                                    </h4>
                                </div>
                                <div class="card-body">
                                    <div class="card-footer text-left">
                                        <div class="stats">
                                            <span>
                                                <i class="now-ui-icons users_circle-08"></i>Nguyễn Tùng Lâm
                                            </span>
                                            <span>
                                                <i class="now-ui-icons tech_watch-time"></i> January 25, 2019
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-fashion card-background" style="background-image: url('../img/anh_trang_phuc_tet.jpg')">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Sản phẩm khuyến mãi -->
        <div class="section" id="san_pham_khuyen_mai">
            <div class="container">
                <h2 class="section-title">Sản Phẩm Khuyến Mãi</h2>
                <div class="row">
                    <div class="col-md-4">
                        <div class="card card-product card-plain">
                            <div class="card-image">
                                <img class="img rounded" src="../img/saint-laurent1.jpg" />
                            </div>
                            <div class="card-body">
                                <h4 class="card-title">
                                    <a href="#pablo">Áo Lông</a>
                                </h4>
                                <p class="card-description">Áo lông thời trang tạo nên thêm nét lãng tử cho người mặc.</p>
                                <div class="card-footer">
                                    <div class="price-container">
                                        <span class="price price-old">1 500 000 VNĐ</span>&nbsp
                                        <span class="price price-new">500 000 <small>VNĐ</small></span>
                                    </div>
                                    <div class="stats stats-right">
                                        <button type="button" rel="tooltip" title="" class="btn btn-icon btn-neutral" data-original-title="Thêm vào danh sách yêu thích">
                                            <i class="now-ui-icons ui-2_favourite-28"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-product card-plain">
                            <div class="card-image">
                                <img class="img rounded" src="../img/saint-laurent.jpg" />
                            </div>
                            <div class="card-body">
                                <h4 class="card-title">
                                    <h4 class="card-title">Áo Da</h4>
                                </h4>
                                <p class="card-description">Áo được làm từ chất liệu da bò với nhiều khóa kéo tạo thêm nét nam tính, khỏe khoắn cho người mặc.</p>
                                <div class="card-footer">
                                    <div class="price-container">
                                        <span class="price price-old">1 430 000 VNĐ</span>&nbsp
                                        <span class="price price-new">740 000 <small>VNĐ</small></span>
                                    </div>
                                    <div class="stats stats-right">
                                        <button type="button" rel="tooltip" title="" class="btn btn-icon btn-neutral" data-original-title="Thêm vào danh sách yêu thích">
                                            <i class="now-ui-icons ui-2_favourite-28"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-product card-plain">
                            <div class="card-image">
                                <img class="img rounded" src="../img/gucci.jpg" />
                            </div>
                            <div class="card-body">
                                <h4 class="card-title">
                                    <h4 class="card-title">Áo Gucci</h4>
                                </h4>
                                <p class="card-description">Sản phẩm được cung cấp bởi hãng thời trang Gucci và được làm từ chất liệu cao cấp.</p>
                                <div class="card-footer">
                                    <div class="price-container">
                                        <span class="price price-old">2 430 000 VNĐ</span>&nbsp
                                        <span class="price price-new">890 000 <small>VNĐ</small></span>
                                    </div>
                                    <div class="stats stats-right">
                                        <button type="button" rel="tooltip" title="" class="btn btn-icon btn-neutral btn-default" data-original-title="Thêm vào danh sách yêu thích">
                                            <i class="now-ui-icons ui-2_favourite-28"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="row">
                    <div class="col-md-3 ml-auto mr-auto">
                        <a href="../san_pham/san_pham.php#san_pham_khuyen_mai">
                            <button rel="tooltip" class="btn btn-primary btn-round">
                                Xem tiếp...
                                <i class="now-ui-icons emoticons_satisfied"></i>
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Gửi mail -->
        <div class="subscribe-line subscribe-line-image" style="background-image: url('../img/bg43.jpg');" id="gui_mail">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 ml-auto mr-auto">
                        <div class="text-center">
                            <h4 class="title">Gửi phảm hồi cho shop</h4>
                            <p class="description">
                                Bạn hãy gửi những ý kiến của bản thân về sản phẩm cho shop tại đây
                            </p>
                        </div>
                        <div class="card card-raised card-form-horizontal">
                            <div class="card-body">
                                <form method="" action="">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="now-ui-icons ui-1_email-85"></i>
                                                </span>
                                                <input type="email" class="form-control" placeholder="Nhập phản hồi của bạn...">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <button type="button" class="btn btn-primary btn-block">
                                                Gửi
                                                <i class="now-ui-icons ui-1_send"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end-main-raised -->

    <!-- Phần footer -->
    <?php
        include '../template/footer.php';
        include '../connecting/close.php';
    ?>

</div>
</body>

<!--   Core JS Files   -->
<script src="../js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="../js/core/popper.min.js" type="text/javascript"></script>
<script src="../js/core/bootstrap.min.js" type="text/javascript"></script>
<script src="../js/plugins/moment.min.js"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="../js/plugins/bootstrap-switch.js"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="../js/plugins/bootstrap-tagsinput.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="../js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="../js/plugins/jasny-bootstrap.min.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../js/plugins/nouislider.min.js" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="../js/plugins/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="../js/now-ui-kit.js?v=1.1.0" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {

        var slider2 = document.getElementById('sliderRefine');

        noUiSlider.create(slider2, {
            start: [42, 880],
            connect: true,
            range: {
                'min': [30],
                'max': [900]
            }
        });

        var limitFieldMin = document.getElementById('price-left');
        var limitFieldMax = document.getElementById('price-right');

        slider2.noUiSlider.on('update', function(values, handle) {
            if (handle) {
                limitFieldMax.innerHTML = $('#price-right').data('currency') + Math.round(values[handle]);
            } else {
                limitFieldMin.innerHTML = $('#price-left').data('currency') + Math.round(values[handle]);
            }
        });
    });
</script>

</html>
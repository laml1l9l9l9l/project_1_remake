<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Đăng Ký</title>
    <?php
        include '../template/head_link.php';
    ?>

</head>

<body class="signup-page">

    <!-- Menu -->
    <?php
        include '../template/menu.php';
    ?>

    <div class="page-header section-image" style="position: relative; overflow:hidden;">
        <div class="page-header-image" style="background-image:url(../img/bg18.jpg);"></div>
        <div class="content-center" style="position: absolute; margin-top: 20px; bottom: -370px;">
            <div class="container">
                <div class="row">

                    <!-- Div đăng lý -->
                    <div class="col-md-6">
                        <div class="card card-signup">
                            <div class="card-body">
                                <h4 class="card-title text-center">Đăng Ký</h4>

                                <!-- Đăng nhập bằng mạng xã hội -->
                                <!-- <div class="social text-center">
                                    <button class="btn btn-icon btn-round btn-twitter">
                                        <i class="fa fa-twitter"></i>
                                    </button>
                                    <button class="btn btn-icon btn-round btn-youtube">
                                        <i class="fa fa-youtube"></i>
                                    </button>
                                    <button class="btn btn-icon btn-round btn-facebook">
                                        <i class="fa fa-facebook"> </i>
                                    </button>
                                    <h5 class="card-description"> or be classical </h5>
                                </div> -->

                                <!-- Form đăng ký -->
                                <form class="form" method="post" action="xu_ly_dang_ky.php" id="form_dang_ky">

                                    <!-- Tài khoản -->
                                    <div style="position: relative;">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="now-ui-icons users_circle-08"></i>
                                            </span>
                                            <input type="text" class="form-control" placeholder="Tài khoản..." name="tai_khoan" id="tai_khoan">
                                        </div>

                                        <!-- Báo lỗi tài khoản -->
                                        <div style="position: absolute; top: 31px; left: 0; right: 0; margin-left: auto; margin-right: auto;">
                                            <small id="loi_tai_khoan" style="margin: auto; color: red;"></small>
                                        </div>
                                    </div>

                                    <!-- Mật khẩu -->
                                    <div style="position: relative;">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="now-ui-icons objects_key-25"></i>
                                            </span>
                                            <input type="password" placeholder="Mật khẩu..." class="form-control" name="mat_khau" id="mat_khau">
                                        </div>

                                        <!-- Báo lỗi mật khẩu -->
                                        <div style="position: absolute; top: 31px; left: 0; right: 0; margin-left: auto; margin-right: auto;">
                                            <small id="loi_mat_khau" style="margin: auto; color: red;"></small>
                                        </div>
                                    </div>

                                    <!-- Họ và tên -->
                                    <div style="position: relative;">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="now-ui-icons users_single-02"></i>
                                            </span>
                                            <input type="text" class="form-control" placeholder="Họ và tên..." name="ho_ten" id="ho_ten">
                                        </div>

                                        <!-- Báo lỗi họ tên -->
                                        <div style="position: absolute; top: 31px; left: 0; right: 0; margin-left: auto; margin-right: auto;">
                                            <small id="loi_ho_ten" style="margin: auto; color: red;"></small>
                                        </div>
                                    </div>

                                    <!-- Ngày sinh -->
                                    <div style="position: relative;">
                                        <div class="input-group">
                                            <div>
                                                <select id="nam_sinh" class="custom-select" name="nam_sinh">
                                                    <option value="0" >Năm</option>
                                                </select>
                                            </div>
                                            <div>
                                                <select id="thang_sinh" class="custom-select" name="thang_sinh">
                                                    <option value="0" >Tháng</option>
                                                </select>
                                            </div>
                                            <div>
                                                <select id="ngay_sinh" class="custom-select" name="ngay_sinh">
                                                    <option value="0" >Ngày</option>
                                                </select>
                                            </div>
                                        </div>

                                        <!-- Báo lỗi ngày - tháng - năm -->
                                        <div style="position: absolute; top: 31px; left: 0; right: 0; margin-left: auto; margin-right: auto;">
                                            <small id="loi_ngay_thang_nam" style="margin: auto; color: red;"></small>
                                        </div>
                                    </div>

                                    <!-- Giới tính -->
                                    <div class="input-group" style="margin-top: -20px;">
                                        <div class="form-check form-check-radio form-check-inline">
                                            <label class="form-check-label" for="gioi_tinh_nam">
                                                <input class="form-check-input" type="radio" name="gioi_tinh" id="gioi_tinh_nam" value="0" checked>
                                                <span class="form-check-sign"></span>
                                                Nam
                                            </label>
                                        </div>
                                        <div class="form-check form-check-radio form-check-inline">
                                            <label class="form-check-label" for="gioi_tinh_nu">
                                                <input class="form-check-input" type="radio" name="gioi_tinh" id="gioi_tinh_nu" value="1">
                                                <span class="form-check-sign"></span>
                                                Nữ
                                            </label>
                                        </div>
                                    </div>

                                    <!-- Số điện thoại -->
                                    <div style="position: relative;">
                                        <div class="input-group" style="margin-top: -10px;">
                                            <span class="input-group-addon">
                                                <i class="now-ui-icons tech_mobile"></i>
                                            </span>
                                            <input type="text" class="form-control" name="so_dien_thoai" placeholder="Số điện thoại..." id="so_dien_thoai">
                                        </div>

                                        <!-- Báo lỗi số điện thoại -->
                                        <div style="position: absolute; top: 31px; left: 0; right: 0; margin-left: auto; margin-right: auto;">
                                            <small id="loi_so_dien_thoai" style="margin: auto; color: red;"></small>
                                        </div>
                                    </div>


                                    <!-- Địa chỉ -->
                                    <div style="position: relative;">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="now-ui-icons text_caps-small"></i>
                                            </span>
                                            <input type="text" class="form-control" placeholder="Địa chỉ..." name="dia_chi" id="dia_chi">
                                        </div>

                                        <!-- Báo lỗi Địa chỉ -->
                                        <div style="position: absolute; top: 31px; left: 0; right: 0; margin-left: auto; margin-right: auto;">
                                            <small id="loi_dia_chi" style="margin: auto; color: red;"></small>
                                        </div>
                                    </div>
                                    
                                    <!-- Ghi nhớ mật khẩu -->
                                    <!-- If you want to add a checkbox to this form, uncomment this code -->
                                    <!-- <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox">
                                            <span class="form-check-sign"></span>
                                            I agree to the terms and
                                            <a href="#something">conditions</a>.
                                        </label>
                                    </div> -->

                                    <div class="card-footer text-center">
                                        <button type="submit" class="btn btn-primary btn-round btn-lg" onclick="return form_dang_ky()">
                                            Đăng Ký
                                            <i class="now-ui-icons shopping_tag-content"></i>
                                        </button>
                                    </div>
                                    <?php
                                        if(isset($_GET["loi_dang_ky"])){
                                    ?>
                                        <div>
                                            <small style="color: red;">
                                                Bạn phải điền đầy đủ thông tin
                                            </small>
                                        </div>
                                    <?php
                                        }
                                    ?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Phần footer -->
        <footer class="footer ">
            <div class="container">
                <!-- <nav>
                    <ul>
                        <li>
                            <a href="https://www.creative-tim.com">
                                Creative Tim
                            </a>
                        </li>
                        <li>
                            <a href="http://presentation.creative-tim.com">
                                About Us
                            </a>
                        </li>
                        <li>
                            <a href="http://blog.creative-tim.com">
                                Blog
                            </a>
                        </li>
                        <li>
                            <a href="https://www.creative-tim.com/license">
                                License
                            </a>
                        </li>
                    </ul>
                </nav> -->
                <div class="copyright">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, Designed by
                    <a href="http://www.invisionapp.com" target="_blank">Invision</a>. Coded by
                    <a href="https://www.facebook.com/laamnguyentung" target="_blank">Laam-firstking99</a>. Version 0.0.1
                </div>
            </div>
        </footer>
    </div>

</body>
<!--   Core JS Files   -->
<script src="../js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="../js/core/popper.min.js" type="text/javascript"></script>
<script src="../js/core/bootstrap.min.js" type="text/javascript"></script>
<script src="../js/plugins/moment.min.js"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="../js/plugins/bootstrap-switch.js"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="../js/plugins/bootstrap-tagsinput.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="../js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="../js/plugins/jasny-bootstrap.min.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../js/plugins/nouislider.min.js" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="../js/plugins/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="../js/now-ui-kit.js?v=1.1.0" type="text/javascript"></script>

<!-- Regex đăng ký -->
<script type="text/javascript" src="../js/plugins/bootstrapvalidator.min.js"></script>
<script type="text/javascript" src="../js/regex/form_dang_ky.js"></script>

<!-- script jquery ngày sinh -->
<script type="text/javascript">
    $(document).ready(function() {
        // jquery ngay/thang/nam
        for(var i=1; i<=31; i++){
            $("#ngay_sinh").append(`<option value='${i}'>Ngày ${i}</option>`);
        }
        for(var i=1; i<=12; i++){
            $("#thang_sinh").append(`<option value='${i}'>Tháng ${i}</option>`);
        }
        var hien_tai = new Date();
        var nam = hien_tai.getFullYear();
        for(var j=1900; j<=nam; j++){
            $("#nam_sinh").append(`<option value='${j}'>Năm ${j}</option>`);
        }
        $("#thang_sinh, #nam_sinh").change(function(){
            var thang = $('#thang_sinh').val();
            thang = parseInt(thang);
            var ngay = 31;
            switch(thang)
            {
                case 4:
                case 6:
                case 9:
                case 11:
                    ngay=30;
                    break;
                case 2:
                    var nam = $("#nam_sinh").val();
                    if(nam%4==0)
                    {
                        ngay = 29;
                    }else
                    {
                        ngay = 28;
                    }
                    break;
            }
            $("#ngay_sinh").html('');
            for(var i=1; i<=ngay; i++)
            {
                $("#ngay_sinh").append(`<option value='${i}'>Ngày ${i}</option>`);
            }
        });
    });
</script>

</html>
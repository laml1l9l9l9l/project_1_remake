<?php
    session_start();
    if(isset($_SESSION["tai_khoan_khach_hang"])){
        $tai_khoan = $_SESSION["tai_khoan_khach_hang"];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Quản lý tài khoản</title>
    <?php
        include '../template/head_link.php';
    ?>
    
</head>

<body class="profile-page">

    <!-- Menu -->
    <?php
        include '../template/menu.php';

        // lấy dữ liệu về
        include '../connecting/open.php';
        $lenh_lay_du_lieu = mysqli_query($ket_noi,"select * from khach_hang where tai_khoan_khach_hang = '$tai_khoan' ");
        $thong_tin_ca_nhan = mysqli_fetch_array($lenh_lay_du_lieu);
    ?>

    <div class="wrapper">

        <!-- Phần đầu -->
        <div class="page-header page-header-small" filter-color="orange" >
            <div class="page-header-image" data-parallax="true" style="background-image: url('../img/bg5.jpg') ;">
            </div>
            <div class="content-center">

                <!-- Ảnh đại diện -->
                <div class="photo-container" >
                    <img src="../img/<?php echo $thong_tin_ca_nhan["anh_khach_hang"]; ?>" alt="">
                </div>

                <h2 class="title"><?php echo $thong_tin_ca_nhan["tai_khoan_khach_hang"] ?></h2>
                <p class="category" style="margin-top: 10px;"><?php echo $thong_tin_ca_nhan["so_dien_thoai"] ?></p>
            </div>
        </div>
        <div class="section">
            <div class="container" id="quan_ly_tai_khoan">

                <!-- Nút -->
                <div class="button-container">
                    <!-- Nút thay ảnh đại diện -->
                    <a href="thay_doi_anh_dai_dien.php" class="btn btn-primary btn-round btn-lg">
                        <i class="now-ui-icons media-1_album"></i> Thay đổi ảnh đại diện
                    </a>

                    <!-- Nút xem hóa đơn -->
                    <a href="../hoa_don/hoa_don.php" class="btn btn-default btn-round btn-lg" rel="tooltip">
                        <i class="now-ui-icons files_single-copy-04"></i> Xem hóa đơn
                    </a>
                </div>

                <!-- Chỉnh sửa thông tin cá nhân -->
                <h3 class="title" id="quan_ly_tai_khoan">Quản Lý Tài Khoản</h3>
                <h5 class="description text-center">Bạn có thể thay đổi thông tin cá nhân tại đây.</h5>

                <div class="d-flex justify-content-center">
                    <div class="col-md-6">
                        <!-- Form quản lý tài khoản -->
                        <form class="form" method="post" action="cap_nhat_tai_khoan.php">

                            <!-- Họ và tên -->
                            <div class="input-group" style="margin-bottom: 25px;">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons users_single-02"></i>
                                </span>
                                <input type="text" class="form-control" placeholder="Họ và tên..." name="ho_ten" value="<?php echo $thong_tin_ca_nhan["ten_khach_hang"] ?>" readonly>
                            </div>

                            <!-- Ngày sinh -->
                            <?php
                                $ngay_sinh_khach_hang = date("d - m - Y", strtotime($thong_tin_ca_nhan["ngay_sinh"]));
                            ?>
                            <div class="input-group" style="margin-bottom: 25px;">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons ui-1_calendar-60"></i>
                                </span>
                                <input type="text" class="form-control" placeholder="Ngày sinh..." name="ho_ten" value="<?php echo $ngay_sinh_khach_hang ?>" readonly>
                            </div>

                            <!-- Giới tính -->
                            <div class="input-group" style="margin-top: -20px; margin-bottom: 25px;">
                                <div class="form-check form-check-radio form-check-inline">
                                    <label class="form-check-label" for="gioi_tinh_nam">
                                        <input class="form-check-input" type="radio" name="gioi_tinh" id="gioi_tinh_nam" value="0" checked>
                                        <span class="form-check-sign"></span>
                                        Nam
                                    </label>
                                </div>
                                <div class="form-check form-check-radio form-check-inline">
                                    <label class="form-check-label" for="gioi_tinh_nu">
                                        <input class="form-check-input" type="radio" name="gioi_tinh" id="gioi_tinh_nu" value="1">
                                        <span class="form-check-sign"></span>
                                        Nữ
                                    </label>
                                </div>
                            </div>

                            <!-- Số điện thoại -->
                            <div class="input-group" style="margin-top: -10px;">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons tech_mobile"></i>
                                </span>
                                <input type="text" class="form-control" placeholder="Số điện thoại..." value="<?php echo $thong_tin_ca_nhan["so_dien_thoai"] ?>" name="so_dien_thoai" id="so_dien_thoai">
                            </div>
                            <!-- Báo lỗi số điện thoại -->
                                <div class="input-group form-group-no-border input-lg">
                                    <small id="loi_so_dien_thoai" style="padding-bottom: 10px; color: yellow;"></small>
                                </div>

                            <!-- Email -->
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons ui-1_email-85"></i>
                                </span>
                                <input type="text" class="form-control" placeholder="Email..." value="<?php echo $thong_tin_ca_nhan["email_khach_hang"] ?>" name="email" id="email">
                            </div>
                            <!-- Báo lỗi email -->
                                <div class="input-group form-group-no-border input-lg">
                                    <small id="loi_email" style="padding-bottom: 10px; color: yellow;"></small>
                                </div>

                            <!-- Địa chỉ -->
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons text_caps-small"></i>
                                </span>
                                <input type="text" class="form-control" placeholder="Địa chỉ..." value="<?php echo $thong_tin_ca_nhan["dia_chi"] ?>"  name="dia_chi" id="dia_chi">
                            </div>
                            <!-- Báo lỗi địa chỉ -->
                                <div class="input-group form-group-no-border input-lg">
                                    <small id="loi_tai_khoan" style="padding-bottom: 10px; color: yellow;"></small>
                                </div>


                        <div class="footer text-center">
                            <button type="submit" class="btn btn-primary btn-round btn-lg" id="bao_loi" onclick="return form_cap_nhat_thong_tin()">
                                Cập nhật tài khoản
                                <i class="now-ui-icons loader_refresh"></i>
                            </button>
                        </div>
                        <?php
                            if(isset($_GET["cap_nhat_thanh_cong"])){
                        ?>
                                <div class="description text-center">
                                <?php
                                    echo "<h6>Cập nhật thông tin thành công</h6>";
                                ?>
                                </div>
                        <?php

                            }

                            if( isset( $_GET["thanh_cong"]) ){
                        ?>
                                <div class="description text-center">
                                <?php
                                    echo "<h6>Đổi mật khẩu thành công</h6>";
                                ?>
                                </div>
                        <?php
                            }

                            if( isset( $_GET["thay_anh_dai_dien"]) ){
                                if( $_GET["thay_anh_dai_dien"] == 1){
                        ?>
                                <div class="description text-center">
                                <?php
                                    echo "<h6>Đổi ảnh đại diện thành công</h6>";
                                ?>
                                </div>
                        <?php
                                }else{
                        ?>
                                <div class="description text-center">
                                <?php
                                    echo "<h6>Đổi ảnh đại diện bị lỗi</h6>";
                                ?>
                                </div>
                        <?php
                                }
                            }
                        ?>
                    </form>
                    </div>
                </div>
            </div>

            <!-- Đổi mật khẩu và xem hóa đơn -->
            <div class="row">
                <div class="col-md-12">
                    <h4 class="title text-center">Đổi mật khẩu</h4>
                    <div class="nav-align-center">
                        <a href="doi_mat_khau.php">
                            <ul class="nav nav-pills nav-pills-primary nav-pills-just-icons" role="tablist">
                                <li class="nav-item">
                                    <div class="nav-link active" role="tablist">
                                        <i class="now-ui-icons loader_gear"></i>
                                    </div>
                                </li>
                            </ul>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Phần footer -->
        <footer class="footer ">
            <div class="container">
                <!-- <nav>
                    <ul>
                        <li>
                            <a href="https://www.creative-tim.com">
                                Creative Tim
                            </a>
                        </li>
                        <li>
                            <a href="http://presentation.creative-tim.com">
                                About Us
                            </a>
                        </li>
                        <li>
                            <a href="http://blog.creative-tim.com">
                                Blog
                            </a>
                        </li>
                        <li>
                            <a href="https://www.creative-tim.com/license">
                                License
                            </a>
                        </li>
                    </ul>
                </nav> -->
                <div class="copyright">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, Designed by
                    <a href="http://www.invisionapp.com" target="_blank">Invision</a>. Coded by
                    <a href="https://www.facebook.com/laamnguyentung" target="_blank">Laam-firstking99</a>. Version 0.0.1
                </div>
            </div>
        </footer>
    </div>
</body>
<!--   Core JS Files   -->
<script src="../js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="../js/core/popper.min.js" type="text/javascript"></script>
<script src="../js/core/bootstrap.min.js" type="text/javascript"></script>
<script src="../js/plugins/moment.min.js"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="../js/plugins/bootstrap-switch.js"></script>
<!--    Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="../js/plugins/bootstrap-tagsinput.js"></script>
<!--    Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="../js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!--    Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="../js/plugins/jasny-bootstrap.min.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../js/plugins/nouislider.min.js" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="../js/plugins/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="../js/now-ui-kit.js?v=1.1.0" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        // the body of this function is in assets/js/now-ui-kit.js
        nowuiKit.initContactUsMap();
    });
</script>

<!-- script jquery ngày sinh -->
<script type="text/javascript">
    $(document).ready(function() {
        // jquery ngay/thang/nam
        for(var i=1; i<=31; i++){
            $("#ngay_sinh").append(`<option value='${i}'>Ngày ${i}</option>`);
        }
        for(var i=1; i<=12; i++){
            $("#thang_sinh").append(`<option value='${i}'>Tháng ${i}</option>`);
        }
        var hien_tai = new Date();
        var nam = hien_tai.getFullYear();
        for(var j=1900; j<=nam; j++){
            $("#nam_sinh").append(`<option value='${j}'>Năm ${j}</option>`);
        }
        $("#thang_sinh, #nam_sinh").change(function(){
            var thang = $('#thang_sinh').val();
            thang = parseInt(thang);
            var ngay = 31;
            switch(thang)
            {
                case 4:
                case 6:
                case 9:
                case 11:
                    ngay=30;
                    break;
                case 2:
                    var nam = $("#nam_sinh").val();
                    if(nam%4==0)
                    {
                        ngay = 29;
                    }else
                    {
                        ngay = 28;
                    }
                    break;
            }
            $("#ngay_sinh").html('');
            for(var i=1; i<=ngay; i++)
            {
                $("#ngay_sinh").append(`<option value='${i}'>Ngày ${i}</option>`);
            }
        });
    });
</script>

</html>

<?php
    }else{
        header("location: dang_nhap.php");
    }
?>
<?php
    session_start();
    if(isset($_SESSION["tai_khoan_khach_hang"])){
        $tai_khoan = $_SESSION["tai_khoan_khach_hang"];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Thay Đổi Ảnh Đại Diện</title>
    <?php
        include '../template/head_link.php';
    ?>
    
</head>

<body class="profile-page">

    <!-- Menu -->
    <?php
        include '../template/menu.php';

        // lấy dữ liệu về
        include '../connecting/open.php';
        $lenh_lay_du_lieu = mysqli_query($ket_noi,"select * from khach_hang where tai_khoan_khach_hang = '$tai_khoan' ");
        $thong_tin_ca_nhan = mysqli_fetch_array($lenh_lay_du_lieu);
    ?>

    <div class="wrapper">

        <!-- Phần đầu -->
        <div class="page-header page-header-small" filter-color="orange" >
            <div class="page-header-image" data-parallax="true" style="background-image: url('../img/bg5.jpg') ;">
            </div>
            <div class="content-center">

                <!-- Ảnh đại diện -->
                <div class="photo-container" >
                    <img src="../img/<?php echo $thong_tin_ca_nhan["anh_khach_hang"]; ?>" alt="">
                </div>
                
                <h2 class="title"><?php echo $thong_tin_ca_nhan["tai_khoan_khach_hang"] ?></h2>
                <p class="category" style="margin-top: 10px;"><?php echo $thong_tin_ca_nhan["so_dien_thoai"] ?></p>
            </div>
        </div>
        <div class="section">
            <div class="container" id="quan_ly_tai_khoan">

                <!-- Nút -->
                <div class="button-container">
                    <!-- Nút quản lý tài khoản -->
                    <a href="quan_ly_tai_khoan.php" class="btn btn-primary btn-round btn-lg">
                        <i class="now-ui-icons users_single-02"></i> Quản lý tài khoản
                    </a>

                    <!-- Nút xem hóa đơn -->
                    <a href="../hoa_don/hoa_don.php" class="btn btn-default btn-round btn-lg" rel="tooltip">
                        <i class="now-ui-icons files_single-copy-04"></i> Xem hóa đơn
                    </a>
                </div>

                <!-- Chỉnh sửa thông tin cá nhân -->
                <h3 class="title" id="quan_ly_tai_khoan">Thay Đổi Ảnh Đại Diện</h3>
                <h5 class="description text-center">Bạn chọn ảnh muốn thay đổi làm ảnh đại diện tại đây.</h5>

                <div class="d-flex justify-content-center" id="quan_ly_tai_khoan_doi_mat_khau">
                    <div class="col-md-6">
                        <!-- Form thay đổi ảnh đại diện -->
                        <form action="lay_anh_khach_hang.php" method="post" enctype="multipart/form-data">

                            <!-- Thay ảnh -->
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons design_image"></i>
                                </span>
                                <input type="file" class="form-control" placeholder="Chọn Ảnh..." name="anh_khach_hang" accept="image/*" id="anh_khach_hang">
                            </div>
                            
                        <div class="footer text-center">
                            <button type="submit" class="btn btn-primary btn-round btn-lg">
                                Thay đổi ảnh
                            </button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

        <!-- Phần footer -->
        <footer class="footer ">
            <div class="container">
                <!-- <nav>
                    <ul>
                        <li>
                            <a href="https://www.creative-tim.com">
                                Creative Tim
                            </a>
                        </li>
                        <li>
                            <a href="http://presentation.creative-tim.com">
                                About Us
                            </a>
                        </li>
                        <li>
                            <a href="http://blog.creative-tim.com">
                                Blog
                            </a>
                        </li>
                        <li>
                            <a href="https://www.creative-tim.com/license">
                                License
                            </a>
                        </li>
                    </ul>
                </nav> -->
                <div class="copyright">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, Designed by
                    <a href="http://www.invisionapp.com" target="_blank">Invision</a>. Coded by
                    <a href="https://www.facebook.com/laamnguyentung" target="_blank">Laam-firstking99</a>. Version 0.0.1
                </div>
            </div>
        </footer>
    </div>
</body>
<!--   Core JS Files   -->
<script src="../js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="../js/core/popper.min.js" type="text/javascript"></script>
<script src="../js/core/bootstrap.min.js" type="text/javascript"></script>
<script src="../js/plugins/moment.min.js"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="../js/plugins/bootstrap-switch.js"></script>
<!--    Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="../js/plugins/bootstrap-tagsinput.js"></script>
<!--    Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="../js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!--    Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="../js/plugins/jasny-bootstrap.min.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../js/plugins/nouislider.min.js" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="../js/plugins/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="../js/now-ui-kit.js?v=1.1.0" type="text/javascript"></script>


</html>

<?php
    }else{
        header("location: dang_nhap.php");
    }
?>
<?php
    session_start();
    if(!empty($_SESSION["tai_khoan_khach_hang"])){
        $tai_khoan_khach_hang = $_SESSION["tai_khoan_khach_hang"];

        //lấy về file ảnh
        $file          = $_FILES['anh_khach_hang'];
        $imageFileType = strtolower(pathinfo($file["name"],PATHINFO_EXTENSION));

        //lưu tên file ảnh là thời gian hiện tại kèm đuôi định dạng ảnh
        $file_name     = time(). ".$imageFileType";

        //thư mục chứa ảnh
        $target_dir    = "image/";
        $target_file   = $target_dir . $file_name;
        $uploadOk = 1;

        //kiểm tra có gửi được ảnh lên không
        if(isset($_POST["submit"])) {
            $check = getimagesize($file["tmp_name"]);
            if($check !== false) {
                $uploadOk = 1;
            } else {
                $uploadOk = 0;
            }
        }

        // kiểm tra file ảnh đã tồn tại
        if (file_exists($target_file)) {
            $uploadOk = 0;
        }
        // kiểm tra dung lượng ảnh
        if ($file["size"] > 500000) {
            $uploadOk = 0;
        }
        // chỉ chấp nhận đuôi ảnh hợp lệ
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            $uploadOk = 0;
        }
        // kiểm tra nếu tất cả cái trên đều đúng thì tải ảnh lên thư mục image (thư mục con của code)
        if ($uploadOk == 1) {
            if (move_uploaded_file($file["tmp_name"], $target_file)) {
            	$uploadOk = 1;
            } else {
                $uploadOk = 0;
            }
        }

        // Thay ảnh đại diện
        if($uploadOk!=0){
            $anh = $file_name;
            include '../connecting/open.php';
            mysqli_query($ket_noi,"update khach_hang set anh_khach_hang = '$file' where tai_khoan_khach_hang = '$tai_khoan_khach_hang'");
            include '../connecting/close.php';
            header("location: quan_ly_tai_khoan.php?thay_anh_dai_dien=1#bao_loi");
        }else{
            header("location: quan_ly_tai_khoan.php?thay_anh_dai_dien=0#bao_loi");
        }
    }else{
        header("location: dang_nhap.php");
    }
<?php
session_start();
if(isset($_SESSION["tai_khoan_khach_hang"])){
    $tai_khoan = $_SESSION["tai_khoan_khach_hang"];
    ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>Đổi mật khẩu</title>
        <?php
        include '../template/head_link.php';
        ?>

    </head>

    <body class="profile-page">

        <!-- Menu -->
        <?php
        include '../template/menu.php';

        // lấy dữ liệu về
        include '../connecting/open.php';
        $lenh_lay_du_lieu = mysqli_query($ket_noi,"select * from khach_hang where tai_khoan_khach_hang = '$tai_khoan' ");
        $thong_tin_ca_nhan = mysqli_fetch_array($lenh_lay_du_lieu);
        ?>

        <div class="wrapper">

            <!-- Phần đầu -->
            <div class="page-header page-header-small" filter-color="orange" >
                <div class="page-header-image" data-parallax="true" style="background-image: url('../img/bg5.jpg') ;">
                </div>
                <div class="content-center">

                    <!-- Ảnh đại diện -->
                    <div class="photo-container" >
                        <img src="../img/<?php echo $thong_tin_ca_nhan["anh_khach_hang"]; ?>" alt="">
                    </div>
                    
                    <h2 class="title"><?php echo $thong_tin_ca_nhan["tai_khoan_khach_hang"] ?></h2>
                    <p class="category" style="margin-top: 10px;"><?php echo $thong_tin_ca_nhan["so_dien_thoai"] ?></p>
                </div>
            </div>
            <div class="section">
                <div class="container">

                    <div class="button-container">
                        <!-- Nút quản lý tài khoản -->
                        <a href="quan_ly_tai_khoan.php" class="btn btn-primary btn-round btn-lg">
                            <i class="now-ui-icons users_single-02"></i> Quản lý tài khoản
                        </a>
                        <!-- Nút xem hóa đơn -->
                        <a href="../hoa_don/hoa_don.php" class="btn btn-default btn-round btn-lg" rel="tooltip">
                            <i class="now-ui-icons files_single-copy-04"></i> Xem hóa đơn
                        </a>
                    </div>

                    <!-- Đổi mật khẩu -->
                    <h3 class="title">Đổi Mật Khẩu</h3>
                    <h5 class="description text-center">Bạn có thể thay đổi mật khẩu tại đây.</h5>

                    <div class="d-flex justify-content-center" id="doi_mat_khau">
                        <div class="col-md-6">
                            <!-- Form quản lý tài khoản -->
                            <form class="form" method="post" action="xu_ly_doi_mat_khau.php">

                                <!-- Mật khẩu cũ -->
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="now-ui-icons objects_key-25"></i>
                                    </span>
                                    <input type="password" class="form-control" placeholder="Mật khẩu cũ..." name="mat_khau_cu">
                                </div>

                                <!-- Mật khẩu mới -->
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="now-ui-icons ui-2_settings-90"></i>
                                    </span>
                                    <input type="password" class="form-control" placeholder="Mật khẩu mới..." name="mat_khau_moi">
                                </div>

                                <!-- Nhập lại mật khẩu mới -->
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="now-ui-icons loader_refresh"></i>
                                    </span>
                                    <input type="password" class="form-control" placeholder="Nhập lại mật khẩu mới..." name="nhap_lai_mat_khau_moi">
                                </div>

                                <!-- Ghi nhớ mật khẩu -->
                                <!-- If you want to add a checkbox to this form, uncomment this code -->
                        <!-- <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox">
                                <span class="form-check-sign"></span>
                                I agree to the terms and
                                <a href="#something">conditions</a>.
                            </label>
                        </div> -->

                        <div class="footer text-center">
                            <button type="submit" class="btn btn-primary btn-round btn-lg">
                                Đổi mật khẩu
                            </button>
                        </div>

                    <?php
                        if( isset( $_GET["loi_de_trong"]) ){
                    ?>
                        <div class="description text-center">
                        <?php
                            echo "<h6>Phải điền đủ vào các ô không được để trống</h6>";
                        ?>
                        </div>
                    <?php
                        }

                        if( isset($_GET["loi"] ) ){
                            if($_GET["loi"]==1){

                            ?>
                                <div class="description text-center">
                                <?php
                                    echo "<h6>Mật khẩu cũ không đúng</h6>";
                                ?>
                                </div>
                            <?php

                            }else{
                    ?>
                                <div class="description text-center">
                                <?php
                                    echo "<h6>Mật khẩu nhập lại không khớp với mật khẩu mới</h6>";
                                ?>
                                </div>
                    <?php
                            }
                        }
                    ?>

                    </form>
                </div>
            </div>
        </div>

    <!-- Phần footer -->
    <footer class="footer ">
        <div class="container">
                <!-- <nav>
                    <ul>
                        <li>
                            <a href="https://www.creative-tim.com">
                                Creative Tim
                            </a>
                        </li>
                        <li>
                            <a href="http://presentation.creative-tim.com">
                                About Us
                            </a>
                        </li>
                        <li>
                            <a href="http://blog.creative-tim.com">
                                Blog
                            </a>
                        </li>
                        <li>
                            <a href="https://www.creative-tim.com/license">
                                License
                            </a>
                        </li>
                    </ul>
                </nav> -->
                <div class="copyright" style="margin-top:100px; position: static;">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, Designed by
                    <a href="http://www.invisionapp.com" target="_blank">Invision</a>. Coded by
                    <a href="https://www.facebook.com/laamnguyentung" target="_blank">Laam-firstking99</a>. Version 0.0.1
                </div>
            </div>
        </footer>
    </div>
</body>
<!--   Core JS Files   -->
<script src="../js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="../js/core/popper.min.js" type="text/javascript"></script>
<script src="../js/core/bootstrap.min.js" type="text/javascript"></script>
<script src="../js/plugins/moment.min.js"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="../js/plugins/bootstrap-switch.js"></script>
<!--    Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="../js/plugins/bootstrap-tagsinput.js"></script>
<!--    Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="../js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!--    Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="../js/plugins/jasny-bootstrap.min.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../js/plugins/nouislider.min.js" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="../js/plugins/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="../js/now-ui-kit.js?v=1.1.0" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        // the body of this function is in assets/js/now-ui-kit.js
        nowuiKit.initContactUsMap();
    });
</script>

</html>

<?php
}else{
    header("location: dang_nhap.php");
}
?>
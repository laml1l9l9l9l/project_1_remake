<?php
    session_start();
    if(isset($_SESSION["tai_khoan_khach_hang"])){
        header("location:quan_ly_tai_khoan.php");
    }else{
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Quên mật khẩu</title>
    <?php
        include '../template/head_link.php';
    ?>
</head>

<body class="login-page">

    <!-- Menu -->
     <?php
        include '../template/menu.php';
     ?>

    <div class="page-header" filter-color="orange">
        <div class="page-header-image" style="background-image:url(../img/bg28.jpg)"></div>
        <div class="content-center">
            <div class="container">
                <div class="col-md-4 content-center">
                    <div class="card card-login card-plain">
                        <form class="form" method="post" action="xu_ly_dang_nhap.php" id="form_dang_nhap">
                            <div class="card-header text-center">
                                <div class="logo-container">
                                    <img src="../img/icon_project1.jpg" alt="">
                                </div>
                            </div>
                            <div class="card-body">
                                <!-- Tài khoản -->
                                <div class="input-group form-group-no-border input-lg">
                                    <span class="input-group-addon">
                                        <i class="now-ui-icons users_circle-08"></i>
                                    </span>
                                    <input type="text" class="form-control" name="tai_khoan" placeholder="Nhập Tên Tài khoản" id="tai_khoan" />
                                </div>

                                <!-- Báo lỗi tài khoản -->
                                <div class="input-group form-group-no-border input-lg">
                                    <small id="loi_tai_khoan" style="padding-bottom: 10px; color: yellow;"></small>
                                </div>
                                
                                <!-- Email -->
                                <div class="input-group form-group-no-border input-lg">
                                    <span class="input-group-addon">
                                        <i class="now-ui-icons ui-1_email-85"></i>
                                    </span>
                                    <input type="password" class="form-control" name="email" placeholder="Nhập Email Của Tài Khoản" id="email" />
                                </div>

                                <!-- Báo lỗi email -->
                                <div class="input-group form-group-no-border input-lg">
                                    <small id="loi_mat_khau" style="padding-bottom: 10px; color: yellow;"></small>
                                </div>

                                <!-- Số điện thoại -->
                                <div class="input-group form-group-no-border input-lg">
                                    <span class="input-group-addon">
                                        <i class="now-ui-icons tech_mobile"></i>
                                    </span>
                                    <input type="password" class="form-control" name="so_dien_thoai" placeholder="Nhập Số Điện Thoại Của Tài Khoản" id="so_dien_thoai" />
                                </div>

                                <!-- Báo lỗi số điện thoại -->
                                <div class="input-group form-group-no-border input-lg">
                                    <small id="loi_mat_khau" style="padding-bottom: 10px; color: yellow;"></small>
                                </div>
                            </div>
                            
                            <!-- Nút tìm -->
                            <div class="card-footer text-center">
                                <button class="btn btn-primary btn-round btn-lg btn-block" onclick="return form_dang_nhap()">Tìm Mật Khẩu</button>
                            </div>
                            <div class="pull-center">
                                <h6>
                                    <a href="dang_nhap.php" class="link footer-link">Quay lại</a>
                                </h6>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Phần footer -->
        <footer class="footer ">
            <div class="container">
                <!-- <nav>
                    <ul>
                        <li>
                            <a href="https://www.creative-tim.com">
                                Creative Tim
                            </a>
                        </li>
                        <li>
                            <a href="http://presentation.creative-tim.com">
                                About Us
                            </a>
                        </li>
                        <li>
                            <a href="http://blog.creative-tim.com">
                                Blog
                            </a>
                        </li>
                        <li>
                            <a href="https://www.creative-tim.com/license">
                                License
                            </a>
                        </li>
                    </ul>
                </nav> -->
                <div class="copyright">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, Designed by
                    <a href="http://www.invisionapp.com" target="_blank">Invision</a>. Coded by
                    <a href="https://www.facebook.com/laamnguyentung" target="_blank">Laam-firstking99</a>. Version 0.0.1
                </div>
            </div>
        </footer>

    </div>
</body>
<!--   Core JS Files   -->
<script src="../js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="../js/core/popper.min.js" type="text/javascript"></script>
<script src="../js/core/bootstrap.min.js" type="text/javascript"></script>
<script src="../js/plugins/moment.min.js"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="../js/plugins/bootstrap-switch.js"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="../js/plugins/bootstrap-tagsinput.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="../js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="../js/plugins/jasny-bootstrap.min.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../js/plugins/nouislider.min.js" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="../js/plugins/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

<!-- regex form -->
<script type="text/javascript" src="../js/regex/form_dang_nhap.js"></script>

<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->

</html>

<?php
    }
?>
<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title> Giới Thiệu </title>
    
    <?php
        include '../template/head_link.php';
    ?>
</head>

<body class="about-us">

<!-- Menu -->
<?php
    include '../template/menu.php';
    // include '../connecting/open.php';
?>

    <div class="wrapper">
        <div class="page-header page-header-small">
            <div class="page-header-image" data-parallax="true" style="background-image: url('../img/banner_gioi_thieu.jpg');">
            </div>
            <div class="content-center">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto">
                        <h1 class="title">Giới thiệu về Shop</h1>
                        <h4>Bạn có thể tìm mua các loại trang phục dành cho nam tại Shop</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="about-description text-center">
                <div class="features-3">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 mr-auto ml-auto">
                                <h2 class="title">Thương hiệu</h2>
                                <h4 class="description">Shop là đối tác kinh doanh của các thượng hiệu có uy tin trên thế giới.</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="info info-hover">
                                    <div class="icon icon-success icon-circle">
                                        <i class="now-ui-icons objects_globe"></i>
                                    </div>
                                    <h4 class="info-title">Sản Phẩm</h4>
                                    <p class="description">Các sản phẩm của shop được nhập từ các thương hiệu trong và ngoài nước.</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="info info-hover">
                                    <div class="icon icon-info icon-circle">
                                        <i class="now-ui-icons education_atom"></i>
                                    </div>
                                    <h4 class="info-title">Chất liệu</h4>
                                    <p class="description">Sản phẩm được nhập từ nhiều thương hiệu trong và ngoài nước nên rất đa dạng về chất lượng.</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="info info-hover">
                                    <div class="icon icon-primary icon-circle">
                                        <i class="now-ui-icons tech_watch-time"></i>
                                    </div>
                                    <h4 class="info-title">Đặt hàng</h4>
                                    <p class="description">Shop hoạt động 24/24h nên bạn có thể đặt hàng hoặc gửi cho mail cho shop về những vấn đề thắc mắc.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- Đóng div secsion -->
        </div>

    <!-- Phần footer -->
    <?php
        include '../template/footer.php';
        // include '../connecting/close.php';
    ?>
    </div>
    
</body>
<!--   Core JS Files   -->
<script src="../js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="../js/core/popper.min.js" type="text/javascript"></script>
<script src="../js/core/bootstrap.min.js" type="text/javascript"></script>
<script src="../js/plugins/moment.min.js"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="../js/plugins/bootstrap-switch.js"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="../js/plugins/bootstrap-tagsinput.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="../js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="../js/plugins/jasny-bootstrap.min.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../js/plugins/nouislider.min.js" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="../js/plugins/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="../js/now-ui-kit.js?v=1.1.0" type="text/javascript"></script>

</html>
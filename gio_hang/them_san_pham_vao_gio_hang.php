<?php
	session_start();
	if(isset($_SESSION["tai_khoan_khach_hang"])){
		$tai_khoan = $_SESSION["tai_khoan_khach_hang"];
		if(isset($_GET["ma_san_pham"])){

			//Quay ve trang cu
			$lich_su = $_SERVER['HTTP_REFERER'];


			$ma_san_pham = $_GET["ma_san_pham"];
			// Neu san pham da ton tai trong gio hang thi tang them 1
			if(isset($_SESSION["gio_hang"][$ma_san_pham])){
				if($_SESSION["gio_hang"][$ma_san_pham]["so_luong"] >= 1 && $_SESSION["gio_hang"][$ma_san_pham]["so_luong"] <= 9){
					$_SESSION["gio_hang"][$ma_san_pham]["so_luong"]++;

					header("location:$lich_su&them_san_pham=1#chi_tiet_san_pham");
				}else{

					// Thêm sản phẩm lỗi
					header("location:$lich_su&them_san_pham=0#chi_tiet_san_pham");
				}
			}else{
				// Thêm sản phẩm chưa tồn tại trong giỏ hàng
				include("../connecting/open.php");
				$lenh = mysqli_query($ket_noi,"select * from san_pham where ma_san_pham = $ma_san_pham");
				include("../connecting/close.php");
				$san_pham = mysqli_fetch_array($lenh);
				// Lưu vào session
				$_SESSION["gio_hang"][$ma_san_pham]["anh_san_pham"] = $san_pham["anh_san_pham"];
				$_SESSION["gio_hang"][$ma_san_pham]["ten_san_pham"] = $san_pham["ten_san_pham"];
				$_SESSION["gio_hang"][$ma_san_pham]["gia_san_pham"] = $san_pham["gia_san_pham"];
				$_SESSION["gio_hang"][$ma_san_pham]["so_luong"] = 1;

				header("location:$lich_su&them_san_pham=1#chi_tiet_san_pham");
			}

			
		}
		else{
			header("location:../san_pham/san_pham.php");
		}
	}else{
		header("location:../tai_khoan_khach_hang/dang_nhap.php?chua_dang_nhap=0");
	}
?>
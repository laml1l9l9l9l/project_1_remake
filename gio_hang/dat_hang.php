<?php
    session_start();
    if(isset($_SESSION["tai_khoan_khach_hang"])){
        $tai_khoan = $_SESSION["tai_khoan_khach_hang"];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Đặt Hàng</title>
    <?php
        include '../template/head_link.php';
    ?>
</head>

<body class="login-page">

    <!-- Menu -->
     <?php
        include '../template/menu.php';
     ?>

    <div class="page-header" filter-color="orange">
        <div class="page-header-image" style="background-image:url(../img/project19.jpg)"></div>
        <div class="content-center">
            <div class="container">
                <div class="col-md-4 content-center">
                    <div class="card card-login card-plain">

                        <form class="form" method="post" action="xu_ly_dat_hang.php">
                            <div class="card-header text-center">
                                <div class="content-header">
                                    <div class="row">
                                        <div class="col-md ml-auto mr-auto">
                                            <h2 class="title">Thông Tin Người Nhận Hàng</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php
                                // Mở kết nối
                                include '../connecting/open.php';
                                $lenh = mysqli_query($ket_noi,"select * from khach_hang where tai_khoan_khach_hang = '$tai_khoan' ");
                                $thong_tin_khach_hang = mysqli_fetch_array($lenh);
                            ?>

                            <div class="card-body">
                                <div class="input-group form-group-no-border input-lg">
                                    <span class="input-group-addon">
                                        <i class="now-ui-icons users_circle-08"></i>
                                    </span>
                                    <input type="text" class="form-control" name="ten_nguoi_nhan" placeholder="Họ và tên..." value="<?php echo $thong_tin_khach_hang["ten_khach_hang"] ?>" />
                                </div>
                                <div class="input-group form-group-no-border input-lg">
                                    <span class="input-group-addon">
                                        <i class="now-ui-icons objects_key-25"></i>
                                    </span>
                                    <input type="text" class="form-control" name="so_dien_thoai_nguoi_nhan" placeholder="Số điện thoại..." value="<?php echo $thong_tin_khach_hang["so_dien_thoai"] ?>" />
                                </div>
                                <div class="input-group form-group-no-border input-lg">
                                    <span class="input-group-addon">
                                        <i class="now-ui-icons text_caps-small"></i>
                                    </span>
                                    <input type="text" class="form-control" name="dia_chi_nguoi_nhan" placeholder="Địa chỉ..." value="dia_chi" />
                                </div>
                            </div>
                            <?php

                                // Đóng kết nối
                                include '../connecting/close.php';

                            if( isset( $_GET["loi"]) ){

                            ?>
                                <div class="description text-center" style="color: white;">
                                <?php
                                    echo "<h6>Bạn nhập sai toàn khoản hoặc mật khẩu</h6>";
                                ?>
                                </div>
                            <?php

                                }

                            ?>
                            <div class="card-footer text-center">
                                <button class="btn btn-primary btn-round btn-lg btn-block">Đặt Hàng</button>
                            </div>
                            <div class="pull-center">
                                <h6>
                                    <a href="gio_hang.php#gio_hang" class="link footer-link">Quay Lại</a>
                                </h6>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        
        <!-- Phần footer -->
        <footer class="footer ">
            <div class="container">
                <!-- <nav>
                    <ul>
                        <li>
                            <a href="https://www.creative-tim.com">
                                Creative Tim
                            </a>
                        </li>
                        <li>
                            <a href="http://presentation.creative-tim.com">
                                About Us
                            </a>
                        </li>
                        <li>
                            <a href="http://blog.creative-tim.com">
                                Blog
                            </a>
                        </li>
                        <li>
                            <a href="https://www.creative-tim.com/license">
                                License
                            </a>
                        </li>
                    </ul>
                </nav> -->
                <div class="copyright">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, Designed by
                    <a href="http://www.invisionapp.com" target="_blank">Invision</a>. Coded by
                    <a href="https://www.facebook.com/laamnguyentung" target="_blank">Laam-firstking99</a>. Version 0.0.1
                </div>
            </div>
        </footer>

    </div>
</body>
<!--   Core JS Files   -->
<script src="../js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="../js/core/popper.min.js" type="text/javascript"></script>
<script src="../js/core/bootstrap.min.js" type="text/javascript"></script>
<script src="../js/plugins/moment.min.js"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="../js/plugins/bootstrap-switch.js"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="../js/plugins/bootstrap-tagsinput.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="../js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="../js/plugins/jasny-bootstrap.min.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../js/plugins/nouislider.min.js" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="../js/plugins/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="../js/now-ui-kit.js?v=1.1.0" type="text/javascript"></script>

</html>

<?php
    }else{
        header("location../tai_khoan_khach_hang/quan_ly_tai_khoan.php");
    }
?>
<?php
	session_start();
	if(isset($_SESSION["gio_hang"])){
		if(isset($_GET["thay_doi"]) && isset($_GET["ma_san_pham"])){
			$ma_san_pham = $_GET["ma_san_pham"];
			if($_GET['thay_doi']=="cong"){

				// Kiểm tra tổng tiền
				if($_SESSION["tong_tien"] >= 10000000){
					// Lỗi tổng tiền
					header("location:gio_hang.php?tien_gioi_han=1#tong_tien_dat_gioi_han");
				}else{
					if($_SESSION["gio_hang"][$ma_san_pham]["so_luong"]==10){
						header("location:gio_hang.php?loi_thay_doi_cong=1&ma_san_pham=$ma_san_pham#gio_hang");
					}else{
						$_SESSION["gio_hang"][$ma_san_pham]["so_luong"]++;
						header("location:gio_hang.php#gio_hang");
					}
				}


			}else{
				if($_SESSION["gio_hang"][$ma_san_pham]["so_luong"]==1){
					header("location:gio_hang.php?loi_thay_doi_tru=1&ma_san_pham=$ma_san_pham#gio_hang");
				}else{
					$_SESSION["gio_hang"][$ma_san_pham]["so_luong"]--;
					header("location:gio_hang.php#gio_hang");
				}
			}
		}else{
			header("location:gio_hang.php#gio_hang");
		}
	}else{
		header("location:../san_pham/san_pham.php#san_pham");
	}
?>
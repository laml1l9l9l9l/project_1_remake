<?php
    session_start();
    if(empty($_SESSION["tai_khoan_khach_hang"])){
        header("location:../tai_khoan_khach_hang/dang_nhap.php");
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title> Giỏ Hàng </title>
    
    <?php
        include '../template/head_link.php';
    ?>
</head>

<body class="about-us">

<!-- Menu -->
<?php
    include '../template/menu.php';
    include '../connecting/open.php';
?>

    <div class="wrapper">
        <div class="page-header page-header-small">
            <div class="page-header-image" data-parallax="true" style="background-image: url('../img/banner_gio_hang.jpg');">
            </div>
            <div class="content-center">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto">
                        <h1 class="title">Giỏ Hàng</h1>
                        <h4>Bạn có thể thêm các sản phẩm muốn mua vào đây</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="section" id="gio_hang">
            <div class="features-5">
                <div class="container">

                    <!-- Div row giỏ hàng -->
                    <div class="row">
                        <?php
                            if(empty($_SESSION["gio_hang"])){
                        ?>

                            <div class="col-md-12">
                                <h2 class="title">Không Tồn Tại Sản Phẩm Trong Giỏ Hàng</h2>
                            </div>

                        <?php
                            }else{
                        ?>

                            <div class="col-md-12">
                                <h2 class="title">Những Sản Phẩm Đã Được Thêm Vào Giỏ Hàng</h2>
                            </div>

                        <div class="col-md-12">
                            <div class="card card-plain">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-shopping">
                                            <thead class="">
                                                <th class="text-center">
                                                </th>
                                                <th>
                                                    Sản Phẩm
                                                </th>
                                                <th class="text-right">
                                                    Giá Sản Phẩm
                                                </th>
                                                <th class="text-right">
                                                    Số Lượng
                                                </th>
                                                <th class="text-right">
                                                    Thành Tiền
                                                </th>
                                            </thead>

                                        <!-- Phẩn thông tin sản phẩm trong giỏ hàng -->
                                            <tbody>
                                                <?php
                                                    $tong_tien = 0;
                                                    foreach($_SESSION["gio_hang"] as $ma_san_pham => $san_pham){
                                                ?>
                                                <tr>
                                                    <td>
                                                        <div class="img-container">
                                                            <img src="../img/<?php echo $san_pham["anh_san_pham"] ?>" alt="...">
                                                        </div>
                                                    </td>
                                                    <td class="td-name">
                                                        <?php echo $san_pham["ten_san_pham"] ?>
                                                    </td>
                                                    <td class="td-number">
                                                        <?php echo number_format($san_pham["gia_san_pham"]); ?> <small>VNĐ</small>
                                                    </td>
                                                    <td class="td-number">
                                                        <?php echo $san_pham["so_luong"] ?>
                                                        <!-- Thay đổi số lượng sản phẩm -->
                                                        <div class="btn-group">
                                                            <!-- Giảm -->
                                                            <a href="thay_doi_so_luong_san_pham_gio_hang.php?thay_doi=tru&ma_san_pham=<?php echo $ma_san_pham ?>" class="btn btn-link" style="text-decoration: none">
                                                                <button class="btn btn-secondary btn-sm"> <i class="now-ui-icons ui-1_simple-delete"></i> </button>
                                                            </a>
                                                            <!-- Tăng -->
                                                            <a href="thay_doi_so_luong_san_pham_gio_hang.php?thay_doi=cong&ma_san_pham=<?php echo $ma_san_pham ?>" class="btn btn-link" style="text-decoration: none">
                                                                <button class="btn btn-success btn-sm"> <i class="now-ui-icons ui-1_simple-add"></i> </button>
                                                            </a>
                                                            
                                                        </div>
                                                    </td>
                                                    <td class="td-number">
                                                        <?php echo number_format($thanh_tien = $san_pham["so_luong"]*$san_pham["gia_san_pham"]) ?> <small>VNĐ</small>

                                                        <?php
                                                            // Tổng tiền
                                                            $tong_tien = $tong_tien + $thanh_tien;
                                                        ?>
                                                    </td>
                                                    <td class="td-actions">
                                                        <!-- Xóa sản phẩm -->
                                                        <a href="xoa_san_pham_gio_hang.php?ma_san_pham=<?php echo $ma_san_pham; ?>">
                                                            <button type="button" rel="tooltip" data-placement="left" title="Xóa sản phẩm" class="btn btn-neutral">
                                                                <i class="now-ui-icons ui-1_simple-remove"></i>
                                                            </button>
                                                        </a>
                                                    </td>
                                                </tr>
                                            
                                                <tr>
                                                    <td colspan="6">
                                                    <?php
                                                    // báo lỗi tăng/giảm sản phẩm
                                                        if(isset($_GET["loi_thay_doi_tru"]) && isset($_GET["ma_san_pham"])){
                                                            $ma_san_pham_loi = $_GET["ma_san_pham"];
                                                            if($ma_san_pham_loi == $ma_san_pham){
                                                    ?>
                                                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                            <strong>Sản phẩm đạt số lượng nhỏ nhất!</strong> Bạn không được phép giảm nữa.
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true" class="align-middle">&times;</span>
                                                            </button>
                                                        </div>
                                                    <?php
                                                            }
                                                        }
                                                        if(isset($_GET["loi_thay_doi_cong"]) && isset($_GET["ma_san_pham"])){
                                                            $ma_san_pham_loi = $_GET["ma_san_pham"];
                                                            if($ma_san_pham_loi == $ma_san_pham){
                                                    ?>
                                                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                            <strong>Sản phẩm đạt số lượng lớn nhất!</strong> Bạn không được phép tăng nữa.
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true" class="align-middle">&times;</span>
                                                            </button>
                                                        </div>
                                                    <?php
                                                            }
                                                        }
                                                    ?>
                                                    </td>
                                                </tr>

                                <?php
                                        // Đóng vòng lặp foreach
                                        }

                                        if(!empty($_SESSION["gio_hang"])){
                                ?>

                                                <tr>
                                                    <td colspan="3">
                                                    </td>
                                                    <td class="td-total">
                                                        Tổng tiền
                                                    </td>
                                                    <td class="td-price">
                                                        <?php echo number_format($tong_tien); ?> <small>VNĐ</small>
                                                        <?php $_SESSION["tong_tien"] = $tong_tien ?>
                                                    </td>
                                                    <td class="text-right">
                                                        <a href="dat_hang.php" class="btn btn-link" style="text-decoration: none">
                                                            <button type="button" rel="tooltip" class="btn btn-warning btn-round ">
                                                                Đặt Hàng
                                                                <i class="now-ui-icons arrows-1_minimal-right"></i>
                                                            </button>
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="5" class="text-right">
                                                        <a href="huy_gio_hang.php" class="btn btn-link" style="text-decoration: none">
                                                            <button type="button" rel="tooltip" class="btn btn-danger btn-round ">
                                                                Hủy giỏ hàng
                                                                <i class="now-ui-icons ui-1_simple-remove"></i>
                                                            </button>
                                                        </a>
                                                    </td>
                                                </tr>
                                        <?php
                                            }
                                        ?>
                                            </tbody>

                                        </table>
                                    </div>

                        
                                <?php
                                    // Thông báo
                                
                                    if(isset($_GET["tien_gioi_han"])){
                                        // Lỗi tổng tiền đạt giới hạn
                                ?>
                                    <div class="alert alert-warning alert-dismissible fade show" role="alert" id="tong_tien_dat_gioi_han">
                                        <strong>Tổng tiền đạt giới hạn!</strong> Bạn phải đặt hàng mới có thể mua tiếp.
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true" class="align-middle">&times;</span>
                                        </button>
                                    </div>
                                <?php
                                    }

                                    if(isset($_GET["dat_hang_thanh_cong"])){
                                        // Đặt hàng thành công
                                ?>
                                    <div class="col-md-6 text-center">
                                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                            <b>Đặt hàng thành công!</b> Hãy tiếp tục mua sắm.
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true" class="align-middle">&times;</span>
                                            </button>
                                        </div>
                                    </div>
                                    <!-- Xem hóa đơn -->
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-2 ml-auto mr-auto text-center">
                                            <div class="card-body">
                                                <a href="../hoa_don/hoa_don.php#hoa_don" class="btn btn-link" style="text-decoration: none;">
                                                    <button class="btn btn-info btn-round">
                                                        Xem hóa đơn
                                                        <i class="now-ui-icons files_paper"></i>
                                                    </button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php
                                    }
                                ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Quay lại trang sản phẩm -->
                    <div class="row">
                        <div class="col-md-2 ml-auto mr-auto text-center">
                            <div class="card-body">
                                <a href="../san_pham/san_pham.php#san_pham" class="btn btn-link" style="text-decoration: none;">
                                    <button class="btn btn-info btn-round">
                                        Xem sản phẩm
                                        <i class="now-ui-icons arrows-1_minimal-right"></i>
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>

            </div>
            <?php
                }
            ?>
                <!-- Đóng div container -->
                </div>
                        
            </div>

        <!-- Đóng div section -->
        </div>

    <!-- Phần footer -->
    <?php
        include '../template/footer.php';
        include '../connecting/close.php';
    ?>

    <!-- Đóng div wrapper -->
    </div>
    
</body>
<!--   Core JS Files   -->
<script src="../js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="../js/core/popper.min.js" type="text/javascript"></script>
<script src="../js/core/bootstrap.min.js" type="text/javascript"></script>
<script src="../js/plugins/moment.min.js"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="../js/plugins/bootstrap-switch.js"></script>
<!--    Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="../js/plugins/bootstrap-tagsinput.js"></script>
<!--    Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="../js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!--    Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="../js/plugins/jasny-bootstrap.min.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../js/plugins/nouislider.min.js" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="../js/plugins/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="../js/now-ui-kit.js?v=1.1.0" type="text/javascript"></script>

</html>
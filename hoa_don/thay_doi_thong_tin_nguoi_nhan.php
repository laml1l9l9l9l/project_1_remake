<?php
    session_start();
    if(isset($_SESSION["tai_khoan_khach_hang"])){
        if(isset($_GET["ma_hoa_don"])){
            $ma_hoa_don = $_GET["ma_hoa_don"];
            $_SESSION["ma_hoa_don"] = $ma_hoa_don;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title> Thay Đổi Thông Tin </title>

    <?php
        include '../template/head_link.php';
    ?>

</head>

<body class="pricing">

<!-- Menu -->
<?php
    include '../template/menu.php';
    include '../connecting/open.php';
    $lenh_lay_hoa_don = mysqli_query($ket_noi,"select * from hoa_don where ma_hoa_don = $ma_hoa_don");
    $hoa_don = mysqli_fetch_array($lenh_lay_hoa_don);
?>

    <div class="wrapper">
        <div class="page-header page-header-small">
            <div class="page-header-image" data-parallax="true" style="background-image: url('../img/bg37.jpg') ;">
            </div>
            <div class="content-center">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto">
                        <h1 class="title">Thay Đổi Thông Tin</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="main">
            <div class="pricing-4">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 ml-auto mr-auto text-center">
                            <h4 class="description">Có tất cả các hóa đơn của khách hàng tại đây.</h4>
                            <div class="section-space"></div>
                        </div>
                    </div>
                <form action="cap_nhat_thong_tin_nguoi_nhan.php">
                    <div class="row">
                        <div class="col-md-3 ml-auto mr-auto text-center">
                            <div class="card card-pricing card-plain">
                                <div class="card-header">
                                    <h6 class="category">Tên Người Nhận</h6>
                                </div>
                            </div>
                        <!-- Họ và tên -->
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons users_single-02"></i>
                                </span>
                                <input type="text" class="form-control" name="ten_nguoi_nhan" value="<?php echo $hoa_don["ten_nguoi_nhan"] ?>">
                            </div>
                        </div>
                        <div class="col-md-3 ml-auto mr-auto text-center">
                            <div class="card card-pricing card-plain">
                                <div class="card-header">
                                    <h6 class="category">Số Điện Thoại Người Nhận</h6>
                                </div>
                            </div>
                        <!-- Số điện thoại -->
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons tech_mobile"></i>
                                </span>
                                <input type="text" class="form-control" name="so_dien_thoai_nguoi_nhan" value="<?php echo $hoa_don["so_dien_thoai_nguoi_nhan"] ?>">
                            </div>
                        </div>
                        <div class="col-md-3 ml-auto mr-auto text-center">
                            <div class="card card-pricing card-plain">
                                <div class="card-header">
                                    <h6 class="category">Địa Chỉ Người Nhận</h6>
                                </div>
                            </div>
                        <!-- Địa chỉ người nhận -->
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons text_caps-small"></i>
                                </span>
                                <input type="text" class="form-control" name="dia_chi_nguoi_nhan" value="<?php echo $hoa_don["dia_chi_nguoi_nhan"] ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 ml-auto mr-auto text-center">
                            <div class="card card-pricing card-plain">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-info btn-round">
                                        Thay Đổi Thông Tin
                                    </button>
                                </div>
                            </div>
                        </div>
                        
                    </div>

                </form>
                    <div class="col-md-2 ml-auto mr-auto text-center">
                            <div class="card card-pricing card-plain">
                                <div class="card-body">
                                    <a href="hoa_don.php#hoa_don" class="btn btn-link" style="text-decoration: none;">
                                        <button type="submit" class="btn btn-info btn-round">
                                            Quay Lại
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            
            <!-- Phần footer -->
            <div class="footer-1 section-image" style="background-image: url('../img/bg27.jpg')">
                <?php
                    include '../template/footer.php';
                    include '../connecting/close.php';
                ?>
            </div>
        <!-- Đóng thẻ main -->
        </div>
    </div>

</body>
<!--   Core JS Files   -->
<script src="../js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="../js/core/popper.min.js" type="text/javascript"></script>
<script src="../js/core/bootstrap.min.js" type="text/javascript"></script>
<script src="../js/plugins/moment.min.js"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="../js/plugins/bootstrap-switch.js"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="../js/plugins/bootstrap-tagsinput.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="../js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="../js/plugins/jasny-bootstrap.min.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../js/plugins/nouislider.min.js" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="../js/plugins/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="../js/now-ui-kit.js?v=1.1.0" type="text/javascript"></script>

</html>

<?php
        }else{
            header("location:hoa_don.php");
        }
    }else{
        header("location:../tai_khoan_khach_hang/dang_nhap.php");
    }
?>
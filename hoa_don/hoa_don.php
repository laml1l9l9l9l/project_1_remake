<?php
    session_start();
    if(isset($_SESSION["tai_khoan_khach_hang"])){
        $ma_tai_khoan = $_SESSION["ma_khach_hang"];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title> Hóa Đơn </title>

    <?php
        include '../template/head_link.php';
    ?>

</head>

<body class="pricing">

<!-- Menu -->
<?php
    include '../template/menu.php';
    include '../connecting/open.php';
    $lenh_lay_hoa_don = mysqli_query($ket_noi,"select * from hoa_don where ma_khach_hang = $ma_tai_khoan order by ma_hoa_don desc");
?>

    <div class="wrapper">
        <div class="page-header page-header-small">
            <div class="page-header-image" data-parallax="true" style="background-image: url('../img/bg37.jpg') ;">
            </div>
            <div class="content-center">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto">
                        <h1 class="title">Hóa Đơn</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="main" id="hoa_don">
            <div class="pricing-4">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 ml-auto mr-auto text-center">
                            <h4 class="description">Có tất cả các hóa đơn của khách hàng tại đây.</h4>
                        </div>
                    </div>

                    <!-- Xem hóa đơn đang giao -->
                    <div class="row">
                        <div class="col-md-2 ml-auto text-center">
                            <div class="card-body">
                                <a href="../hoa_don/hoa_don.php#hoa_don_dang_giao" class="btn btn-link" style="text-decoration: none;">
                                    <button class="btn btn-warning btn-round">
                                        Hóa Đơn Đang Giao
                                        <i class="now-ui-icons ui-1_send"></i>
                                    </button>
                                </a>
                            </div>
                        </div>

                        <!-- Xem hóa đơn đã giao -->
                        <div class="col-md-2 text-center">
                            <div class="card-body">
                                <a href="../hoa_don/hoa_don.php#hoa_don_da_giao" class="btn btn-link" style="text-decoration: none;">
                                    <button class="btn btn-success btn-round">
                                        Hóa Đơn Đã Giao
                                        <i class="now-ui-icons ui-1_check"></i>
                                    </button>
                                </a>
                            </div>
                        </div>

                        <!-- Xem hóa đơn đã hủy -->
                        <div class="col-md-2 mr-auto text-center">
                            <div class="card-body">
                                <a href="hoa_don_da_xoa.php#hoa_don" class="btn btn-link" style="text-decoration: none;">
                                    <button class="btn btn-danger btn-round">
                                        Hóa Đơn Đã Hủy
                                        <i class="now-ui-icons ui-1_simple-remove"></i>
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 ml-auto mr-auto text-center">
                            <h4 class="description" style="color: purple;">Tất cả sản phẩm chưa giao.</h4>
                        </div>
                    </div>

            <?php
                while($hoa_don = mysqli_fetch_array($lenh_lay_hoa_don)){

                    if($hoa_don["tinh_trang_giao_hang"]==0){
            ?>
                    <div class="row">
                        <div class="col-md-2.5 ml-auto mr-auto text-center">
                            <div class="card card-pricing card-plain">
                                <div class="card-header">
                                    <h6 class="category">Tên Người Nhận</h6>
                                </div>
                                <div class="card-body">
                                    <h5><?php echo $hoa_don["ten_nguoi_nhan"]; ?></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 ml-auto mr-auto text-center">
                            <div class="card card-pricing card-plain">
                                <div class="card-header">
                                    <h6 class="category">Số Điện Thoại Người Nhận</h6>
                                </div>
                                <div class="card-body">
                                    <h5><?php echo $hoa_don["so_dien_thoai_nguoi_nhan"]; ?></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 ml-auto mr-auto text-center">
                            <div class="card card-pricing card-plain">
                                <div class="card-body">
                                    <ul>
                                        <li>
                                            Địa Chỉ Người Nhận: <?php echo $hoa_don["dia_chi_nguoi_nhan"]; ?>
                                        </li>
                                        <li>
                                            Tình Trạng Giao Hàng: <?php if($hoa_don["tinh_trang_giao_hang"]==0){ ?> <p style="color: purple;"><strong>Chưa giao hàng</strong></p> <?php }else if($hoa_don["tinh_trang_giao_hang"]==1){ ?> <p style="color: #d6d629;"><strong>Đang giao hàng</strong></p> <?php }else if($hoa_don["tinh_trang_giao_hang"]==2){ ?> <p style="color: green;"><strong>Đã giao hàng</strong></p> <?php }else{ ?> <p style="color: red;"><strong>Đơn hàng đã hủy</strong></p> <?php } ?>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 ml-auto mr-auto text-center">
                            <div class="card card-pricing card-plain">
                                <div class="card-body">
                                    <h4 class="card-title">
                                        <?php echo $hoa_don["thanh_tien"]; ?> VNĐ
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 ml-auto mr-auto text-center">
                            <div class="card card-pricing card-plain">
                                <div class="card-body">
                                    <a href="thay_doi_thong_tin_nguoi_nhan.php?ma_hoa_don=<?php echo $hoa_don["ma_hoa_don"] ?>" class="btn btn-info btn-round">
                                        Thay Đổi Thông Tin
                                        <i class="now-ui-icons ui-1_settings-gear-63"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-0.5 ml-auto mr-auto text-center">
                            <div class="card card-pricing card-plain">
                                <div class="card-body">
                                    <a href="hoa_don_chi_tiet.php?ma_hoa_don=<?php echo $hoa_don["ma_hoa_don"] ?>" class="badge badge-pill badge-primary" title="Xem chi tiết hóa đơn">
                                        <img src="../img/detail_icon.jpg" width="35px" height="35px">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1 ml-auto mr-auto text-center">
                            <div class="card card-pricing card-plain">
                                <div class="card-body">
                                    <a href="xoa_hoa_don.php?ma_hoa_don=<?php echo $hoa_don["ma_hoa_don"] ?>" class="badge badge-pill badge-primary" title="Hủy hóa đơn">
                                        <img src="../img/icon_delete.jpg" width="35px" height="35px">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
            <?php
                    }
                }
            ?>

            <!-- Sản phẩm đang giao -->

                    <div class="row" id="hoa_don_dang_giao">
                        <div class="col-md-6 ml-auto mr-auto text-center">
                            <h4 class="description" style="color: #d6d629;">Tất cả sản phẩm đang giao.</h4>
                        </div>
                    </div>
            <?php
                $lenh_lay_hoa_don = mysqli_query($ket_noi,"select * from hoa_don where ma_khach_hang = $ma_tai_khoan order by ma_hoa_don desc");
                while($hoa_don = mysqli_fetch_array($lenh_lay_hoa_don)){

                    if($hoa_don["tinh_trang_giao_hang"]==1){
            ?>
                    <div class="row">
                        <div class="col-md-2.5 ml-auto mr-auto text-center">
                            <div class="card card-pricing card-plain">
                                <div class="card-header">
                                    <h6 class="category">Tên Người Nhận</h6>
                                </div>
                                <div class="card-body">
                                    <h5><?php echo $hoa_don["ten_nguoi_nhan"]; ?></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 ml-auto mr-auto text-center">
                            <div class="card card-pricing card-plain">
                                <div class="card-header">
                                    <h6 class="category">Số Điện Thoại Người Nhận</h6>
                                </div>
                                <div class="card-body">
                                    <h5><?php echo $hoa_don["so_dien_thoai_nguoi_nhan"]; ?></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 ml-auto mr-auto text-center">
                            <div class="card card-pricing card-plain">
                                <div class="card-body">
                                    <ul>
                                        <li>
                                            Địa Chỉ Người Nhận: <?php echo $hoa_don["dia_chi_nguoi_nhan"]; ?>
                                        </li>
                                        <li>
                                            Tình Trạng Giao Hàng: <?php if($hoa_don["tinh_trang_giao_hang"]==0){ ?> <p style="color: purple;"><strong>Chưa giao hàng</strong></p> <?php }else if($hoa_don["tinh_trang_giao_hang"]==1){ ?> <p style="color: #d6d629;"><strong>Đang giao hàng</strong></p> <?php }else if($hoa_don["tinh_trang_giao_hang"]==2){ ?> <p style="color: green;"><strong>Đã giao hàng</strong></p> <?php }else{ ?> <p style="color: red;"><strong>Đơn hàng đã hủy</strong></p> <?php } ?>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 ml-auto mr-auto text-center">
                            <div class="card card-pricing card-plain">
                                <div class="card-body">
                                    <h4 class="card-title">
                                        <?php echo $hoa_don["thanh_tien"]; ?> VNĐ
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-0.5 ml-auto mr-auto text-center">
                            <div class="card card-pricing card-plain">
                                <div class="card-body">
                                    <a href="hoa_don_chi_tiet.php?ma_hoa_don=<?php echo $hoa_don["ma_hoa_don"] ?>" class="badge badge-pill badge-primary" title="Xem chi tiết hóa đơn">
                                        <img src="../img/detail_icon.jpg" width="35px" height="35px">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
            <?php
                    }
                }
            ?>

            <!-- Sản phẩm đã giao -->

                    <div class="row" id="hoa_don_da_giao">
                        <div class="col-md-6 ml-auto mr-auto text-center">
                            <h4 class="description" style="color: green;">Tất cả sản phẩm đã giao.</h4>
                        </div>
                    </div>
            <?php
                $lenh_lay_hoa_don = mysqli_query($ket_noi,"select * from hoa_don where ma_khach_hang = $ma_tai_khoan order by ma_hoa_don desc");
                while($hoa_don = mysqli_fetch_array($lenh_lay_hoa_don)){

                    if($hoa_don["tinh_trang_giao_hang"]==2){
            ?>
                    <div class="row">
                        <div class="col-md-2.5 ml-auto mr-auto text-center">
                            <div class="card card-pricing card-plain">
                                <div class="card-header">
                                    <h6 class="category">Tên Người Nhận</h6>
                                </div>
                                <div class="card-body">
                                    <h5><?php echo $hoa_don["ten_nguoi_nhan"]; ?></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 ml-auto mr-auto text-center">
                            <div class="card card-pricing card-plain">
                                <div class="card-header">
                                    <h6 class="category">Số Điện Thoại Người Nhận</h6>
                                </div>
                                <div class="card-body">
                                    <h5><?php echo $hoa_don["so_dien_thoai_nguoi_nhan"]; ?></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 ml-auto mr-auto text-center">
                            <div class="card card-pricing card-plain">
                                <div class="card-body">
                                    <ul>
                                        <li>
                                            Địa Chỉ Người Nhận: <?php echo $hoa_don["dia_chi_nguoi_nhan"]; ?>
                                        </li>
                                        <li>
                                            Tình Trạng Giao Hàng: <?php if($hoa_don["tinh_trang_giao_hang"]==0){ ?> <p style="color: purple;"><strong>Chưa giao hàng</strong></p> <?php }else if($hoa_don["tinh_trang_giao_hang"]==1){ ?> <p style="color: #d6d629;"><strong>Đang giao hàng</strong></p> <?php }else if($hoa_don["tinh_trang_giao_hang"]==2){ ?> <p style="color: green;"><strong>Đã giao hàng</strong></p> <?php }else{ ?> <p style="color: red;"><strong>Đơn hàng đã hủy</strong></p> <?php } ?>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 ml-auto mr-auto text-center">
                            <div class="card card-pricing card-plain">
                                <div class="card-body">
                                    <h4 class="card-title">
                                        <?php echo $hoa_don["thanh_tien"]; ?> VNĐ
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-0.5 ml-auto mr-auto text-center">
                            <div class="card card-pricing card-plain">
                                <div class="card-body">
                                    <a href="hoa_don_chi_tiet.php?ma_hoa_don=<?php echo $hoa_don["ma_hoa_don"] ?>" class="badge badge-pill badge-primary" title="Xem chi tiết hóa đơn">
                                        <img src="../img/detail_icon.jpg" width="35px" height="35px">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
            <?php
                    }
                }
            ?>
                    
                    <!-- Xem hóa đơn đang giao -->
                    <div class="row">
                        <div class="col-md-2 ml-auto text-center">
                            <div class="card-body">
                                <a href="../hoa_don/hoa_don.php#hoa_don_dang_giao" class="btn btn-link" style="text-decoration: none;">
                                    <button class="btn btn-warning btn-round">
                                        Hóa Đơn Đang Giao
                                        <i class="now-ui-icons ui-1_send"></i>
                                    </button>
                                </a>
                            </div>
                        </div>

                        <!-- Xem hóa đơn đã giao -->
                        <div class="col-md-2 text-center">
                            <div class="card-body">
                                <a href="../hoa_don/hoa_don.php#hoa_don_da_giao" class="btn btn-link" style="text-decoration: none;">
                                    <button class="btn btn-success btn-round">
                                        Hóa Đơn Đã Giao
                                        <i class="now-ui-icons ui-1_check"></i>
                                    </button>
                                </a>
                            </div>
                        </div>

                        <!-- Xem hóa đơn đã hủy -->
                        <div class="col-md-2 mr-auto text-center">
                            <div class="card-body">
                                <a href="hoa_don_da_xoa.php#hoa_don" class="btn btn-link" style="text-decoration: none;">
                                    <button class="btn btn-danger btn-round">
                                        Hóa Đơn Đã Hủy
                                        <i class="now-ui-icons ui-1_simple-remove"></i>
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2 ml-auto mr-auto text-center">
                            <div class="card-body">
                                <a href="../tai_khoan_khach_hang/quan_ly_tai_khoan.php" class="btn btn-link" style="text-decoration: none;">
                                    <button class="btn btn-info btn-round">
                                        Quản Lý Tài Khoản
                                        <i class="now-ui-icons users_single-02"></i>
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            
            <!-- Phần footer -->
            <div class="footer-1 section-image" style="background-image: url('../img/bg27.jpg')">
                <?php
                    include '../template/footer.php';
                    include '../connecting/close.php';
                ?>
            </div>
        <!-- Đóng thẻ main -->
        </div>
    </div>

</body>
<!--   Core JS Files   -->
<script src="../js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="../js/core/popper.min.js" type="text/javascript"></script>
<script src="../js/core/bootstrap.min.js" type="text/javascript"></script>
<script src="../js/plugins/moment.min.js"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="../js/plugins/bootstrap-switch.js"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="../js/plugins/bootstrap-tagsinput.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="../js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="../js/plugins/jasny-bootstrap.min.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../js/plugins/nouislider.min.js" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="../js/plugins/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="../js/now-ui-kit.js?v=1.1.0" type="text/javascript"></script>

<!-- Đoạn ajax nút -->

<!-- <script type="text/javascript">   
    $('selector').click(function(e){
      e.preventDefault();
      $.ajax({
           url: "<where to post>",
           type: "POST",//type of posting the data
           data: ,
           success: function (data) {
             //what to do in success
           },
           error: function(xhr, ajaxOptions, thrownError){
              //what to do in error
           },
           timeout : 15000//timeout of the ajax call
      });

    });
</script> -->

</html>

<?php
    }else{
        header("location:../tai_khoan_khach_hang/dang_nhap.php");
    }
?>
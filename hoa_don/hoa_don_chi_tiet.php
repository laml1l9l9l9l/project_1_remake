<?php 
	session_start();
	if(isset($_SESSION["tai_khoan_khach_hang"])){
		$tai_khoan = $_SESSION["tai_khoan_khach_hang"];
		if(isset($_GET["ma_hoa_don"])){
			$ma_hoa_don = $_GET["ma_hoa_don"];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title> Hóa Đơn Chi Tiết </title>

    <?php
        include '../template/head_link.php';
    ?>

</head>

<body class="pricing">

<!-- Menu -->
<?php
    include '../template/menu.php';
    include '../connecting/open.php';
    $lenh_lay_hoa_don_chi_tiet = mysqli_query($ket_noi,"select san_pham.anh_san_pham, san_pham.ten_san_pham, san_pham.gia_san_pham, hoa_don_chi_tiet.so_luong from hoa_don_chi_tiet inner join san_pham on san_pham.ma_san_pham = hoa_don_chi_tiet.ma_san_pham where hoa_don_chi_tiet.ma_hoa_don = $ma_hoa_don");
?>

    <div class="wrapper">
        <div class="page-header page-header-small">
            <div class="page-header-image" data-parallax="true" style="background-image: url('../img/project20.jpg') ;">
            </div>
            <div class="content-center">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto">
                        <h1 class="title">Hóa Đơn Chi Tiết</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="main">
            <div class="pricing-4">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 ml-auto mr-auto text-center">
                            <h4 class="description">Có tất cả những thông tin chi tiết về hóa đơn.</h4>
                            <div class="section-space"></div>
                        </div>
                    </div>

            <?php
                while($hoa_don_chi_tiet = mysqli_fetch_array($lenh_lay_hoa_don_chi_tiet)){
                	$thanh_tien = $hoa_don_chi_tiet["gia_san_pham"] * $hoa_don_chi_tiet["so_luong"];
            ?>
                    <div class="row">
                        <div class="col-md-3 ml-auto mr-auto text-center">
                            <div class="card card-pricing card-plain">
                                <div class="card-header">
                                    <h6 class="category">Ảnh Sản Phẩm</h6>
                                </div>
                                <div class="card-body">
                                    <img src="../img/<?php echo $hoa_don_chi_tiet["anh_san_pham"]; ?>" class="img-thumbnail">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 ml-auto mr-auto text-center">
                            <div class="card card-pricing card-plain">
                                <div class="card-header">
                                    <h6 class="category">Tên Sản Phẩm</h6>
                                </div>
                                <div class="card-body">
                                    <h4><?php echo $hoa_don_chi_tiet["ten_san_pham"]; ?></h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 ml-auto mr-auto text-center">
                            <div class="card card-pricing card-plain">
                                <div class="card-body">
                                    <ul>
                                        <li>
                                            Số Lượng: <?php echo $hoa_don_chi_tiet["so_luong"]; ?>
                                        </li>
                                        <li>
                                            Thành Tiền: <?php echo $thanh_tien; ?>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
            <?php
                }
            ?>
	 				<!-- Nút quay lại trang hóa đơn -->
	                <div class="row">
	                	<div class="col-md-2 ml-auto mr-auto text-center">
	                		<div class="card-body">
	                            <a href="hoa_don.php#hoa_don" class="btn btn-link" style="text-decoration: none;">
                                    <button class="btn btn-info btn-round">
	                                   Quay Lại Hóa Đơn
                                    </button>
	                            </a>
	                        </div>
	                	</div>
	                </div>
	            </div>
        	</div>
            
            <!-- Phần footer -->
            <div class="footer-1 section-image" style="background-image: url('../img/bg27.jpg')">
                <?php
                    include '../template/footer.php';
                    include '../connecting/close.php';
                ?>
            </div>
        <!-- Đóng thẻ main -->
        </div>
    <!-- Đóng thẻ wrapper -->
    </div>

</body>
<!--   Core JS Files   -->
<script src="../js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="../js/core/popper.min.js" type="text/javascript"></script>
<script src="../js/core/bootstrap.min.js" type="text/javascript"></script>
<script src="../js/plugins/moment.min.js"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="../js/plugins/bootstrap-switch.js"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="../js/plugins/bootstrap-tagsinput.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="../js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="../js/plugins/jasny-bootstrap.min.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../js/plugins/nouislider.min.js" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="../js/plugins/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="../js/now-ui-kit.js?v=1.1.0" type="text/javascript"></script>

</html>

<?php
		}else{
			header("location:hoa_don.php");
		}
	}else{
		header("location:../tai_khoan_khach_hang/dang_nhap.php");
	}
?>